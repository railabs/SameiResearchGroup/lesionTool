classdef insertionPointObject < handle
    
    properties
        handles
        position
        visible = 'on';
        color = [206 82 65]./256;
    end
    
    properties (Dependent = true)
        position_physical
    end
    
    events
        newInsertionPosition
    end
    
    methods
        
        %Contstructor
        function point = insertionPointObject(lesionTool)
            
            %get the parent axes handle
            h = getHandles(lesionTool.handles.imtool_CT);
            point.handles.parent = h.Axes;
            point.handles.lesionTool=lesionTool;
            point.handles.fig = lesionTool.handles.fig;
            
            %get the center of the image
            xlims = get(h.Axes,'Xlim');
            ylims = get(h.Axes,'Ylim');
            position(1) = round(mean(xlims));
            position(2) = round(mean(ylims));
            
            %get the countour of the central slice of the lesion
            [x,y] = getContours(point);
            
            %create the filled polygon
            x = x+position(1); y = y+position(2);
            h=fill(x,y,point.color,'Parent',point.handles.parent);
            set(h,'FaceAlpha',.2,'EdgeColor',point.color);
            point.handles.poly=h;
            
            %make the center point graphics object
            point.handles.point = plot(position(1),position(2),'+','MarkerSize',12,'MarkerEdgeColor',point.color,'Parent',point.handles.parent);

            %add the listeners
            addlistener(lesionTool,'updatedLesion',@point.handlePropEvents);
            addlistener(lesionTool,'CTimageChanged',@point.handlePropEvents);
            h = getHandles(lesionTool.handles.imtool_CT);
            addlistener(h.Slider,'Value','PostSet',@point.handlePropEvents);
    
            %set the button down function
            fun = @(hObject,evnt) ButtonDownFunction(hObject,evnt,point); set(point.handles.point,'ButtonDownFcn',fun);
            
            %set the position
            point.position = position;
        end
        
        function set.position(point,position)
            %get the old position
            oldPos = point.position;
            if isempty(oldPos)
                oldPos=position;
            end
            
            %set the position property
            point.position=position;
            
            %get the distance moved
            d = position-oldPos;
            
            %get the polygon points
            x=get(point.handles.poly,'XData');
            y=get(point.handles.poly,'YData');
            
            %move the polygon
            x=x+d(1);
            y=y+d(2);
            set(point.handles.poly,'XData',x);
            set(point.handles.poly,'YData',y);
            
            %move the central point
            set(point.handles.point,'XData',position(1),'YData',position(2));
            
            %Broadcast that the position has changed
            notify(point,'newInsertionPosition')
            
            
        end
        
        function set.visible(point,visible)
            
            switch visible
                case 'on'
                    set(point.handles.point,'Visible','on');
                    set(point.handles.poly,'Visible','on');
                    point.visible=visible;
                case 'off'
                    set(point.handles.point,'Visible','off');
                    set(point.handles.poly,'Visible','off');
                    point.visible=visible;
            end
            
        end
        
        function set.color(point,color)
            point.color=color;
            set(point.handles.poly,'FaceColor',color,'EdgeColor',color);
            set(point.handles.point,'MarkerEdgeColor',color);
        end
        
        function handlePropEvents(point,src,evnt)
            
            switch evnt.EventName
                
                case 'updatedLesion'
                    [x,y] = getContours(point);
                    x = x+point.position(1); y = y+point.position(2);
                    set(point.handles.poly,'XData',x);
                    set(point.handles.poly,'YData',y);
                    
                case 'CTimageChanged'
                    
                    %remake the contours
                    [x,y] = getContours(point);
                    x = x+point.position(1); y = y+point.position(2);
                    set(point.handles.poly,'XData',x);
                    set(point.handles.poly,'YData',y);
                    
                    %place point at center of the image
                    xlims = get(point.handles.parent,'Xlim');
                    ylims = get(point.handles.parent,'Ylim');
                    position(1) = round(mean(xlims));
                    position(2) = round(mean(ylims));
                    
                    %set the new position
                    point.position=position;
                case 'PostSet'
                    notify(point,'newInsertionPosition')
                    

            end
        end
        
        function [x,y] = getContours(point)
            
            %get the image slice.
            im = getImage(point.handles.lesionTool.handles.imtool_Lesion);
            slice = round(size(im,3)/2);
            im=im(:,:,slice);
            
            %get the isovalue at which to compute the contrast;
            C = point.handles.lesionTool.lesion.contrast;
            val = .1*C;
            
            %get the coordinate system
            [X,Y,~] = getCartesianCoordinates(point.handles.lesionTool.lesion,'PostV');
            
            %make the contour
            fig = figure;
            [C,h]=contour(X,Y,im,[val val]);
            close(fig);
            
            %extract the x and y points
            nPoints = C(2,1);
            x= C(1,2:2+nPoints-1);
            y=C(2,2:2+nPoints-1);
            
            %convert those points to pixel units of the image
            psize = point.handles.lesionTool.CTInfo.psize;
            if isempty(psize)
                psize = [4 4 4];
            end
            x=x./psize(1);
            y=y./psize(2);
            
            
            
        end
        
        function position_physical = get.position_physical(point)
            position_physical = point.position;
            
        end
        
        function T = getLocationText(point)
            
            %Get the in-plane position
            position = round(point.position);
            
            %Get the slice location
            slice = getCurrentSlice(point.handles.lesionTool.handles.imtool_CT);
            
            %Make the string
            T = ['(' num2str(position(1)) ',' num2str(position(2)) ',' num2str(slice) ')'];
        end
        
    end
    
end

function ButtonDownFunction(hObject,evnt,point)

%get the parent figure handle
fig = point.handles.fig;

%get the current button motion and button up functions of the figure
WBMF_old = get(fig,'WindowButtonMotionFcn');
WBUF_old = get(fig,'WindowButtonUpFcn');

%set the new window button motion function and button up function of the figure
fun = @(src,evnt) ButtonMotionFunction(src,evnt,point);
fun2=@(src,evnt)  ButtonUpFunction(src,evnt,point,WBMF_old,WBUF_old);
set(fig,'WindowButtonMotionFcn',fun,'WindowButtonUpFcn',fun2);

end

function ButtonMotionFunction(src,evnt,point)

%get the current point
cp = get(point.handles.parent,'CurrentPoint'); cp=[cp(1,1) cp(1,2)];
position(1) = cp(1); position(2) = cp(2);

%set the new position
point.position=position;

end

function ButtonUpFunction(src,evnt,point,WBMF_old,WBUF_old)
fig = point.handles.fig;
set(fig,'WindowButtonMotionFcn',WBMF_old,'WindowButtonUpFcn',WBUF_old);
end