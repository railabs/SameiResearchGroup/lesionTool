classdef lesion3DViewer < handle
    properties
        handles
        lesion
        tool
        lesionTool
        alpha = .9
        iso = .2;
        color = [206 82 65]./256;
        position
        listenerHandle  %handle to the slider
    end
    
    properties (Dependent)
        zdata
    end
    
    properties (Constant)
        tbuff = .1;
        h = 30;
        wbutt = 20;
    end
    
    events
        newIso
    end
    
    methods
        
        %Constructor
        function viewer = lesion3DViewer(parent,position,lesion,tool,lesionTool)
            
            %set the default values
            viewer.lesion = lesion;
            viewer.tool = tool;
            viewer.lesionTool = lesionTool;
     
            h=viewer.h; %Pixel height of the top panel
            wbutt = viewer.wbutt; %Pixel size of the buttons
            
            %Create the large panel within the parent object
            viewer.handles.parent = parent;
            viewer.handles.largePanel = uipanel(parent,'Position',position,'Title','','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','FontSize',12,'TitlePosition','leftbottom');
            set(viewer.handles.largePanel,'Units','Pixels'); pos=get(viewer.handles.largePanel,'Position'); set(viewer.handles.largePanel,'Units','normalized');
            viewer.handles.toolPanel = uipanel(viewer.handles.largePanel,'Units','Pixels','Position',[0 pos(4)-h pos(3) h],'Title','','BackgroundColor','k','ForegroundColor','w','HighlightColor','k');
            viewer.handles.axesPanel = uipanel(viewer.handles.largePanel,'Units','Pixels','Position',[0 0 pos(3) pos(4)-h],'Title','','BackgroundColor','k','ForegroundColor','w','HighlightColor','k');
            
            
            %Set up the resize function
            fun=@(x,y) panelResizeFunction(x,y,viewer,h);
            set(viewer.handles.largePanel,'ResizeFcn',fun)
            
            %create the check boxes
            wp=h;
            w=wbutt;
            buff=(wp-w)/2;
            
            %Create Axes checkbox
            viewer.handles.Checkbox_Axes     =   uicontrol(viewer.handles.toolPanel,'Style','Checkbox','String','Axes?','Position',[0 0 3.5*w w],'TooltipString','Show Axes','BackgroundColor','k','ForegroundColor','w','Value',1);
            fun=@(hObject,evnt) Checkbox_Callback(hObject,evnt,viewer,'Axes');
            set(viewer.handles.Checkbox_Axes,'Callback',fun)
            lp=buff+3.5*w;
            
            %Create Grid checkbox
            viewer.handles.Checkbox_Grid     =   uicontrol(viewer.handles.toolPanel,'Style','Checkbox','String','Grid?','Position',[lp 0 3.5*w w],'TooltipString','Show Grid','BackgroundColor','k','ForegroundColor','w');
            fun=@(hObject,evnt) Checkbox_Callback(hObject,evnt,viewer,'Grid');
            set(viewer.handles.Checkbox_Grid,'Callback',fun)
            lp=lp+buff+3.5*w;
            
            %Create Slice checkbox
            viewer.handles.Checkbox_Slice     =   uicontrol(viewer.handles.toolPanel,'Style','Checkbox','String','Slice?','Position',[lp 0 3.5*w w],'TooltipString','Show Slice','BackgroundColor','k','ForegroundColor','w');
            fun=@(hObject,evnt) Checkbox_Callback(hObject,evnt,viewer,'Slice');
            set(viewer.handles.Checkbox_Slice,'Callback',fun)
            lp=lp+buff+3.5*w;
            
            %Create Shape checkbox
            viewer.handles.Checkbox_Shape     =   uicontrol(viewer.handles.toolPanel,'Style','Checkbox','String','Shape?','Position',[lp 0 3.5*w w],'TooltipString','Show Shape','BackgroundColor','k','ForegroundColor','w');
            fun=@(hObject,evnt) Checkbox_Callback(hObject,evnt,viewer,'Shape');
            set(viewer.handles.Checkbox_Shape,'Callback',fun)
            lp=lp+buff+3.5*w;
            
            %Create Lesion checkbox
            viewer.handles.Checkbox_Lesion     =   uicontrol(viewer.handles.toolPanel,'Style','Checkbox','String','Lesion?','Position',[lp 0 3.5*w w],'TooltipString','Show Lesion Isosurface','BackgroundColor','k','ForegroundColor','w','Value',1);
            fun=@(hObject,evnt) Checkbox_Callback(hObject,evnt,viewer,'Lesion');
            set(viewer.handles.Checkbox_Lesion,'Callback',fun)
            lp=lp+buff+3.5*w;
            
            %Create the slider
            viewer.handles.Slider_ISO = uicontrol(viewer.handles.axesPanel,'Style','Slider','Units','Normalized','Position',[.95 0 .05 1],'TooltipString','Change Isovalue');
            set(viewer.handles.Slider_ISO,'min',0,'max',1,'value',viewer.iso)
            set(viewer.handles.Slider_ISO,'SliderStep',[.1 .1])
            fun=@(hObject,evnt) Slider_Callback(hObject,evnt,viewer);
            set(viewer.handles.Slider_ISO,'Callback',fun)
            
            %Create the iso value text
            viewer.handles.text_ISO=uicontrol(viewer.handles.toolPanel,'Style','text','String',['ISO = ' num2str(viewer.iso)],'Units','Normalized','Position',[.5 0 .48 .8],'BackgroundColor','k','ForegroundColor','w','FontSize',10,'HorizontalAlignment','Right');
            
            %Create listener for the image data in imtool3D
            addlistener(tool,'newImage',@viewer.handlePropEvents);
           
            %create the axis
            viewer.handles.Axes = axes('Parent',viewer.handles.axesPanel,'Color','none','XColor','w','YColor','w','Zcolor','w','YDir','normal','Position',[0 0 1 1]);
            axis equal; view(3); hold on; axis off; axis vis3d;
            fun=@(hObject,eventdata) axesButtonDownFunction(hObject,eventdata,viewer,'Axes');
            set(viewer.handles.Axes,'ButtonDownFcn',fun)
            
            
            %Create the axis lines
            FOV = lesion.FOV; tbuff = FOV*viewer.tbuff;
            viewer.handles.axis(1) = plot3([-FOV(1)/2 FOV(1)/2],[0 0],[0 0],'-w','Parent',viewer.handles.Axes);
            viewer.handles.axis(2) = text((FOV(1)/2) + tbuff(1),0,0,'x','Color','w');
            viewer.handles.axis(3) = plot3([0 0],[-FOV(2)/2 FOV(2)/2],[0 0],'-w','Parent',viewer.handles.Axes);
            viewer.handles.axis(4) = text(0,(FOV(2)/2) + tbuff(2),0,'y','Color','w');
            viewer.handles.axis(5) = plot3([0 0],[0 0],[-FOV(3)/2 FOV(3)/2],'-w','Parent',viewer.handles.Axes);
            viewer.handles.axis(6) = text(0,0,(FOV(3)/2) + tbuff(3),'z','Color','w');
            xlim([-FOV(1)/2 FOV(1)/2]); ylim([-FOV(2)/2 FOV(2)/2]); zlim([-FOV(3)/2 FOV(3)/2]);
            fun=@(hObject,eventdata) axesButtonDownFunction(hObject,eventdata,viewer,'Axes');
            set(viewer.handles.axis,'ButtonDownFcn',fun)
            
            %Create the surface object
            [X,Y,Z] = getCartesianCoordinates(lesion,'Post');
            C = lesion.contrast;
            shape = isosurface(X,Y,Z,getImage(tool),viewer.iso*C);
            viewer.handles.surface  = patch(shape);
            set(viewer.handles.surface,'FaceColor',viewer.color,'EdgeColor','none');
            set(viewer.handles.surface,'FaceAlpha',viewer.alpha);
            light('Position',[-15 -15 15],'Style','Local');
            %camlight;
            lighting gouraud
            fun=@(hObject,eventdata) axesButtonDownFunction(hObject,eventdata,viewer,'Surface');
            set(viewer.handles.surface,'ButtonDownFcn',fun)
            
            
            %create the image plane object
            viewer.handles.plane=surface('XData',[-FOV(1)/2 FOV(1)/2;-FOV(1)/2 FOV(1)/2],...
                'YData',[-FOV(2)/2 -FOV(2)/2;FOV(2)/2 FOV(2)/2],...
                'ZData',[0 0; 0 0],...
                'CData',.2,...
                'FaceColor','texturemap','FaceAlpha',.9,'EdgeColor',[.5 .5 .5],'Visible','off');
            h=getHandles(tool);
            addlistener(h.Slider,'Value','PostSet',@viewer.handlePropEvents);
            viewer.listenerHandle = h.Slider;
            fun=@(hObject,eventdata) axesButtonDownFunction(hObject,eventdata,viewer,'Plane');
            set(viewer.handles.plane,'ButtonDownFcn',fun)
            
            %Create the control points
            controlPoints = lesion.controlPoints;
            x = controlPoints(:,1); y = controlPoints(:,2); z = controlPoints(:,3);
            viewer.handles.CP(1)=scatter3(x,y,z,'filled','o','SizeData',50,'Visible','off','MarkerFaceColor',viewer.color,'MarkerEdgeColor','none');
            p=zeros(size(x,1),1);
            viewer.handles.CP(2)=quiver3(p,p,p,x,y,z,0,'ShowArrowHead','off','Visible','off','Color',viewer.color);
            fun=@(hObject,eventdata) axesButtonDownFunction(hObject,eventdata,viewer,'ControlPoints');
            set(viewer.handles.CP,'ButtonDownFcn',fun)
            
            %create listener for the iso value
            addlistener(viewer,'newIso',@viewer.handlePropEvents);
            
        end
        
        function set.iso(viewer,iso)
            viewer.iso = iso;
            
            %Broadcast that the image has been updated
            notify(viewer,'newIso')
        end
        
        function zdata = get.zdata(viewer)
            [~,~,zdata] = getCartesianCoordinates(viewer.lesion,'PostV');
        end
        
        function handlePropEvents(viewer,src,evnt)
            switch evnt.EventName
                case 'PostSet'
                    slice = round(get(evnt.AffectedObject,'Value'));
                    zdata = viewer.zdata;
                    z = zdata(slice);
                    set(viewer.handles.plane,'ZData',[z z; z z]);
                case 'newImage'
                    updateSurface(viewer)
                case 'newIso'
                    updateSurface(viewer)
            end
        end
        
        function updateSurface(viewer)
            lesion = viewer.lesion;
            tool = viewer.tool;
            [X,Y,Z] = getCartesianCoordinates(lesion,'Post');
            C = lesion.contrast;
            shape = isosurface(X,Y,Z,getImage(tool),viewer.iso*C);
            set(viewer.handles.surface,'vertices',shape.vertices,'faces',shape.faces)
            set(viewer.handles.text_ISO,'String',['ISO = ' num2str(viewer.iso)])
            
            %Update the image plane
            FOV = lesion.FOV;
            Xdata = [-FOV(1)/2 FOV(1)/2;-FOV(1)/2 FOV(1)/2]-lesion.origin(1);
            Ydata = [-FOV(2)/2 -FOV(2)/2 ;FOV(2)/2 FOV(2)/2]-lesion.origin(2);
            set(viewer.handles.plane,'XData',Xdata,'YData',Ydata);
            
            %Update the axis lines
            tbuff = FOV*viewer.tbuff;
            set(viewer.handles.axis(1),'XData',[-FOV(1)/2 FOV(1)/2]-lesion.origin(1));
            set(viewer.handles.axis(2),'Position',[FOV(1)/2+tbuff(1)-lesion.origin(1) 0  0]);
            set(viewer.handles.axis(3),'YData',[-FOV(2)/2 FOV(2)/2]-lesion.origin(2));
            set(viewer.handles.axis(4),'Position',[0 FOV(2)/2+tbuff(2)-lesion.origin(2) 0]);
            set(viewer.handles.axis(5),'ZData',[-FOV(3)/2 FOV(3)/2]-lesion.origin(3));
            set(viewer.handles.axis(6),'Position',[0 0 FOV(3)/2+tbuff(3)-lesion.origin(3)]);
            
            %Update the control points
            controlPoints = lesion.controlPoints;
            x = controlPoints(:,1); y = controlPoints(:,2); z = controlPoints(:,3);
            set(viewer.handles.CP(1),'XData',x,'YData',y,'ZData',z);
            p=zeros(size(x));
            set(viewer.handles.CP(2),'XData',p,'YData',p,'ZData',p,'UData',x,'VData',y,'WData',z);
            
            %Update the axes limits
            set(viewer.handles.Axes,'Xlim',[-FOV(1)/2 FOV(1)/2]-lesion.origin(1),'Ylim',[-FOV(2)/2 FOV(2)/2]-lesion.origin(2),'Zlim',[-FOV(3)/2 FOV(3)/2]-lesion.origin(3))
        end
           
    end
    
end

function axesButtonDownFunction(hObject,eventdata,viewer,label)
%get the parent figure handle
fig = viewer.lesionTool.handles.fig;

%get the current button motion and button up functions of the figure
WBMF_old = get(fig,'WindowButtonMotionFcn');
WBUF_old = get(fig,'WindowButtonUpFcn');

switch label
    case 'Axes'
        Cmode = 'Rotate';
        data.bp = get(0,'PointerLocation');
        [data.Az, data.El] = view(viewer.handles.Axes);
        setptr(gcf,'rotate');
    case 'Surface'
        Cmode = 'Rotate';
        data.bp = get(0,'PointerLocation');
        [data.Az, data.El] = view(viewer.handles.Axes);
        setptr(gcf,'rotate');
    case 'Plane'
        Cmode = 'Rotate';
        data.bp = get(0,'PointerLocation');
        [data.Az, data.El] = view(viewer.handles.Axes);
        setptr(gcf,'rotate');
    case 'ControlPoints'
        Cmode = 'Rotate';
        data.bp = get(0,'PointerLocation');
        [data.Az, data.El] = view(viewer.handles.Axes);
        setptr(gcf,'rotate');
    otherwise
        Cmode = '';
end




%set the new window button motion function and button up function of the figure
fun = @(src,evnt) ButtonMotionFunction(src,evnt,viewer,Cmode,data);
fun2=@(src,evnt)  ButtonUpFunction(src,evnt,viewer,WBMF_old,WBUF_old);
set(fig,'WindowButtonMotionFcn',fun,'WindowButtonUpFcn',fun2);
end

function ButtonMotionFunction(src,evnt,viewer,Cmode,data)
switch Cmode
    case 'Rotate'
        cp = get(0,'PointerLocation');
        dp = cp - data.bp;
        az = data.Az-dp(1);
        el = data.El-dp(2);
        view(viewer.handles.Axes,az,el)
end

end

function ButtonUpFunction(src,evnt,viewer,WBMF_old,WBUF_old)
fig = viewer.lesionTool.handles.fig;
set(fig,'WindowButtonMotionFcn',WBMF_old,'WindowButtonUpFcn',WBUF_old);
setptr(gcf,'arrow');
end

function Slider_Callback(hObject,evnt,viewer)

iso = get(hObject,'Value');
viewer.iso=iso;

end

function Checkbox_Callback(hObject,evnt,viewer,label)
checked = get(hObject,'Value');
switch label
    case 'Axes'
        h=viewer.handles.axis;
        if checked
            set(h,'Visible','on')
        else
            set(h,'Visible','off')
        end
    case 'Grid'
        axes_old = gca;
        h = viewer.handles.Axes;
        axes(h);
        if checked
            axis on
            grid on
            box on
        else
            axis off
        end
        axes(axes_old);
    case 'Slice'
        h=viewer.handles.plane;
        if checked
            set(h,'Visible','on')
        else
            set(h,'Visible','off')
        end
    case 'Shape'
        h=viewer.handles.CP;
        if checked
            set(h,'Visible','on')
        else
            set(h,'Visible','off')
        end
    case 'Lesion'
        h=viewer.handles.surface;
        if checked
            set(h,'Visible','on')
        else
            set(h,'Visible','off')
        end
end

end

function panelResizeFunction(hObject,events,viewer,h)
units=get(viewer.handles.largePanel,'Units');
set(viewer.handles.largePanel,'Units','Pixels')
pos=get(viewer.handles.largePanel,'Position');
set(viewer.handles.largePanel,'Units',units);
set(viewer.handles.toolPanel,'Position',[0 pos(4)-h pos(3) h]);
set(viewer.handles.axesPanel,'Position',[0 0 pos(3) pos(4)-h])
end