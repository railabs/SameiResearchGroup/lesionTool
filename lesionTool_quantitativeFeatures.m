classdef lesionTool_quantitativeFeatures < handle
    %This class stores morphalogic, volumetric, attenuation, and texture
    %features. Requires the QuantitativeTools repository
    
    properties (SetAccess = immutable)
        
        %meta data about the CT images
        OriginalVoxelSize
        
        
        %Settings
        ResampledVoxelSize
        nLevels
        RLmax
        
        
        %Image data
        IcroppedOriginal    %Original cropped image data
        maskOriginal       %Original cropped mask
        IcroppedResampled   %Resampled cropped image data
        maskResampled      %Resampled cropped mask
        
        %Morphology features
        Radius
        Volume
        ApproximateVolume
        SurfaceArea
        SurfaceAreaToVolumeRatio
        Sphericity
        SphericalDisproportion
        Asphericity
        Spiculation
        DiscreteCompactness
        Compactness1
        Compactness2
        MajorLength
        MinorLength
        LeastLength
        Elongation
        Flatness
        EllipsoidSurfaceArea
        EllipsoidVolume
        SurfaceAreaToEllipsoidSurfaceAreaRatio
        VolumeToEllipsoidVolumeRatio
        
        %Texture features
        TextureEnergy
        TextureContrast
        TextureCorrelation
        TextureHomogeneity
        TextureVariance
        TextureSumAverage
        TextureEntropy
        TextureDissimilarity
        TextureShortRunEmphasis
        TextureLongRunEmphasis
        TextureGrayLevelNonuniformity
        TextureRunLengthNonuniformity
        TextureRunPercentage
        TextureLowGrayLevelRunEmphasis
        TextureHighGrayLevelRunEmphasis
        TextureShortRunLowGrayLevelEmphasis
        TextureShortRunHighGrayLevelEmphasis
        TextureLongRunLowGrayLevelEmphasis
        TextureLongRunHighGrayLevelEmphasis
        TextureGrayLevelVariance
        TextureRunLengthVariance
        
        %Attenuation/HU histogram features
        HUmean
        HUmin
        HUmax
        HUstd
        HUentropy
        HUkurtosis
        HUskewness
        
        %Other properties
        featurePropertyNames    %This is a nFeatures x 1 cell array of strings matching the names of all the measured feature properties stored in this class
 
    end
    
    properties (Dependent = true)
        featureValues           %This is a nFeatures x 1 vector off all the feature values
        featureValuesLabels     %This is a nFeatures x 1 cell array of feature names (Nicely formatted for human readers)
        featuresInStruct        %Returns a structured variable with a field for each feature value
    end
    
    methods
        function res = lesionTool_quantitativeFeatures(I,mask,pspacing,pspacing_resampled,nLevels,RLmax) %Constructor
            
            %Set the meta data and settings properties
            res.OriginalVoxelSize=pspacing;
            res.ResampledVoxelSize=pspacing_resampled;
            res.nLevels = nLevels;
            res.RLmax = RLmax;
            
            %Crop the image and mask to the bounding box of the mask
            [Y,X,Z]=ind2sub(size(I),find(mask));
            xmin = min(X);
            xmax = max(X);
            clear X;
            ymin = min(Y);
            ymax = max(Y);
            clear Y;
            zmin = min(Z);
            zmax = max(Z);
            clear Z;
            I = I(ymin:ymax,xmin:xmax,zmin:zmax);
            mask = mask(ymin:ymax,xmin:xmax,zmin:zmax);
            res.IcroppedOriginal=I;
            res.maskOriginal=mask;
            
            %Resample
            x = 0:size(I,2)-1; 
            y = 0:size(I,1)-1; 
            z = 0:size(I,3)-1;
            scale = pspacing_resampled ./ pspacing;
            xq = 0:scale(1):size(I,2)-1;
            yq = 0:scale(2):size(I,1)-1;
            zq = 0:scale(3):size(I,3)-1;
            [xq,yq,zq]=meshgrid(xq,yq,zq);
            I = interp3(x,y,z,I,xq,yq,zq,'linear');
            mask = logical(interp3(x,y,z,single(mask),xq,yq,zq,'nearest'));
            res.IcroppedResampled=I;
            res.maskResampled = mask;
            
            %Get the morphology features (uses QuantitativeTools repository)
            resultsMorphology = getMorphologyFeatures(res.maskResampled,'psize',pspacing_resampled);
            res.Radius = resultsMorphology.Radius;
            res.Volume = resultsMorphology.Volume;
            res.ApproximateVolume = resultsMorphology.ApproximateVolume;
            res.SurfaceArea = resultsMorphology.SurfaceArea;
            res.SurfaceAreaToVolumeRatio = resultsMorphology.SurfaceAreaToVolumeRatio;
            res.Sphericity = resultsMorphology.Sphericity;
            res.SphericalDisproportion = resultsMorphology.SphericalDisproportion;
            res.Asphericity = resultsMorphology.Asphericity;
            res.Spiculation = resultsMorphology.Spiculation;
            res.DiscreteCompactness = resultsMorphology.DiscreteCompactness;
            res.Compactness1 = resultsMorphology.Compactness1;
            res.Compactness2 = resultsMorphology.Compactness2;
            res.MajorLength = resultsMorphology.MajorLength;
            res.MinorLength = resultsMorphology.MinorLength;
            res.LeastLength = resultsMorphology.LeastLength;
            res.Elongation = resultsMorphology.Elongation;
            res.Flatness = resultsMorphology.Flatness;
            res.EllipsoidSurfaceArea = resultsMorphology.EllipsoidSurfaceArea;
            res.EllipsoidVolume = resultsMorphology.EllipsoidVolume;
            res.SurfaceAreaToEllipsoidSurfaceAreaRatio = resultsMorphology.SurfaceAreaToEllipsoidSurfaceAreaRatio;
            res.VolumeToEllipsoidVolumeRatio = resultsMorphology.VolumeToEllipsoidVolumeRatio;
            
            %Get the texture features (uses QuantitativeTools repository)
            resultsTexture = getTextureFeatures(res.IcroppedResampled,'mask',res.maskResampled,'nLevels',res.nLevels,'RLmax',res.RLmax);
            res.TextureEnergy = resultsTexture.Energy;
            res.TextureContrast = resultsTexture.Contrast;
            res.TextureCorrelation = resultsTexture.Correlation;
            res.TextureHomogeneity = resultsTexture.Homogeneity;
            res.TextureVariance = resultsTexture.Variance;
            res.TextureSumAverage = resultsTexture.SumAverage;
            res.TextureEntropy = resultsTexture.TextureEntropy;
            res.TextureDissimilarity = resultsTexture.Dissimilarity;
            res.TextureShortRunEmphasis = resultsTexture.SRE;
            res.TextureLongRunEmphasis = resultsTexture.LRE;
            res.TextureGrayLevelNonuniformity = resultsTexture.GLN;
            res.TextureRunLengthNonuniformity = resultsTexture.RLN;
            res.TextureRunPercentage = resultsTexture.RP;
            res.TextureLowGrayLevelRunEmphasis = resultsTexture.LGRE;
            res.TextureHighGrayLevelRunEmphasis = resultsTexture.HGRE;
            res.TextureShortRunLowGrayLevelEmphasis = resultsTexture.SRLGE;
            res.TextureShortRunHighGrayLevelEmphasis = resultsTexture.SRHGE;
            res.TextureLongRunLowGrayLevelEmphasis = resultsTexture.LRLGE;
            res.TextureLongRunHighGrayLevelEmphasis = resultsTexture.LRHGE;
            res.TextureGrayLevelVariance = resultsTexture.GLV;
            res.TextureRunLengthVariance = resultsTexture.RLV;
            
            %Get attenuation features
            vals = res.IcroppedResampled(res.maskResampled); %Get only the HU values within the mask
            res.HUmean = mean(vals);
            res.HUmin = min(vals);
            res.HUmax = max(vals);
            res.HUstd = std(vals);
            res.HUentropy= entropy(vals);
            res.HUkurtosis = kurtosis(vals);
            res.HUskewness = skewness(vals);
            
            %Set the featurePropertyNames property
            res.featurePropertyNames = {...
                    'HUmean';...
                    'HUmin';...
                    'HUmax';...
                    'HUstd';...
                    'HUentropy';...
                    'HUkurtosis';...
                    'HUskewness';...
                    'Radius';...
                    'Volume';...
                    'ApproximateVolume';...
                    'SurfaceArea';...
                    'SurfaceAreaToVolumeRatio';...
                    'Sphericity';...
                    'SphericalDisproportion';...
                    'Asphericity';...
                    'Spiculation';...
                    'DiscreteCompactness';...
                    'Compactness1';...
                    'Compactness2';...
                    'MajorLength';...
                    'MinorLength';...
                    'LeastLength';...
                    'Elongation';...
                    'Flatness';...
                    'EllipsoidSurfaceArea';...
                    'EllipsoidVolume';...
                    'SurfaceAreaToEllipsoidSurfaceAreaRatio';...
                    'VolumeToEllipsoidVolumeRatio';...
                    'TextureEnergy';...
                    'TextureContrast';...
                    'TextureCorrelation';...
                    'TextureHomogeneity';...
                    'TextureVariance';...
                    'TextureSumAverage';...
                    'TextureEntropy';...
                    'TextureDissimilarity';...
                    'TextureShortRunEmphasis';...
                    'TextureLongRunEmphasis';...
                    'TextureGrayLevelNonuniformity';...
                    'TextureRunLengthNonuniformity';...
                    'TextureRunPercentage';...
                    'TextureLowGrayLevelRunEmphasis';...
                    'TextureHighGrayLevelRunEmphasis';...
                    'TextureShortRunLowGrayLevelEmphasis';...
                    'TextureShortRunHighGrayLevelEmphasis';...
                    'TextureLongRunLowGrayLevelEmphasis';...
                    'TextureLongRunHighGrayLevelEmphasis';...
                    'TextureGrayLevelVariance';...
                    'TextureRunLengthVariance';...
                    };
            
                        
        end
        
        function vals = get.featureValues(res)
            
            str = res.featurePropertyNames;
            vals = NaN(size(str));
            for i=1:size(str,1)
                vals(i,1) = res.(str{i});
            end
   
            
        end
        
        function labs = get.featureValuesLabels(res)
            
            labs={};
            
            for i=1:length(res.featurePropertyNames)
                switch res.featurePropertyNames{i}
                    case 'HUmean'
                        labs{end+1} ='Mean HU';
                    case 'HUmin'
                        labs{end+1} ='Min HU';
                    case 'HUmax'
                        labs{end+1} ='Max HU';
                    case 'HUstd'
                        labs{end+1} ='STD HU';
                    case 'HUentropy'
                        labs{end+1} ='Entropy HU';
                    case 'HUkurtosis'
                        labs{end+1} ='Kurtosis HU';
                    case 'HUskewness'
                        labs{end+1} ='Skewness HU';
                    case 'ApproximateVolume'
                        labs{end+1} ='Approximate Volume';
                    case 'SurfaceArea'
                        labs{end+1} ='Surface Area';
                    case 'SurfaceAreaToVolumeRatio'
                        labs{end+1} ='Surface Area To Volume Ratio';
                    case 'SphericalDisproportion'
                        labs{end+1} ='Spherical Disproportion';
                    case 'DiscreteCompactness'
                        labs{end+1} = 'Discrete Compactness';
                    case 'MajorLength'
                        labs{end+1} ='Major Length';
                    case 'MinorLength'
                        labs{end+1} ='Minor Length';
                    case 'LeastLength'
                        labs{end+1} ='Least Length';
                    case 'EllipsoidSurfaceArea'
                        labs{end+1} ='Ellipsoid Surface Area';
                    case 'EllipsoidVolume'
                        labs{end+1} ='Ellipsoid Volume';
                    case 'SurfaceAreaToEllipsoidSurfaceAreaRatio'
                        labs{end+1} ='Surface Area To Ellipsoid Surface Area Ratio';
                    case 'VolumeToEllipsoidVolumeRatio'
                        labs{end+1} ='Volume To Ellipsoid Volume Ratio';
                    case 'TextureEnergy'
                        labs{end+1} ='Texture Energy';
                    case 'TextureContrast'
                        labs{end+1} ='Texture Contrast';
                    case 'TextureCorrelation'
                        labs{end+1} ='Texture Correlation';
                    case 'TextureHomogeneity'
                        labs{end+1} ='Texture Homogeneity';
                    case 'TextureVariance'
                        labs{end+1} ='Texture Variance';
                    case 'TextureSumAverage'
                        labs{end+1} = 'Texture Sum Average';
                    case 'TextureEntropy'
                        labs{end+1} ='Texture Entropy';
                    case 'TextureDissimilarity'
                        labs{end+1} ='Texture Dissimilarity';
                    case 'TextureShortRunEmphasis'
                        labs{end+1} ='Texture Short Run Emphasis';
                    case 'TextureLongRunEmphasis'
                        labs{end+1} ='Texture Long Run Emphasis';
                    case 'TextureGrayLevelNonuniformity'
                        labs{end+1} ='Texture Gray Level Nonuniformity';
                    case 'TextureRunLengthNonuniformity'
                        labs{end+1} ='Texture Run Length Nonuniformity';
                    case 'TextureRunPercentage'
                        labs{end+1} ='Texture Run Percentage';
                    case 'TextureLowGrayLeveRunEmphasis'
                        labs{end+1} ='Texture Low Gray Level Run Emphasis';
                    case 'TextureHighGrayLevelRunEmphasis'
                        labs{end+1} ='Texture High Gray Level Run Emphasis';
                    case 'TextureShortRunLowGrayLevelEmphasis'
                        labs{end+1} ='Texture Short Run Low Gray Level Emphasis';
                    case 'TextureShortRunHighGrayLevelEmphasis'
                        labs{end+1} ='Texture Short Run High Gray Level Emphasis';
                    case 'TextureLongRunLowGrayLevelEmphasis'
                        labs{end+1} ='Texture LongRun LowGray Level Emphasis';
                    case 'TextureLongRunHighGrayLevelEmphasis'
                        labs{end+1} ='Texture Long Run High Gray Level Emphasis';
                    case 'TextureGrayLevelVariance'
                        labs{end+1} ='Texture Gray Level Variance';
                    case 'TextureRunLengthVariance'
                        labs{end+1} ='Texture Run Length Variance';
                    otherwise
                        labs{end+1} = res.featurePropertyNames{i};
                end
            end
            labs = labs';
        end
        
        function out = get.featuresInStruct(res)
            val = res.featureValues;
            for i=1:length(val)
                out.(res.featurePropertyNames{i})=val(i);
            end
        end
    end
    
end