classdef NPSobject < handle
    
    properties
        a = 20;
        b = 2;
        c = 7;
        d = 1;
        Ny = [1 1 1];      %nyquist frequency in the [x y z] direction
        N = [50 50 50];    %number of pixels in the [x y z] direction
    end
    
    properties (Dependent = true)
        nps
        noise
        df
        fx      %Frequency axis 
        fy
        fz
    end
    
    events
        NPSPropertyChanged
    end
    
    methods
        
        function nps = get.nps(NPS)
            a=NPS.a;
            b=NPS.b;
            c=NPS.c;
            d=NPS.d;
            Ny=NPS.Ny;
            N=NPS.N;
            fx=NPS.fx;
            fy=NPS.fy;
            fz=abs(NPS.fz);
            if prod(N)<=prod([512 512 100]);
                [Fx,Fy,Fz]=meshgrid(fx,fy,fz);
                [~,Fr] = cart2pol(Fx,Fy);
                B=Fz*d+b;
                C=(B./b)*c;
                fun = @(Fr) a./((B./C).^B.*exp(-B)).*Fr.^B.*exp(-C.*Fr);
                nps = fun(Fr);
            else
                [Fx,Fy]=meshgrid(fx,fy);
                [~,Fr] = cart2pol(Fx,Fy);
                for i=1:N(3)
                    Fz=fz(i);
                    B=Fz*d+b;
                    C=(B./b)*c;
                    fun = @(Fr) a./((B./C).^B.*exp(-B)).*Fr.^B.*exp(-C.*Fr);
                    nps(:,:,i)=fun(Fr);
                end
            end
        end
        
        function [fx,fz,npsz]=getZnps(NPS)
            a=NPS.a;
            b=NPS.b;
            c=NPS.c;
            d=NPS.d;
            Ny=NPS.Ny;
            N=NPS.N;
            fx=NPS.fx;
            fy=0;
            fz=NPS.fz;
            [Fx,Fy,Fz]=meshgrid(fx,fy,abs(fz));
            [~,Fr] = cart2pol(Fx,Fy);
            B=Fz*d+b;
            C=(B./b)*c;
            fun = @(Fr) a./((B./C).^B.*exp(-B)).*Fr.^B.*exp(-C.*Fr);
            npsz = fun(Fr);
            npsz = permute(npsz,[3 2 1]);
            fx=[-Ny(1) Ny(1)];
            fz=[-Ny(3) Ny(3)];
        end
        
        function [nps,fx,fy] = get2DNPS(NPS)
            a=NPS.a;
            b=NPS.b;
            c=NPS.c;
            d=NPS.d;
            Ny=NPS.Ny;
            N=NPS.N;
            fx=NPS.fx;
            fy=NPS.fy;
            fz=0;
            [Fx,Fy,Fz]=meshgrid(fx,fy,fz);
            [~,Fr] = cart2pol(Fx,Fy);
            B=Fz*d+b;
            C=(B./b)*c;
            fun = @(Fr) a./((B./C).^B.*exp(-B)).*Fr.^B.*exp(-C.*Fr);
            nps = fun(Fr);
        end
        
        function [Fr, npsxy] = getNPSprofile(NPS)
            a=NPS.a;
            b=NPS.b;
            c=NPS.c;
            d=NPS.d;
            Ny=NPS.Ny;
            N=NPS.N;
            Fr=linspace(0,Ny(1),N(1));
            Fz=0;
            B=Fz*d+b;
            C=(B./b)*c;
            fun = @(Fr) a/((B./C).^B.*exp(-B)).*Fr.^B.*exp(-C.*Fr);
            npsxy = fun(Fr);
            
        end
        
        function [fr, npsxy] = getNPSfilterprofile(NPS)
            a=NPS.a;
            b=NPS.b;
            c=NPS.c;
            d=NPS.d;
            Ny=NPS.Ny;
            N=NPS.N;
            fr = NPS.fx;
            Fr=abs(fr);
            Fz=0;
            B=Fz*d+b;
            C=(B./b)*c;
            fun = @(Fr) a/((B./C).^B.*exp(-B)).*Fr.^B.*exp(-C.*Fr);
            npsxy = fun(Fr);
        end
        
        function set.a(NPS,a)
            NPS.a=a;
            notify(NPS,'NPSPropertyChanged')
        end
        
        function set.b(NPS,b)
            NPS.b=b;
            notify(NPS,'NPSPropertyChanged')
        end
        
        function set.c(NPS,c)
            NPS.c=c;
            notify(NPS,'NPSPropertyChanged')
        end
        
        function set.d(NPS,d)
            NPS.d=d;
            notify(NPS,'NPSPropertyChanged')
        end
        
        function noise = get.noise(NPS)
            nps = NPS.nps;
            df = NPS.df;
            df = prod(df);
            
            noise = sqrt(sum(nps(:))*df);
            
        end
        
        function df = get.df(NPS)
            df = (2*NPS.Ny)./(NPS.N-1);
            df(isinf(df))=1;
        end
        
        function fx = get.fx(NPS)
            fx = getFrequencyAxis(NPS.Ny(1),NPS.N(1),'shifted');
        end
        
        function fy = get.fy(NPS)
            fy = getFrequencyAxis(NPS.Ny(2),NPS.N(2),'shifted');
        end
        
        function fz = get.fz(NPS)
            fz = getFrequencyAxis(NPS.Ny(3),NPS.N(3),'shifted');
        end
        
        function setNoise(NPS,noise)
            %adjusts the a that would result in a given noise
            NPS.a = NPS.a*((noise/NPS.noise)^2);
            
        end
        
        function newNPS = copy(NPS)
            newNPS=NPSobject;
            newNPS.a=NPS.a;
            newNPS.b=NPS.b;
            newNPS.c=NPS.c;
            newNPS.d=NPS.d;
            newNPS.Ny=NPS.Ny;
            newNPS.N=NPS.N;
        end
        
        function [fr, npsr] = get1DRadialNPSFilter(NPS,psize,N)
            NPS = copy(NPS);
            NPS.N(1)=N;
            NPS.Ny(1) = 1/(2*psize);
            [fr, npsr] = getNPSfilterprofile(NPS);
            fr=fr';
            npsr=sqrt(sqrt(npsr)');
        end
        
        function Pfilt = filterProjectionsWithNPS(NPS,P,psize)
            %P is a nDetectors x nAngles x nSlices sinogram. Filtering only happens
            %accross detectors, not accross angles or slices.
            
            %Take fourier transform of projections
            F = fft(P);
            
            %Remove DC (i.e., substract mean)
            F(1,:,:)=0;
            
            %Create filter
            [~,npsr]=get1DRadialNPSFilter(NPS,psize,size(P,1));
            npsr=fftshift(npsr);
            npsr = repmat(npsr,[1 size(P,2) size(P,3)]);
            
            %Apply filtering
            F = F.*npsr;
            Pfilt = real(ifft(F));
            
            
        end
        
        function I = filterImagewithNPS(NPS,I,psize)
            N=[size(I,2) size(I,1) size(I,3)];
            noise=NPS.noise;
            NPS=copy(NPS);
            NPS.N=N;
            NPS.Ny=1./(2*psize);
            setNoise(NPS,noise);
            
            %get the NPS filter
            if N(3)==1
                nps = fftshift(get2DNPS(NPS));
            else
                nps=fftshift(NPS.nps);
            end
            
            %Remove mean
            MV = mean(I(:));
            I=I-MV;
            I = fftn(I);
            I(1)=0; %Redundant? 
            I = I.*sqrt(nps); clear nps;
            I = real(ifftn(I));
            
        end
        
        function I = generateNoise(NPS,N,psize)
            
            %Generate white noise image
            if prod(N)<=prod([512 512 100])
                I=random('norm',0,1,[N(2) N(1) N(3)]);
            else
                I=zeros([N(2) N(1) N(3)]);
                for i=1:N(3)
                    I(:,:,i)=random('norm',0,1,[N(2) N(1)]);
                end
            end
            
            %Filter
            I = filterImagewithNPS(NPS,I,psize);
            
            %Scale the noise to match the desired noise
            if prod(N)<=prod([512 512 100])
                I = I*NPS.noise/std(I(:));
            else
                v=I(1:prod([512 512 100])); scale=std(v);
                I = I*NPS.noise/scale;
            end
            
            
            
        end
        
        function fitNPS2ROI(NPS,I,psize,mode)
            %Mode can be '2D' or '3D'
            %first need to estimate the NPS from the ROI
            
            I = subtractMean(I,mode); %remove mean
            %get ROI info
            psize = [psize(2) psize(1) psize(3)];
            N=size(I);
            
            switch mode
                case '2D'
                    norm = prod(psize(1:2))./prod(N(1:2));
                    ny = 1./(2*psize); %nyquist frequency
                    v = linspace(-ny(1),ny(1),N(1));
                    u = linspace(-ny(2),ny(2),N(2));
                    [U,V] = meshgrid(u,v);
                    [~,F]= cart2pol(U,V);
                    
                    %Get NPS for each slice
                    for i=1:N(3)
                        slice = I(:,:,i);
                        noise(i,1) = std(slice(:));
                        nps = norm*abs(fft2(slice)).^2;
                        nps(1) = 0; %Sets DC component to zero (helps when mean did not get perfectly removed)
                        nps = fftshift(nps);
                        NPS_a(:,:,i) = nps;
                    end
                    noise = mean(noise);
                    F = repmat(F,[1 1 N(3)]);
                    %do the least squares fit
                    ft=fittype('a*x^b*exp(-c*x)');
                    mdl=fit(F(:),NPS_a(:),ft,'StartPoint',[20 2 8],'Lower',[0 0 0]);
                    NPS.b = mdl.b;
                    NPS.c = mdl.c;
                    setNoise(NPS,noise);
                    
                    f = linspace(0,max(F(:)));
                    [xbinned, ybinned, ~] = rebinData(F(:), NPS_a(:), f);
                    figure;
                    scatter(xbinned,ybinned); hold on
                    plot(xbinned,mdl(xbinned));
                    xlabel('f (mm^-^1)');
                    ylabel('NPS (HU^2mm^2)')
                    legend('Measured','Fit')

                case '3D'
                    norm = prod(psize)./prod(N);
            end
            
            
            
            
            
            
            
        end
        
        
        
    end
end

function I  = subtractMean(I,mode)

try     %try polynomial fit (requires polyfitn toolbox)
    switch mode
        case '3D'
            [X,Y,Z] = meshgrid(1:size(I,2),1:size(I,1),1:size(I,3));    %create coordinate system
            indV = [X(:) Y(:) Z(:)];    %independet variables
            dV =  I(:);                 %dependent variable (i.e., pixel values)
            p = polyfitn(indV,dV,2);
            pv = polyvaln(p,indV);
            pv = reshape(pv,size(I));
            I = I-pv;
        case '2D'
            %Fit 2D polynomial to each slice
            [X,Y]=meshgrid(1:size(I,2),1:size(I,1));
            indV=[X(:) Y(:)];
            for i=1:size(I,3)
                dv = I(:,:,i);
                dv = dv(:);
                p = polyfitn(indV,dV,2);
                pv = polyvaln(p,indV);
                pv = reshape(pv,size(I));
                I(:,:,i) = I(:,:,i)-pv;
            end
            
            
    end
    
    
catch   %otherwise just subtract the mean
    switch mode
        case '3D'
        I = I - mean(I(:));
        case '2D'
            for i=1:size(I,3)
                slice=I(:,:,i);
                I(:,:,i)=I(:,:,i)-mean(slice(:));
            end
    end
end

end

function [xbinned, ybinned, variance] = rebinData(x, y, edges)
%This function bins and averages the noisy scattered data

%compute histogram
try
    [~,xbinned,whichBin] = histcounts(x,edges);
catch
    [~,whichBin]= histc(x,edges);
    xbinned = edges;
end

%loop through histogram and average each bin
ybinned = zeros(size(xbinned));
variance = zeros(size(xbinned));
for i=1:length(xbinned)
    binMembers = y(whichBin == i);
    ybinned(i) = mean(binMembers); % this might create NaN. Interp done below takes care of this
    variance(i) = var(binMembers);
end

% NAN will appear after "mean" if binMember is empty, use values of neighboring bin instead
ybinned = interp1(find(~isnan(ybinned)),ybinned(~isnan(ybinned)),1:length(ybinned),'nearest','extrap');
variance = interp1(find(~isnan(variance)),variance(~isnan(variance)),1:length(variance),'nearest','extrap');
end

function f = getFrequencyAxis(Ny,N,mode)

psize = getPsize(Ny);
f = getFFTfrequency(psize,N,mode);

end

function psize = getPsize(Ny)
psize = 1/(2*Ny);
end