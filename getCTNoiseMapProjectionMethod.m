function noise = getCTNoiseMapProjectionMethod(I,D,varargin)
%Get the input parameters
[nhood, T, mu_water,binLimits]=parseInputs(varargin);
nDetectors = 736;       %These are based on a Siemens scanner. May want to generalize but seem to be OK for now
nAngles = 360;
FanCoverage = 'cycle';
FanRotationIncrement = 1;
FanSensorGeometry = 'arc';
FanDetectorSpan = 75; %degrees
FanSensorSpacing = FanDetectorSpan/nDetectors;
ParallelCoverage = FanCoverage;

%Convert image to liner attenuation
im = mu_water + mu_water*I/1000; im(im<0)=0;

%Get map of sum attenuation for rays through each pixel
%Forward Projection
dTh = 360/nAngles;
im = fanbeam(im,D,'FanRotationIncrement',dTh,...
    'FanSensorGeometry',FanSensorGeometry,'FanSensorSpacing',FanSensorSpacing);
%Interpolate to parrallel geometry
[P, xp, theta] = fan2para(im,D,'FanCoverage',FanCoverage,'FanRotationIncrement',FanRotationIncrement,...
    'FanSensorGeometry',FanSensorGeometry,'FanSensorSpacing',FanSensorSpacing,'ParallelCoverage',ParallelCoverage);
%Backproject attenuation (without filtering)
im = iradon(P,theta,'Linear','none',1,size(I,1)); %This is a map of sum attenuation. noise is proportional to this map

%get the intitial noise map
noise=stdfilt(I,nhood);
[P,EDGES] = histcounts(noise(:),'Normalization','probability','BinWidth',1,'BinLimits',binLimits);
EDGES=EDGES(1:end-1) + diff(EDGES)/2;
%Get the mode
[~,ind_mode]=max(P); %index of mode
m = EDGES(ind_mode); %mode
%Get mask of all pixels with noise within �T of mode
mask = noise >= m - T*m & noise <= m + T*m;

scale = mean(noise(mask)./im(mask));

noise = scale*im;

end


function [nhood, T, mu_water,binLimits]=parseInputs(args)

nhood = strel('disk',9,0);
nhood = nhood.Neighborhood;
T = .1;
mu_water = 1.837E-02;
binLimits=[0 200];




if length(args)>1
    for i=1:2:length(args)
        switch args{i}
            case 'NHOOD'
                nhood = strel('disk',ceil(args{i+1}/2),0);
                nhood = nhood.Neighborhood;
            case 'Thresh'
                T=args{i+1};
            case 'mu_water'
                mu_water=args{i+1};
            case 'binLimits'
                binLimits=args{i+1};
            otherwise
                disp(['Did not recognize setting: ' args{i}])
        end
    end
        
end
end