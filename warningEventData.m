classdef (ConstructOnLoad) warningEventData < event.EventData
   properties
      Message
   end

   methods
      function evnt = warningEventData(message)
          evnt.Message = message;
      end
   end
end