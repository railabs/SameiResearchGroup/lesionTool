classdef lesionModel < handle
    
    properties
        shape                   %Structured variable describing the shape
        profile                 %Structured variable describing the profile
        blur                    %Structured variable describing the edge blur
        texture                 %Structured variable describing the internal texture
        FOV                     %1x3 vector denoting the FOV in the x, y,and z directions
        n                       %1x3 vector denoting the number of initial voxels in the x,y, and z direction
        origin=[0 0 0];         %This moves the origin of the coordinate system defined when voxelizing the model
        scale                   %factor by which to upsample after the initial voxelization (only use ints)
        interpType              %String denoting the type of interpolation to do when upsampling
        reducePatchFactor=.2;   %Factor by which the patch faces are reduced when getting the control points
        iso = .1;               %Isovalue factor at which the volume and surface area are measured.
        type                    %String identifying the type of lesion (e.g. 'liver')
        tag                     %String of additional id tag (e.g. 'Siemens Trial')
        name                    %String of an name given to the lesion
    end
    
    properties (Dependent = true, Access = protected)
        I                       %Voxelized image data
        R                       %Voxelized radius data (i.e, the distance of each voxel from the center)
    end
    
    properties (Dependent = true)
        psize                   %the pixel size in each dimension (after upsampling)
        psize_pre               %the pixel size in each dimension (before upsampling)
        n_up                    %1x3 vector denoting the number of upsamples voxels
        volume                  %volume in mm
        surfaceArea             %surface area in mm^3
        maxDiameter             %maximum in-plane diameter in mm
        meanContrast            %Contrast within the iso-core of the nodule. 
        mask                    %isosurface mask. used to make other measures of the lesion like volume, surface area, diameter, and contrast. depends on iso and turns off texture to make the mask
        shapeType               %String denoting the type of shape function ('Spoke' or 'Mesh')
        controlPoints           %Nx3 vector of control points in cartesian coordinates
        profileType             %String denoting the type of contrast profile
        contrast                %contrast of the lesion (Pre blur)
        blurType                %String denoting the type of blurring function ('Gaussian')
        textureType             %String denoting the type of internal texture
        
    end
    
    events
        voxelizing              %is broadcase before the lesion is about to be voxelized
        voxelized               %is broadcast after the lesion has been voxelized
    end
    
    methods
        
        %Constructor
        function model = lesionModel(varargin)
            
            [model.shape, model.profile, model.blur, model.FOV, model.n, model.scale, model.interpType, model.texture, model.type, model.tag, model.name] = parseInputs(varargin);
            
            
        end
        
        function I = get.I(model)
            %This function voxelizes the lesion
            
            %Broadcast event that the model is beginning to voxelize
            notify(model,'voxelizing')
            
            %Get the radius of each voxel
            [r,~,~] = getSphericalCoordinates(model);
            
            %Get the lesion radius for each voxel
            R = model.R;
            
            %Evaluate initial contrast profile equation
            switch model.profileType
                case 'Designer'
                    C = model.profile.C;
                    n = model.profile.n;
                    I = designerNodule(r,R,n,1);
                case 'Flat'
                    switch model.shapeType
                        case 'Simple Mesh'
                            I = R;
                        otherwise
                            I = double(r<=R);
                    end
                    
                otherwise
                    warning('Profile type is not supported')
            end
            
            %Blur according to the blurring model
            switch model.blurType
                case 'Gaussian'
                    I = gaussianBlurLesion(I,model.blur.PSF,model.psize_pre);
                case 'None'
                otherwise
                    warning('Blur type is not supported')
            end
            
            %Add texture according the to texture model
            T = getTextureImage(model);
            I = I + I.*T;
            
            %Rescale the lesion data to match the desired overall contrast
            %I = mat2gray(I);
            I=I*model.contrast;
            
            %upsample the image according to the scale
            if model.scale > 1
                [X,Y,Z] = getCartesianCoordinates(model,'Pre');
                [Xq,Yq,Zq] = getCartesianCoordinates(model,'Post');
                I = interp3(X,Y,Z,I,Xq,Yq,Zq,model.interpType,0);
            end
            
            %Broadcast event that the model has finshed voxelizing
            notify(model,'voxelized')
            
        end
        
        function R = get.R(model)
            %This function returns the size of the lesion in the direction
            %pointing from the origin to each voxel
            
            switch model.shapeType
                case {'Spoke','Mesh'}
                    %get the spherical coordinates of the voxels
                    [~,theta,phi] = getSphericalCoordinates(model);
                    R = getShapeInGivenDirection(model,theta,phi);
                    
                case 'Simple Mesh'
                    [X,Y,Z] = getCartesianCoordinates(model,'PreV');
                    fv.faces = model.shape.faces;
                    center = repmat(model.shape.center,[size(model.shape.vertices,1) 1]);
                    fv.vertices = model.shape.vertices-center;
                    fv.vertices = [fv.vertices(:,2) fv.vertices(:,1) fv.vertices(:,3)];
                    R = double(VOXELISE(Y,X,Z,fv,'xyz'));
                    
                    
                otherwise
                    warning('Shape type is not supported, can be ''Spoke'', ''Mesh'', or ''Simple Mesh'' ')
                    
            end
        end
        
        function R = getShapeInGivenDirection(model,theta,phi)
            switch model.shapeType
                case 'Spoke'
                    %get the base vectors
                    R_b = model.shape.Rb;
                    theta_b = model.shape.theta_b;
                    phi_b=model.shape.phi_b;
                    
                    %Wrap to prep for interpolation
                    [THETA_b, PHI_b, R_b] = wrapSpherical_gridded(theta_b,phi_b,R_b);
                    
                    %Interpolote
                    R=interp2(THETA_b,PHI_b,R_b,theta,phi,'spline');
                    
                    %Filter R_grid to remove artifacts
                    if model.shape.f>0
                        h=fspecial('disk',model.shape.f);
                        R=imfilter(R,h);
                    end
                    
                case {'Mesh','Simple Mesh'}
                    %Get the vertices of the mesh
                    center = repmat(model.shape.center,[size(model.shape.vertices,1) 1]);
                    verts = model.shape.vertices-center;
                    
                    %convert to spherical coordinates
                    [theta_b, phi_b, r_b]=cart2sph(verts(:,1), verts(:,2), verts(:,3));
                    theta_b(theta_b<0)=theta_b(theta_b<0)+2*pi;
                    
                    %Wrap to prep for interpolation
                    [theta_b, phi_b, r_b] = wrapSpherical(theta_b,phi_b,r_b);
                    
                    %Create scattered interpolant
                    F = scatteredInterpolant(theta_b,phi_b,r_b);
                    
                    %interpolate to get R for each theta/phi
                    R = F(theta,phi);
                    
                    
            end
        end
        
        function mask = get.mask(model)
            %Turn off texture if needed
            if ~strcmp(model.textureType,'none')
                model=copy(model);
                temp.type='none';
                model.texture=temp;
            end
            
            %Get the image (normalized by peak contrast);
            im = model.I/model.contrast;
            im = im/max(im(:));
            mask = im>model.iso;
        end
        
        function vol = get.volume(model)
            %Get the mask image
            mask = model.mask;
            
            %calculate volume
            vol = sum(mask(:))*prod(model.psize);
            
        end
        
        function setVolume(model,newVolume,varargin)
            
            %get input parameters
            switch length(varargin)
                case 0
                    maxIterations = 5; %maximum number of generations
                    T=.1; %threshold to stop iterating (if new volume is within this threshold, then stop)
                case 1
                    maxIterations=varargin{1};
                    T=.1;
                case 2
                    maxIterations=varargin{1};
                    T=varargin{2};
            end
            
            oldVol=model.volume;
            if abs(oldVol-newVolume)/oldVol<=T
                withinThreshold = true;
            else
                withinThreshold = false;
            end
            
            n=0;
            while n<maxIterations && ~withinThreshold
                n=n+1;
                scaleFactor = (newVolume/oldVol)^(1/3);
                scaleShape(model,scaleFactor);
                model.FOV=model.FOV*scaleFactor;
                oldVol=model.volume;
                if abs(oldVol-newVolume)/oldVol<=T
                    withinThreshold = true;
                else
                    withinThreshold = false;
                end
            end

            
        end
        
        function maxDiameter = get.maxDiameter(model)            
            %Get the image
            mask =model.mask;
            
            [x,y,z] = getCartesianCoordinates(model,'PostV');
            
            %Get the inplane diameter
            [Y, X, Z]=ind2sub(size(mask),find(mask));


            %max in-slice diameter
            dmax=0;
            for iz=min(Z):max(Z)
                slice=mask(:,:,iz);
                border=edge(slice);
                [yedge, xedge]=find(border);
                for i=1:length(xedge)
                    for j=1:length(xedge)
                        d=sqrt((x(xedge(i))-x(xedge(j)))^2+(y(yedge(i))-y(yedge(j)))^2);
                        if d>dmax
                            dmax=d;
                        end
                    end
                end
            end
            
            maxDiameter=dmax;
            
            
        end
        
        function setMaxDiameter(model,newDiameter)
            
            oldDiameter=model.maxDiameter;
            scaleFactor = newDiameter/oldDiameter;
            scaleShape(model,scaleFactor);
            model.FOV=model.FOV*scaleFactor;
            
            
        end
        
        function meanContrast=get.meanContrast(model)
            
            meanContrast = mean(model.I(model.mask));
            
        end
        
        function setMeanContrast(model,contrast)
            
            scaleFactor = contrast/model.meanContrast;
            model.profile.C = model.profile.C*scaleFactor;
        end
        
        function scaleShape(model,scaleFactor)
            switch model.shapeType
                case {'Simple Mesh','Mesh'}
                    
                    center = repmat(model.shape.center,[size(model.shape.vertices,1) 1]);
                    model.shape.vertices = center + scaleFactor*(model.shape.vertices-center);
                    
                    
                    
                case 'Spoke'
                    
                    model.shape.Rb=model.shape.Rb*scaleFactor;
                    
                otherwise
                    warning('Shape type is not supported, can be ''Spoke'', ''Mesh'', or ''Simple Mesh'' ')
                    
            end
        end
        
        function reorientShape(model,dir)
            %new model will have its x-axis oriented along the vector dir
            
            %normalize dir to make it a unit column vector
            dir = dir/norm(dir);
            if iscolumn(dir)
                dir=dir';
            end
            
             switch model.shapeType
                case {'Simple Mesh','Mesh'}
                    
                    %make the transformation matrix
                    u=dir;
                    wv = null(u)';
                    v = wv(1,:);
                    w = wv(2,:);
                    R = [u;v;w];
                    
                    %Get the vertice data
                    center = repmat(model.shape.center,[size(model.shape.vertices,1) 1]);
                    verts = model.shape.vertices;
                    verts = verts-center;
                    verts = verts'; %verts is now 3 x nPoints;
                    
                    %Apply the transformation;
                    verts = R*verts;
                   
                    %Reshift back to the origin
                    verts= verts'-center;
                    
                    %Update the vertices of the model
                    model.shape.vertices = verts;
               
                    
                otherwise
                    warning([model.shapeType ' Shape type is not supported for the reorientation function, can only be ''Mesh'', or ''Simple Mesh'' '])
                    
            end
        end
        
        function reorientShapeRandomly(model)
            %This defines a random vector (pointed in any 3D direction,
            %uniform distribution) and reorients the shape to have the
            %x-axis line up with that direction.
            
            %Define a random direction in 3D space
            dir = random('norm',0,1,[3 1]); %vector will be normalized by the reorientShape function
            
            %reorient the lesion
            reorientShape(model,dir)
        end
        
        function nudgeShapeOrientation(model,varargin)
            %This function slightly adjusts the orientation of the lesion
            
            %Get the angular limit that the lesion can be nudged
            switch length(varargin)
                case 1
                    phiLim = varargin{1};
                otherwise
                    phiLim = 25;
            end
            phiLim = deg2rad(phiLim);
            phi = random('unif',pi/2-phiLim,pi/2); %This is cone angle away from x axis
            th = random('unif',0,2*pi); %This is azimuthal angle around x axis
            
            [z,y,x] = sph2cart(th,phi,1);
            dir = [x y z];
            reorientShape(model,dir);
            
            
            
        end
               
        function SA = get.surfaceArea(model)
            
            %Turn off texture if needed
            if ~strcmp(model.textureType,'none')
                model=copy(model);
                temp.type='none';
                model.texture=temp;
            end
            
            %get the image
            im = model.I/model.contrast;
            
            %Get isosurface
            [X,Y,Z] = getCartesianCoordinates(model,'Post');
            fv = isosurface(X,Y,Z,im,model.iso);
            
            %Get surface area
            verts=fv.vertices; faces=fv.faces;
            a = verts(faces(:, 2), :) - verts(faces(:, 1), :);
            b = verts(faces(:, 3), :) - verts(faces(:, 1), :);
            c = cross(a, b, 2);
            SA = 1/2 * sum(sqrt(sum(c.^2, 2)));
        end
        
        function model = fitModelToCTData(model,im,mask,psize, varargin)
            %work on a copy of the model
            model = copy(model);
            
            %Get NPS and MTF objects
            [mtf,nps]=parseFitinputs(varargin);
            
            %Set the FOV and n of the model
            n = size(im); n=[n(2) n(1) n(3)];
            model.FOV = n.*psize;
            model.n=n;
            model.scale=1;
            model.origin=[0 0 0];
            
            %find the center of the mask (this will become the origin of
            %the lesion)
            [x,y,z] =  getCartesianCoordinates(model,'PreV');
            i=1:size(im,2); j=1:size(im,1); k=1:size(im,3);
            stats = regionprops(mask,'Centroid');
            Cx = interp1(i,x,stats.Centroid(1));
            Cy = interp1(j,y,stats.Centroid(2));
            Cz = interp1(k,z,stats.Centroid(3));
            model.origin=[Cx Cy Cz];

            %Get some initial guesses of the contrast and background
            SE = strel(ones(5,5,5));
            mask2 = imerode(mask,SE);
            if sum(mask2(:))>0
                frg= mean(im(mask2));
            else
                frg=mean(im(mask));
            end
            mask2 = imdilate(mask,SE);
            B= mean(im(~mask2));
            C = frg-B;
            
            %Get a mesh of the shape of the mask
            [x,y,z] =  getCartesianCoordinates(model,'PreV');
            [node,elem]=binsurface(mask,3,x,y,z);
            [conn,~,~]=meshconn(elem,size(node,1));
            node=smoothsurf(node,[],conn,5);
            fv.faces = elem;
            fv.vertices = node;
            
            
            %Set bound limits
            lim = .2;  %This controls how far away from initial guess to set the bounds
            nlim = 5;   %upper limit of n for the designer nodule
            blim = 5;
            
            %Set up the fitting function according to the shapeType,
            %profileType, and blurType
            switch model.shapeType
                case 'Spoke'
                    %inerpolate to get initial R_b
                    edge_list=fv.vertices;
                    [theta,phi,r] = cart2sph(edge_list(:,1),edge_list(:,2),edge_list(:,3));
                    theta(theta<0)=theta(theta<0)+2*pi;
                    [theta phi r] = wrapSpherical(theta,phi,r);
                    F = TriScatteredInterp(theta,phi,r);
                    [THETA_b PHI_b]=meshgrid(model.shape.theta_b,model.shape.phi_b);
                    R_b=F(THETA_b, PHI_b);
                    R_b(1,:)=mean(R_b(1,:));
                    R_b(end,:)=mean(R_b(end,:));
                    
                    %set up the intial guess and bounds
                    Rb2=R_b(2:end-1,:); Rb2=Rb2(:); Rb2(end+1)=R_b(1,1);Rb2(end+1)=R_b(end,1);
                    switch model.profileType
                        case 'Designer'
                            switch model.blurType
                                case 'None'
                                    %[C; B; n; Rb]
                                    x0=[C; B; 1; Rb2];
                                    ub=[C+abs(C)*lim; B + abs(B)*lim; nlim; Rb2+lim*Rb2];
                                    lb=[C-abs(C)*lim; B - abs(B)*lim; 0; Rb2-lim*Rb2]; temp=lb(4:end); temp(temp<0)=0; lb(4:end)=temp;
                                case 'Gaussian'
                                    %[C; B; n; bx; by; bz; Rb]
                                    x0=[C; B; 1; 1; 1; 1; Rb2];
                                    ub=[C+abs(C)*lim; B + abs(B)*lim; nlim; blim; blim; blim; Rb2+lim*Rb2];
                                    lb=[C-abs(C)*lim; B - abs(B)*lim; 0; 0; 0; 0; Rb2-lim*Rb2]; temp=lb(7:end); temp(temp<0)=0; lb(7:end)=temp;
                            end
                        case 'Flat'
                            switch model.blurType
                                case 'None'
                                    %[C; B; Rb]
                                    x0=[C; B; Rb2];
                                    ub=[C+abs(C)*lim; B + abs(B)*lim; Rb2+lim*Rb2];
                                    lb=[C-abs(C)*lim; B - abs(B)*lim; Rb2-lim*Rb2]; temp=lb(3:end); temp(temp<0)=0; lb(3:end)=temp;
                                case 'Gaussian'
                                    %[C; B; bx; by; bz; Rb]
                                    x0=[C; B; 1; 1; 1; Rb2];
                                    ub=[C+abs(C)*lim; B + abs(B)*lim; blim; blim; blim; Rb2+lim*Rb2];
                                    lb=[C-abs(C)*lim; B - abs(B)*lim; 0; 0; 0; Rb2-lim*Rb2]; temp=lb(6:end); temp(temp<0)=0; lb(6:end)=temp;
                            end
                    end
                    
                case {'Mesh', 'Simple Mesh'}
                    shape.type = model.shapeType;
                    shape.faces=fv.faces;
                    shape.vertices=fv.vertices;
                    shape.center = [0 0 0];
                    model.shape=shape;
                    
                    switch model.profileType
                        case 'Designer'
                            switch model.blurType
                                case 'None'
                                    %[C; B; n]
                                    x0=[C; B; 1];
                                    ub=[C+abs(C)*lim; B + abs(B)*lim; nlim];
                                    lb=[C-abs(C)*lim; B - abs(B)*lim; 0];
                                case 'Gaussian'
                                    %[C; B; n; bx; by; bz;]
                                    x0=[C; B; 1; 1; 1; 1];
                                    ub=[C+abs(C)*lim; B + abs(B)*lim; nlim; blim; blim; blim;];
                                    lb=[C-abs(C)*lim; B - abs(B)*lim; 0; 0; 0; 0];
                            end
                        case 'Flat'
                            switch model.blurType
                                case 'None'
                                    %[C; B]
                                    x0=[C; B];
                                    ub=[C+abs(C)*lim; B + abs(B)*lim];
                                    lb=[C-abs(C)*lim; B - abs(B)*lim];
                                case 'Gaussian'
                                    %[C; B; bx; by; bz;]
                                    x0=[C; B; 1; 1; 1];
                                    ub=[C+abs(C)*lim; B + abs(B)*lim; blim; blim; blim;];
                                    lb=[C-abs(C)*lim; B - abs(B)*lim; 0; 0; 0];
                            end
                    end
            end
            
            %compute the weights
            perim = bwperim(mask);
            SE = strel(ones(3,3,3)); mask2 = imdilate(perim,SE); mask2=perim;
            weights=bwdist(mask2);  rmax=max(weights(:)); weights=rmax-weights;
            weights=mat2gray(weights);
            %weights=bwdist(mask);
            %weights=mat2gray(weights);
            
            
            fun = @(x)computeFitError(x,model,im,weights,nps,mtf,'Error');
            options = optimset('Algorithm','trust-region-reflective','Display','off');
            [x,resnorm,residual,exitflag,output,lambda,jacobian] = lsqnonlin(fun,x0,lb,ub,options);
            
            model = computeFitError(x,model,im,weights,nps,mtf,'Model');
            
            model.n=model.n*2;
            model.scale=2;
            
            
            
            
        end
        
        function psize = get.psize(model)
            %This function returns the pixel size in each dimension
            psize=model.FOV./(model.n*model.scale);
        end
        
        function psize_pre = get.psize_pre(model)
            psize_pre = model.FOV./model.n;
        end
        
        function shapeType = get.shapeType(model)
            shapeType = model.shape.type;
        end
        
        function controlPoints = get.controlPoints(model)
            
            switch model.shapeType
                case 'Spoke'
                    %get base vectors
                    R = model.shape.Rb;
                    theta = model.shape.theta_b;
                    phi=model.shape.phi_b;
                    [THETA, PHI]=meshgrid(theta,phi);
                    R(1,2:end)=nan; R(end,2:end)=nan;
                    R=R(:); THETA = THETA(:); PHI=PHI(:);
                    ind = ~isnan(R);
                    R=R(ind); THETA=THETA(ind); PHI=PHI(ind);
                    [x, y, z]=sph2cart(THETA(:),PHI(:),R(:));
                    controlPoints = [x y z];
                case {'Mesh', 'Simple Mesh'}
                    %Reduce the number of triangles
                    fv.faces=model.shape.faces;
                    fv.vertices=model.shape.vertices;
                    fv = reducepatch(fv,model.reducePatchFactor);
                    center = repmat(model.shape.center,[size(fv.vertices,1) 1]);
                    fv.vertices = fv.vertices-center;
                    
                    %create the triangulation
                    TR = triangulation(fv.faces,fv.vertices);
                    
                    %Get the face centers
                    controlPoints = incenter(TR);
                    
                    
                otherwise
                    warning('Shape type is not supported')
                       
            end
            
        end
        
        function contrast = get.contrast(model)
            contrast =model.profile.C;
        end
        
        function profileType = get.profileType(model)
            profileType = model.profile.type;
        end
        
        function blurType = get.blurType(model)
            blurType = model.blur.type;
        end
        
        function textureType = get.textureType(model)
            if isempty(model.texture)
                textureType='none';
            else
                textureType = model.texture.type;
            end
        end
        
        function n_up = get.n_up(model)
            n_up = model.n.*model.scale;
        end
        
        function [r,theta,phi] = getSphericalCoordinates(model)
            %Make a cartesian Coordinate system 
            [X,Y,Z] = getCartesianCoordinates(model,'Pre');
            %convert to spherical coordinates
            [theta phi r]=cart2sph(X, Y, Z);
            theta(theta<0)=theta(theta<0)+2*pi;
            
        end
        
        function  [X,Y,Z] = getCartesianCoordinates(model,flag)
            FOV = model.FOV;
            switch flag
                case 'Pre'
                    n = model.n;
                    psize=FOV./n;
                    x=((0:n(1)-1) +.5)*psize(1)-(FOV(1)/2+model.origin(1));
                    y=((0:n(2)-1) +.5)*psize(2)-(FOV(2)/2+model.origin(2));
                    z=((0:n(3)-1) +.5)*psize(3)-(FOV(3)/2+model.origin(3));
                    [X,Y,Z] = meshgrid(x,y,z);
                case 'Post'
                    n = model.n*model.scale;
                    psize=FOV./n;
                    x=((0:n(1)-1) +.5)*psize(1)-(FOV(1)/2+model.origin(1));
                    y=((0:n(2)-1) +.5)*psize(2)-(FOV(2)/2+model.origin(2));
                    z=((0:n(3)-1) +.5)*psize(3)-(FOV(3)/2+model.origin(3));
                    [X,Y,Z] = meshgrid(x,y,z);
                case 'PreV'
                    n = model.n;
                    psize=FOV./n;
                    X=((0:n(1)-1) +.5)*psize(1)-(FOV(1)/2+model.origin(1));
                    Y=((0:n(2)-1) +.5)*psize(2)-(FOV(2)/2+model.origin(2));
                    Z=((0:n(3)-1) +.5)*psize(3)-(FOV(3)/2+model.origin(3));
                case 'PostV'
                    n = model.n*model.scale;
                    psize=FOV./n;
                    X=((0:n(1)-1) +.5)*psize(1)-(FOV(1)/2+model.origin(1));
                    Y=((0:n(2)-1) +.5)*psize(2)-(FOV(2)/2+model.origin(2));
                    Z=((0:n(3)-1) +.5)*psize(3)-(FOV(3)/2+model.origin(3));
            end
            
            
           
            
        end
        
        function I = voxelizeLesion(model)
            I = model.I;
        end
        
        function [rowNames,columnNames,columnWidths] = getShapeTableNames(model)
            switch model.shapeType
                case 'Spoke'
                    theta_b = model.shape.theta_b;
                    for i=1:length(theta_b)
                        columnNames{i} = num2str(theta_b(i),3);
                        columnWidths{i} = 60;
                    end
                    phi_b = model.shape.phi_b;
                    for i=1:length(phi_b)
                        rowNames{i} = num2str(phi_b(i),3);
                        
                    end
                otherwise
                    rowNames = [];
                    columnNames = [];
            end
        end
        
        function Rmean = getAverageRadius(model)
           cp = model.controlPoints;
           [~,~,r] = cart2sph(cp(:,1),cp(:,2),cp(:,3));
           Rmean = mean(r);
        end
        
        function cropToBoundingBox(model)
            %This function crops the FOV to just fit the lesion model
            %In the case that the current FOV is too small, it makes it
            %bigger then recursively runs this function again.
            
            %Turn off texture if needed
            model2=copy(model);
            if ~strcmp(model.textureType,'none')
                model2=copy(model);
                temp.type='none';
                model2.texture=temp;
            end
            model2.scale = 1;
            %Get the mask image
            mask = model2.I/model2.contrast>model2.iso;
            
            [Y,X,Z] = ind2sub(size(mask),find(mask));
            
            xmin = min(X(:)); xmax = max(X(:));
            ymin = min(Y(:)); ymax = max(Y(:));
            zmin = min(Z(:)); zmax = max(Z(:));
            
            if xmin~=1 && xmax~=size(mask,2) && ymin~=1 && ymax~=size(mask,1) && zmin~=1 && zmax ~= size(mask,3)
                psize = model2.psize;
                Fx = (max(X(:))-min(X(:)))*psize(1);
                Fy = (max(Y(:))-min(Y(:)))*psize(2);
                Fz = (max(Z(:))-min(Z(:)))*psize(3);
                model.FOV = ceil([Fx Fy Fz])+5;
            else
                model.FOV = model.FOV*1.5;
                cropToBoundingBox(model);
            end
            
            
            
        end
        
        function [x,y] = getAverageProfile(model)
            Rmean = getAverageRadius(model);
            [x,~,~] = getCartesianCoordinates(model,'PostV');
            %Evaluate initial contrast profile equation
            switch model.profileType
                case 'Designer'
                    C = model.profile.C;
                    n = model.profile.n;
                    y = designerNodule(abs(x),Rmean,n,1);
                case 'Flat'
                    C = model.profile.C;
                    y = C*double(abs(x)<=Rmean);
                
            end
            
            switch model.blurType
                case 'Gaussian'
                    PSF = mean(model.blur.PSF); PSF = [PSF PSF PSF];
                    y = gaussianBlurLesion(y,PSF,model.psize);
                case 'None'
            end
            y=mat2gray(y);
            y=y*model.contrast;
            
        end
        
        function T = getTextureImage(model)
            
            switch model.textureType
                case 'none'
                    T = zeros([model.n(2) model.n(1) model.n(3)]);
                case 'clusturedLumpyBackground'
                    
                    %get the blob positions and orientations (if they don't
                    %exist yet create them)
                    if isempty(model.texture.positions)
                        cp = model.controlPoints;
                        buff = 1.25;
                        FOV = [buff*(max(cp(:,1))-min(cp(:,1))) buff*(max(cp(:,2))-min(cp(:,2))) buff*(max(cp(:,3))-min(cp(:,3)))];
                        model.texture.positions = generateBlobPositions(model.texture,FOV);
                    end
                    
                    %Voxelize texture image
                    T = voxelizeCLB(model.texture,model.FOV,model.n);
                    
                otherwise 
                    warning('Texture type is not supported')
            end
            
        end
        
        function set.texture(model,texture)
            oldType = model.textureType;
            newType = texture.type;
            switch newType
                
                case 'none'
                    model.texture=texture;
                    
                case 'clusturedLumpyBackground'
                    changed = false;
                    
                    %See if properties affecting blop locations has changed
                    if ~strcmp(oldType,'clusturedLumpyBackground')
                        changed = true;
                    else
                        if texture.N~=model.texture.N
                            changed = true;
                        elseif texture.K~=model.texture.K
                            changed = true;
                        elseif texture.sigma ~=model.texture.sigma
                            changed = true;
                        end
                    end
                    
                    %generate new blob positions if needed
                    if changed
                        cp = model.controlPoints;
                        buff = 1.25;
                        FOV = [buff*(max(cp(:,1))-min(cp(:,1))) buff*(max(cp(:,2))-min(cp(:,2))) buff*(max(cp(:,3))-min(cp(:,3)))];
                        texture.positions = generateBlobPositions(texture,FOV);
                    end
                    
                    model.texture=texture;
                    
                otherwise
                    warning('Texture type is not supported')
            end
        end
        
        function set.shape(model,shape)
            
            model.shape = shape;
            switch shape.type
                case 'Simple Mesh'
                    profile.type='Flat';
                    profile.C = model.contrast;
                    model.profile=profile;
            end
                
        end
        
        function newModel=copy(model)
            newModel=lesionModel;
            newModel.shape=model.shape;
            newModel.profile=model.profile;
            newModel.blur=model.blur;
            newModel.texture=model.texture;
            newModel.FOV=model.FOV;
            newModel.n=model.n;
            newModel.origin=model.origin;
            newModel.scale=model.scale;
            newModel.interpType=model.interpType;
            newModel.reducePatchFactor=model.reducePatchFactor;
            newModel.iso=model.iso;
            newModel.type=model.type;
            newModel.tag=model.tag;
            newModel.name=model.name;
        end
        
    end
    
end

function [shape, profile, blur, FOV, n , scale, interpType, texture, type, tag, name] = parseInputs(args)

%Set all args
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'shape'
                shape = args{i+1};
            case 'profile'
                profile = args{i+1};
            case 'blur'
                blur = args{i+1};
            case 'FOV'
                FOV = args{i+1};
            case 'n'
                n = args{i+1};
            case 'scale'
                scale = args{i+1};
            case 'interpType'
                interpType = args{i+1};
            case 'texture'
                texture = args{i+1};
            case 'type'
                type = args{i+1};
            case 'tag'
                tag = args{i+1};
            case 'name'
                name = args{i+1};
        end
    end
    
end

%Set all other variables to defaults
if ~exist('shape','var')
    shape.type = 'Spoke';
    ntheta_b = 10;
    nphi_b  = 10;
    shape.Rb = 5*ones(nphi_b,ntheta_b);
    shape.theta_b=0:(2*pi)/ntheta_b:2*pi-(2*pi)/ntheta_b;
    shape.phi_b=linspace(-pi/2,pi/2,nphi_b);
    shape.f = 1;
end

if ~exist('profile','var')
    profile.type = 'Designer';
    profile.n = 1;      %Exponent of the designer nodule equation
    profile.C = 100;    %Peak contrast of the designer nodule
end

if ~exist('blur','var')
    blur.type = 'Gaussian';
    blur.PSF = [1 1 1];
end

if ~exist('FOV','var')
    FOV = [20 20 20];
end

if ~exist('n','var')
    n = [25 25 25];
end

if ~exist('scale','var')
    scale = 1;
end

if ~exist('interpType','var')
    interpType = 'linear';
end

if ~exist('texture','var')
    texture.type = 'none';
    
%     texture.type = 'clusturedLumpyBackground';
%     texture.K = .1; %Number of clusters per mm^3
%     texture.N = 1;  %Number of blobs (per cluster point) total number of blobs = round(K*vol*N)
%     texture.sigma = 1; %Defines how far away subclusters are from their parent
%     texture.alpha = 1; %Alpha and beta are parameters of the exponential function
%     texture.beta = 1;
%     texture.L = [1 2 3]; %vector that defines the size of the function in each direction
%     texture.positions = []; %[x y z u v w] if this is empty, the positions need to be generated
%     texture.contrast = .5; %This defines the magnitude of texture fluctuations as a fraction of the lesion contrast

end

if ~exist('type','var')
    type = 'type';
end

if ~exist('tag','var')
    tag = 'tag';
end

if ~exist('name','var')
    name = 'name';
end

end

function result = computeFitError(x,model,im,weights,NPS,MTF,flag)
%set the model according to the input parameters
model.profile.C=x(1);
B = x(2);
switch model.shapeType
    case 'Spoke'
        switch model.profileType
            case 'Designer'
                model.profile.n=x(3);
                
                switch model.blurType
                    
                    case 'None'
                        Rb = x(4:end);
                    case 'Gaussian'
                        model.blur.PSF = [x(4) x(5) x(6)];
                        Rb = x(7:end);         
                end
            case 'Flat'
                switch model.blurType
                    case 'None'
                        Rb = x(3:end);
                       
                    case 'Gaussian'
                        model.blur.PSF = [x(3) x(4) x(5)];
                        Rb = x(6:end);             
                end     
        end
        
        %Rearrange Rb into matrix format (R_b)
        R_b=zeros(length(model.shape.theta_b),length(model.shape.phi_b));
        R_b(2:end-1,:)=reshape(Rb(1:end-2),size(R_b(2:end-1,:)));
        R_b(1,:)=Rb(end-1); R_b(end,:)=x(end);
        model.shape.Rb=R_b;
        
        
    case {'Mesh','Simple Mesh'}
        
        switch model.profileType
            case 'Designer'
                model.profile.n=x(3);
                
                switch model.blurType
                    case 'None'
                        
                    case 'Gaussian'
                        model.blur.PSF = [x(4) x(5) x(6)];
                        
                end
            case 'Flat'
                switch model.blurType
                    case 'None'
                       
                    case 'Gaussian'
                        model.blur.PSF = [x(3) x(4) x(5)];
                        
                end
        end
             
end

switch flag
    case {'Error','Residual'}
        %voxelize the model
        I = voxelizeLesion(model)+B;
        
        %blur the model
        if ~isempty(MTF)
            I = BlurWithMTFs3D(I,model.psize,MTF.x,MTF.y,MTF.z);
        end
        
        %add noise to the model
        if ~isempty(NPS)
            I = I + generateNoise(NPS,[size(I,2) size(I,1) size(I,3)],model.psize);
        end
        
        result=(I-im).*weights;
        
        if strcmp(flag,'Residual')
            result = sum(result(:).^2);
        end
    case 'Model'
        result = model;
end


end

function [THETA, PHI, R] = wrapSpherical_gridded(theta,phi,R)

%theta= vector of length n
%phi=   vector of length m
%R=     mxn matrix of radius values


% figure
[THETA PHI]=meshgrid(theta,phi);
% [x y z]=sph2cart(THETA(:), PHI(:), R(:));
% scatter3(x,y,z,'b');
% hold on
[min_phi i_phi_min]=min(phi);
min_th=min(theta); max_th=max(theta);

if min_phi==-pi/2
    R(i_phi_min,:)= R(i_phi_min,1);
    th_l=theta<pi; th_l=theta(th_l)+pi; ph_l=[phi(3),phi(2)];
    [TH PH]=meshgrid(th_l,ph_l); R_l=interp2(THETA,PHI,R,TH,PH,'spline');
    th_r=theta>=pi; th_r=theta(th_r)-pi; ph_r=[phi(3),phi(2)];
    [TH PH]=meshgrid(th_r,ph_r); R_r=interp2(THETA,PHI,R,TH,PH,'spline');
    R=[R_l R_r;R];
    %R=[flipud([R(2:3,theta>=pi) R(2:3,theta<pi)]);R];
    phi=[fliplr((-pi/2)-abs(-(pi/2)-phi(2:3))) phi];
else
    th_l=theta<pi; th_l=theta(th_l)+pi; ph_l=[phi(2),phi(1)];
    [TH PH]=meshgrid(th_l,ph_l); R_l=interp2(THETA,PHI,R,TH,PH,'spline');
    th_r=theta>=pi; th_r=theta(th_r)-pi; ph_r=[phi(2),phi(1)];
    [TH PH]=meshgrid(th_r,ph_r); R_r=interp2(THETA,PHI,R,TH,PH,'spline');
    R=[R_l R_r;R];
    %R=[flipud([R(1:2,theta>=pi) R(1:2,theta<pi)]);R];
    phi=[fliplr((-pi/2)-abs(-(pi/2)-phi(1:2))) phi];
end
[THETA PHI]=meshgrid(theta,phi);
% [x y z]=sph2cart(THETA(:), PHI(:), R(:));
% scatter3(x,y,z,'.r');
[max_phi i_phi_max]=max(phi);
if max_phi==pi/2
    R(i_phi_max,:)= R(i_phi_max,1);
    th_l=theta<pi; th_l=theta(th_l)+pi; ph_l=[phi(end-1),phi(end-2)];
    [TH PH]=meshgrid(th_l,ph_l); R_l=interp2(THETA,PHI,R,TH,PH,'spline');
    th_r=theta>=pi; th_r=theta(th_r)-pi; ph_r=[phi(end-1),phi(end-2)];
    [TH PH]=meshgrid(th_r,ph_r); R_r=interp2(THETA,PHI,R,TH,PH,'spline');
    R=[R;R_l R_r];
    %R=[R;flipud([R(end-2:end-1,theta>=pi) R(end-2:end-1,theta<pi)])];
    phi=[phi fliplr((pi/2)+abs((pi/2)-phi(end-2:end-1)))];
else
    th_l=theta<pi; th_l=theta(th_l)+pi; ph_l=[phi(end),phi(end-1)];
    [TH PH]=meshgrid(th_l,ph_l); R_l=interp2(THETA,PHI,R,TH,PH,'spline');
    th_r=theta>=pi; th_r=theta(th_r)-pi; ph_r=[phi(end),phi(end-1)];
    [TH PH]=meshgrid(th_r,ph_r); R_r=interp2(THETA,PHI,R,TH,PH,'spline');
    R=[R;R_l R_r];
    %R=[R;flipud([R(end-1:end,theta>=pi) R(end-1:end,theta<pi)])];
    phi=[phi fliplr((pi/2)+abs((pi/2)-phi(end-1:end)))];
end
[THETA PHI]=meshgrid(theta,phi);
% [x y z]=sph2cart(THETA(:), PHI(:), R(:));
% scatter3(x,y,z,'.r');

if min_th==0
    if max_th==2*pi;    %There are R values at theta=0 and theta=2pi
        R(:,end)=R(:,1);
        Rleft=R(:,end-2:end-1);
        Rright=R(:,2:3);
        theta=[theta(end-2:end-1)-2*pi theta theta(2:3)+2*pi];
    else                %There are R values at theta=0 but not at theta=2pi
        Rleft=R(:,end-1:end);
        Rright=R(:,1:3);
        theta=[theta(end-1:end)-2*pi theta theta(1:3)+2*pi];
    end
else  
    if max_th==2*pi;    %There are R values at theta=2pi but not at theta=0
        Rleft=R(:,end-2:end);
        Rright=R(:,1:2);
        theta=[theta(end-2:end)-2*pi theta theta(1:2)+2*pi];
    else                %There are not R values at theta=0 or theta=2*pi
        Rleft=R(:,end-1:end);
        Rright=R(:,1:2);
        theta=[theta(end-1:end)-2*pi theta theta(1:2)+2*pi];
    end
end

%theta=[theta-2*pi theta theta+2*pi];
%R=[R R R];
R=[Rleft R Rright];
[THETA PHI]=meshgrid(theta,phi);

%[x y z]=sph2cart(THETA(:), PHI(:), R(:));
%scatter3(x,y,z,'.b');


end

function [theta, phi, r] = wrapSpherical(theta,phi,r)
%this function correctly wraps data defined in spherical coordinates to
%prepare it for interpolation.
% n=length(theta);
% figure
% scatter(theta,phi,'b');
% hold on
% rectangle('Position',[0 -pi/2 2*pi pi])

ind=find(phi==pi/2);
if length(ind)>0
    r(ind)=mean(r(ind)); %forces all r values where phi=pi/2 to be the same.
    th_add=unique(theta); phi_add=zeros(size(th_add))+pi/2; r_add=zeros(size(th_add))+mean(r(ind));
    theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
else
    [m ind]=min(abs(phi-pi/2)); %take r value closest to the z-axis and copy along phi=pi/2 for all thetas
    th_add=unique(theta); phi_add=zeros(size(th_add))+pi/2; r_add=zeros(size(th_add))+r(ind);
    theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
end
ind=find(phi==-pi/2);
if length(ind)>0
    r(ind)=mean(r(ind)); %forces all r values where phi=-pi/2 to be the same.
    th_add=unique(theta); phi_add=zeros(size(th_add))-pi/2; r_add=zeros(size(th_add))+mean(r(ind));
    theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
else
    [m ind]=min(abs(phi+pi/2)); %take r value closest to the z-axis and copy along phi=pi/2 for all thetas
    th_add=unique(theta); phi_add=zeros(size(th_add))-pi/2; r_add=zeros(size(th_add))+r(ind);
    theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
end

ind=find(theta==0); th_add=theta(ind)+2*pi; phi_add=phi(ind); r_add=r(ind);
ind=find(~(theta==2*pi)); theta=theta(ind); phi=phi(ind); r=r(ind);
theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
%scatter(th_add,phi_add,'g');

ind=find((theta>0).*(theta<pi)); th_add=theta(ind)+2*pi; phi_add=phi(ind); r_add=r(ind);
theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
%scatter(th_add,phi_add,'k');

ind=find((theta>pi).*(theta<2*pi)); th_add=theta(ind)-2*pi; phi_add=phi(ind); r_add=r(ind);
theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
%scatter(th_add,phi_add,'k');

ind=find((phi>0).*(phi<pi/2));
th_list=theta(ind);
ind_low=find((th_list>=0).*(th_list<pi)); ind_hi=find((th_list>=pi).*(th_list<2*pi));
phi_add=[(pi/2)+abs((pi/2)-phi(ind(ind_low)));(pi/2)+abs((pi/2)-phi(ind(ind_hi)));];
th_add=[theta(ind(ind_low))+pi; theta(ind(ind_hi))-pi];
r_add=[r(ind(ind_low)); r(ind(ind_hi))];
theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
%scatter(th_add,phi_add,'c');

ind=find((phi<0).*(phi>-pi/2));
th_list=theta(ind);
ind_low=find((th_list>=0).*(th_list<pi)); ind_hi=find((th_list>=pi).*(th_list<2*pi));
phi_add=[-(pi/2)-abs((-pi/2)-phi(ind(ind_low)));-(pi/2)-abs((-pi/2)-phi(ind(ind_hi)));];
th_add=[theta(ind(ind_low))+pi; theta(ind(ind_hi))-pi];
r_add=[r(ind(ind_low)); r(ind(ind_hi))];
theta=[theta; th_add]; phi=[phi; phi_add]; r=[r; r_add];
%scatter(th_add,phi_add,'c');

[C ia ic]=unique([theta phi],'rows');
theta=C(:,1); phi=C(:,2); r=r(ia);

%scatter(theta,phi,'.r');

end

function c = designerNodule(r,R,n,C)

c=(1-(r./R).^2).^n;
c(r>R)=0;
c=c*C;

end

function I = gaussianBlurLesion(I,PSF,psize)

t=.1;   %Threshold to cut off the PSF

%get the size of the volume
[n1, n2, n3]=size(I);

%Rearrange PSF and psize to be [y x z];
PSF = [PSF(2) PSF(1) PSF(3)];
psize = [psize(2) psize(1) psize(3)];

%Determine the number of kernel voxels needed in each dimension
if n1>1 & PSF(1)>0; 
    r_1_t=PSF(1)*sqrt(-2*log(t));
    N1=2*r_1_t/psize(1);
    N1=2.*round((N1+1)/2)-1;
else
    N1=1;
    r_1_t=0;
    PSF(1)=1;
end
if n2>1 & PSF(2)>0;
    r_2_t=PSF(2)*sqrt(-2*log(t));
    N2=2*r_2_t/psize(2);
    N2=2.*round((N2+1)/2)-1;
else
    N2=1;
    r_2_t=0;
    PSF(2)=1;
end
if n3>1 & PSF(3)>0;
    r_3_t=PSF(3)*sqrt(-2*log(t));
    N3=2*r_3_t/psize(3);
    N3=2.*round((N3+1)/2)-1;
else
    N3=1;
    r_3_t=0;
    PSF(3)=1;
end

%Create kernels and filter c
PSF1=fspecial('gaussian',[N1 1],PSF(1)/psize(1)); PSF1 = PSF1/sum(PSF1(:));
PSF2=fspecial('gaussian',[1 N2],PSF(2)/psize(2)); PSF2 = PSF2/sum(PSF2(:));
PSF3=fspecial('gaussian',[N3 1],PSF(3)/psize(3)); PSF3=reshape(PSF3,[1 1 N3]); PSF3 = PSF3/sum(PSF3(:));
I=imfilter(imfilter(imfilter(I,PSF3,'symmetric','same','conv'),PSF2,'symmetric','same','conv'),PSF1,'symmetric','same','conv');

end

function positions = generateBlobPositions(texture,FOV)
%This function generates random blob positions and orientations for the
%clustured lumpy background

%Get the constants needed from the texture structured variable
K = texture.K;  %Density of the cluster points
N = texture.N;  %Number of blobs per cluster point (must be an int)
sigma = texture.sigma;

%compute the volume
vol = prod(FOV);

%Get the total number of cluster points
K = round(texture.K*vol);

%Generate cluster positions
rk_x=random('unif',-FOV(1)/2,FOV(1)/2,[K 1]);
rk_y=random('unif',-FOV(2)/2,FOV(2)/2,[K 1]);
rk_z=random('unif',-FOV(3)/2,FOV(3)/2,[K 1]);

%Make indicies for each blob's parent cluster
ind = repmat((1:K)',[N 1]);
N = K*N;    %now N is the total number of blobs

%generate random directions away from the cluster location for each blob
u=random('unif',-1,1,[N,1]); theta=random('unif',0,2*pi,[N,1]);
dir(:,1)=sqrt(1-u.^2).*cos(theta);
dir(:,2)=sqrt(1-u.^2).*sin(theta);
dir(:,3)=u;

%generate a distance from the cluster point for each blob
r=random('norm',0,sigma,size(u));

%compute the cluster point locations
positions=zeros(size(dir));
positions(:,1)=rk_x(ind)+r.*dir(:,1);
positions(:,2)=rk_y(ind)+r.*dir(:,2);
positions(:,3)=rk_z(ind)+r.*dir(:,3);

%generate random orientations of the blobs
u=random('unif',-1,1,[N,1]); theta=random('unif',0,2*pi,[N,1]);
positions(:,4)=sqrt(1-u.^2).*cos(theta);
positions(:,5)=sqrt(1-u.^2).*sin(theta);
positions(:,6)=u;

end

function T = voxelizeCLB(texture,FOV,n)

%get the needed constants
alpha = texture.alpha;
beta = texture.beta;
L = texture.L;
C = texture.contrast;
positions=texture.positions;

%Create the coordinate system
x = linspace(-FOV(1)/2,FOV(1)/2,n(1));
y = linspace(-FOV(2)/2,FOV(2)/2,n(2));
z = linspace(-FOV(3)/2,FOV(3)/2,n(3));
[X,Y,Z] = meshgrid(x,y,z);

%Get the cutoff length
cutoff = .05;
r_cutoff=(-max(L)*log(cutoff)/alpha)^(1/beta);

%Create blank image
Texture=zeros(size(X));
blank=zeros(size(Texture));


%create a blank image
T=zeros(size(X));

%Add each blob to the image
for i=1:size(positions,1)
    
    %Shift coordinate system to be centered at the blob position
    Xtemp=X-positions(i,1); Ytemp=Y-positions(i,2); Ztemp=Z-positions(i,3);
    mask=Xtemp <= r_cutoff & Xtemp >= -r_cutoff & Ytemp <= r_cutoff & Ytemp >= -r_cutoff & Ztemp <= r_cutoff & Ztemp >= -r_cutoff;
    Xtemp=Xtemp(mask); Ytemp=Ytemp(mask); Ztemp=Ztemp(mask);
    
    xtemp=x-positions(i,1); ytemp=y-positions(i,2); ztemp=z-positions(i,3);
    indx = xtemp <= r_cutoff & xtemp >= -r_cutoff; xtemp = xtemp(indx);
    indy = ytemp <= r_cutoff & ytemp >= -r_cutoff; ytemp = ytemp(indy);
    indz = ztemp <= r_cutoff & ztemp >= -r_cutoff; ztemp = ztemp(indz);
    [Xtemp, Ytemp, Ztemp] = meshgrid(xtemp,ytemp,ztemp);
    Xtemp=Xtemp(:); Ytemp = Ytemp(:); Ztemp=Ztemp(:);
    mask(:)=false;
    mask(indy,indx,indz) = true;

    
    %Change the coordinate system to be oriented along direction of the
    %given blob
    d=positions(i,4:6);
    yz=null(d).';
    M=[d;yz]';
    temp=(inv(M)*[Xtemp Ytemp Ztemp]')';
    b = GuassianBlob(temp(:,1),temp(:,2),temp(:,3),alpha,beta,L);
    T(mask)=T(mask)+b;   
end

T=mat2gray(T);
T = T-mean(T(:));
T = T*C;


end

function [ b ] = GuassianBlob(X,Y,Z,alpha,beta,L)
%Creates a gaussian blob image I at coordinates X,Y,Z.


bx=exp(-alpha*(abs(X).^beta)/(L(1)));
by=exp(-alpha*(abs(Y).^beta)/(L(2)));
bz=exp(-alpha*(abs(Z).^beta)/(L(3)));
b=bx.*by.*bz;

end

function [gridOUTPUT,varargout] = VOXELISE(gridX,gridY,gridZ,varargin)
% VOXELISE  Voxelise a 3D triangular-polygon mesh.
%==========================================================================
% AUTHOR        Adam H. Aitkenhead
% CONTACT       adam.aitkenhead@christie.nhs.uk
% INSTITUTION   The Christie NHS Foundation Trust
%
% USAGE        [gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(gridX,gridY,gridZ,STLin,raydirection)
%        or... [gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(gridX,gridY,gridZ,meshFV,raydirection)
%        or... [gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(gridX,gridY,gridZ,meshX,meshY,meshZ,raydirection)
%        or... [gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(gridX,gridY,gridZ,meshXYZ,raydirection)
%
% INPUTS
%
%     gridX   - Mandatory - 1xP array     - List of the grid X coordinates. 
%                           OR an integer - Number of voxels in the grid in the X direction.
%
%     gridY   - Mandatory - 1xQ array     - List of the grid Y coordinates.
%                           OR an integer - Number of voxels in the grid in the Y direction.
%
%     gridZ   - Mandatory - 1xR array     - List of the grid Z coordinates.
%                           OR an integer - Number of voxels in the grid in the Z direction.
%
%     STLin   - Optional  - string        - Filename of the STL file.
%
%     meshFV  - Optional  - structure     - Structure containing the faces and vertices
%                                           of the mesh, in the same format as that produced
%                                           by the isosurface command.
%
%     meshX   - Optional  - 3xN array     - List of the mesh X coordinates for the 3 vertices of each of the N triangular patches
%     meshY   - Optional  - 3xN array     - List of the mesh Y coordinates for the 3 vertices of each of the N triangular patches
%     meshZ   - Optional  - 3xN array     - List of the mesh Z coordinates for the 3 vertices of each of the N triangular patches
%
%     meshXYZ - Optional  - Nx3x3 array   - The vertex coordinates for each facet, with:
%                                           1 row for each facet
%                                           3 columns for the x,y,z coordinates
%                                           3 pages for the three vertices
%
%     raydirection - Optional - String    - Defines the directions in which ray-tracing
%                                           is performed.  The default is 'xyz', which
%                                           traces in the x,y,z directions and combines
%                                           the results.
%
% OUTPUTS
%
%     gridOUTPUT - Mandatory - PxQxR logical array - Voxelised data (1=>Inside the mesh, 0=>Outside the mesh)
%
%     gridCOx    - Optional - 1xP array - List of the grid X coordinates.
%     gridCOy    - Optional - 1xQ array - List of the grid Y coordinates.
%     gridCOz    - Optional - 1xR array - List of the grid Z coordinates.
%
% EXAMPLES
%
%     To voxelise an STL file:
%     >>  [gridOUTPUT] = VOXELISE(gridX,gridY,gridZ,STLin)
% 
%     To voxelise a mesh defined by a structure containing the faces and vertices:
%     >>  [gridOUTPUT] = VOXELISE(gridX,gridY,gridZ,meshFV)
% 
%     To voxelise a mesh where the x,y,z coordinates are defined by three 3xN arrays:
%     >>  [gridOUTPUT] = VOXELISE(gridX,gridY,gridZ,meshX,meshY,meshZ)
%
%     To voxelise a mesh defined by a single Nx3x3 array:
%     >>  [gridOUTPUT] = VOXELISE(gridX,gridY,gridZ,meshXYZ)
%
%     To also output the lists of X,Y,Z coordinates:
%     >>  [gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(gridX,gridY,gridZ,STLin)
%
%     To use ray-tracing in only the z-direction:
%     >>  [gridOUTPUT] = VOXELISE(gridX,gridY,gridZ,STLin,'z')
%
% NOTES
%
%   - The mesh must be properly closed (ie. watertight).
%   - Defining raydirection='xyz' means that the mesh is ray-traced in each
%     of the x,y,z directions, with the overall result being a combination
%     of the result from each direction.  This gives the most reliable
%     result at the expense of computation time.
%   - Tracing in only one direction (eg. raydirection='z') is faster, but
%     can potentially lead to artefacts where a ray exactly crosses
%     several facet edges.
%
% REFERENCES
%
%   - This code uses a ray intersection method similar to that described by:
%     Patil S and Ravi B.  Voxel-based representation, display and
%     thickness analysis of intricate shapes. Ninth International
%     Conference on Computer Aided Design and Computer Graphics (CAD/CG
%     2005)
%==========================================================================

%==========================================================================
% VERSION USER CHANGES
% ------- ---- -------
% 100510  AHA  Original version.
% 100514  AHA  Now works with non-STL input.  Changes also provide a
%              significant speed improvement.
% 100520  AHA  Now optionally output the grid x,y,z coordinates.
%              Robustness also improved.
% 100614  AHA  Define gridOUTPUT as a logical array to improve memory
%              efficiency.
% 100615  AHA  Reworked the ray interpolation code to correctly handle
%              logical arrays.
% 100623  AHA  Enable ray-tracing in any combination of the x,y,z
%              directions.
% 100628  AHA  Allow input to be a structure containing [faces,vertices]
%              data, similar to the type of structure output by
%              isosurface.
% 100907  AHA  Now allow the grid to be smaller than the mesh dimensions.
% 101126  AHA  Simplified code, slight speed improvement, more robust.
%              Changed handling of automatic grid generation to reduce
%              chance of artefacts.
% 101201  AHA  Fixed bug in automatic grid generation.
% 110303  AHA  Improved method of finding which mesh facets can possibly
%              be crossed by each ray.  Up to 80% reduction in run-time.
% 111104  AHA  Housekeeping tidy-up.
% 130212  AHA  Added checking of ray/vertex intersections, which reduces
%              artefacts in situations where the mesh vertices are located
%              directly on ray paths in the voxelisation grid.
%==========================================================================


%======================================================
% CHECK THE REQUIRED NUMBER OF OUTPUT PARAMETERS
%======================================================

if nargout~=1 && nargout~=4
  error('Incorrect number of output arguments.')
end

%======================================================
% READ INPUT PARAMETERS
%======================================================

% Read the ray direction if defined by the user, and remove this from the
% list of input arguments.  This makes it to make it easier to extract the
% mesh data from the input arguments in the subsequent step.

if ischar(varargin{end}) && max(strcmpi(varargin{end},{'x','y','z','xy','xz','yx','yz','zx','zy','xyz','xzy','yxz','yzx','zxy','zyx'}))
  raydirection    = lower(varargin{end});
  varargin        = varargin(1:nargin-4);
  narginremaining = nargin-1;
else
  raydirection    = 'xyz';    %Set the default value if none is defined by the user
  narginremaining = nargin;
end

% Whatever the input mesh format is, it is converted to an Nx3x3 array
% defining the vertex positions for each facet, with 1 row for each facet,
% 3 cols for the x,y,z coordinates, and 3 pages for the three vertices.

if narginremaining==4
  
  if isstruct(varargin{1})==1
    meshXYZ = CONVERT_meshformat(varargin{1}.faces,varargin{1}.vertices);
  
  elseif ischar(varargin{1})
    meshXYZ = READ_stl(varargin{1});
    
  else
    meshXYZ = varargin{1};
    
  end

elseif narginremaining==6

  meshX = varargin{1};
  meshY = varargin{2};
  meshZ = varargin{3};
 
  meshXYZ = zeros( size(meshX,2) , 3 , size(meshX,1) );

  meshXYZ(:,1,:) = reshape(meshX',size(meshX,2),1,3);
  meshXYZ(:,2,:) = reshape(meshY',size(meshY,2),1,3);
  meshXYZ(:,3,:) = reshape(meshZ',size(meshZ,2),1,3);
  
else
  
  error('Incorrect number of input arguments.')
  
end

%======================================================
% IDENTIFY THE MIN AND MAX X,Y,Z COORDINATES OF THE POLYGON MESH
%======================================================

meshXmin = min(min(meshXYZ(:,1,:)));
meshXmax = max(max(meshXYZ(:,1,:)));
meshYmin = min(min(meshXYZ(:,2,:)));
meshYmax = max(max(meshXYZ(:,2,:)));
meshZmin = min(min(meshXYZ(:,3,:)));
meshZmax = max(max(meshXYZ(:,3,:)));

%======================================================
% CHECK THE DIMENSIONS OF THE 3D OUTPUT GRID
%======================================================
% The output grid will be defined by the coordinates in gridCOx, gridCOy, gridCOz

if numel(gridX)>1
  if size(gridX,1)>size(gridX,2)   %gridX should be a row vector rather than a column vector
    gridCOx = gridX';
  else
    gridCOx = gridX;
  end
elseif numel(gridX)==1 && gridX==1   %If gridX is a single integer (rather than a vector) and is equal to 1
  gridCOx   = (meshXmin+meshXmax)/2;
elseif numel(gridX)==1 && rem(gridX,1)==0   %If gridX is a single integer (rather than a vector) then automatically create the list of x coordinates
  voxwidth  = (meshXmax-meshXmin)/(gridX+1/2);
  gridCOx   = meshXmin+voxwidth/2 : voxwidth : meshXmax-voxwidth/2;
end

if numel(gridY)>1
  if size(gridY,1)>size(gridY,2)   %gridY should be a row vector rather than a column vector
    gridCOy = gridY';
  else
    gridCOy = gridY;
  end
elseif numel(gridY)==1 && gridY==1   %If gridX is a single integer (rather than a vector) and is equal to 1
  gridCOy   = (meshYmin+meshYmax)/2;
elseif numel(gridY)==1 && rem(gridY,1)==0   %If gridX is a single integer (rather than a vector) then automatically create the list of y coordinates
  voxwidth  = (meshYmax-meshYmin)/(gridY+1/2);
  gridCOy   = meshYmin+voxwidth/2 : voxwidth : meshYmax-voxwidth/2;
end

if numel(gridZ)>1
  if size(gridZ,1)>size(gridZ,2)   %gridZ should be a row vector rather than a column vector
    gridCOz = gridZ';
  else
    gridCOz = gridZ;
  end
elseif numel(gridZ)==1 && gridZ==1   %If gridX is a single integer (rather than a vector) and is equal to 1
  gridCOz   = (meshZmin+meshZmax)/2;
elseif numel(gridZ)==1 && rem(gridZ,1)==0   %If gridZ is a single integer (rather than a vector) then automatically create the list of z coordinates
  voxwidth  = (meshZmax-meshZmin)/(gridZ+1/2);
  gridCOz   = meshZmin+voxwidth/2 : voxwidth : meshZmax-voxwidth/2;
end

%Check that the output grid is large enough to cover the mesh:
if ~isempty(strfind(raydirection,'x'))  &&  (min(gridCOx)>meshXmin || max(gridCOx)<meshXmax)
  gridcheckX = 0;
  if min(gridCOx)>meshXmin
    gridCOx    = [meshXmin,gridCOx];
    gridcheckX = gridcheckX+1;
  end
  if max(gridCOx)<meshXmax
    gridCOx    = [gridCOx,meshXmax];
    gridcheckX = gridcheckX+2;
  end
elseif ~isempty(strfind(raydirection,'y'))  &&  (min(gridCOy)>meshYmin || max(gridCOy)<meshYmax)
  gridcheckY = 0;
  if min(gridCOy)>meshYmin
    gridCOy    = [meshYmin,gridCOy];
    gridcheckY = gridcheckY+1;
  end
  if max(gridCOy)<meshYmax
    gridCOy    = [gridCOy,meshYmax];
    gridcheckY = gridcheckY+2;
  end
elseif ~isempty(strfind(raydirection,'z'))  &&  (min(gridCOz)>meshZmin || max(gridCOz)<meshZmax)
  gridcheckZ = 0;
  if min(gridCOz)>meshZmin
    gridCOz    = [meshZmin,gridCOz];
    gridcheckZ = gridcheckZ+1;
  end
  if max(gridCOz)<meshZmax
    gridCOz    = [gridCOz,meshZmax];
    gridcheckZ = gridcheckZ+2;
  end
end

%======================================================
% VOXELISE USING THE USER DEFINED RAY DIRECTION(S)
%======================================================

%Count the number of voxels in each direction:
voxcountX = numel(gridCOx);
voxcountY = numel(gridCOy);
voxcountZ = numel(gridCOz);

% Prepare logical array to hold the voxelised data:
gridOUTPUT      = false( voxcountX,voxcountY,voxcountZ,numel(raydirection) );
countdirections = 0;

if strfind(raydirection,'x')
  countdirections = countdirections + 1;
  gridOUTPUT(:,:,:,countdirections) = permute( VOXELISEinternal(gridCOy,gridCOz,gridCOx,meshXYZ(:,[2,3,1],:)) ,[3,1,2] );
end

if strfind(raydirection,'y')
  countdirections = countdirections + 1;
  gridOUTPUT(:,:,:,countdirections) = permute( VOXELISEinternal(gridCOz,gridCOx,gridCOy,meshXYZ(:,[3,1,2],:)) ,[2,3,1] );
end

if strfind(raydirection,'z')
  countdirections = countdirections + 1;
  gridOUTPUT(:,:,:,countdirections) = VOXELISEinternal(gridCOx,gridCOy,gridCOz,meshXYZ);
end

% Combine the results of each ray-tracing direction:
if numel(raydirection)>1
  gridOUTPUT = sum(gridOUTPUT,4)>=1;
end

%======================================================
% RETURN THE OUTPUT GRID TO THE SIZE REQUIRED BY THE USER (IF IT WAS CHANGED EARLIER)
%======================================================

if exist('gridcheckX','var')
  if gridcheckX == 1
    gridOUTPUT = gridOUTPUT(2:end,:,:);
    gridCOx    = gridCOx(2:end);
  elseif gridcheckX == 2
    gridOUTPUT = gridOUTPUT(1:end-1,:,:);
    gridCOx    = gridCOx(1:end-1);
  elseif gridcheckX == 3
    gridOUTPUT = gridOUTPUT(2:end-1,:,:);
    gridCOx    = gridCOx(2:end-1);
  end
end
if exist('gridcheckY','var')
  if gridcheckY == 1
    gridOUTPUT = gridOUTPUT(:,2:end,:);
    gridCOy    = gridCOy(2:end);
  elseif gridcheckY == 2
    gridOUTPUT = gridOUTPUT(:,1:end-1,:);
    gridCOy    = gridCOy(1:end-1);
  elseif gridcheckY == 3
    gridOUTPUT = gridOUTPUT(:,2:end-1,:);
    gridCOy    = gridCOy(2:end-1);
  end
end
if exist('gridcheckZ','var')
  if gridcheckZ == 1
    gridOUTPUT = gridOUTPUT(:,:,2:end);
    gridCOz    = gridCOz(2:end);
  elseif gridcheckZ == 2
    gridOUTPUT = gridOUTPUT(:,:,1:end-1);
    gridCOz    = gridCOz(1:end-1);
  elseif gridcheckZ == 3
    gridOUTPUT = gridOUTPUT(:,:,2:end-1);
    gridCOz    = gridCOz(2:end-1);
  end
end

%======================================================
% PREPARE THE OUTPUT PARAMETERS
%======================================================

if nargout==4
  varargout(1) = {gridCOx};
  varargout(2) = {gridCOy};
  varargout(3) = {gridCOz};
end

end

function [gridOUTPUT] = VOXELISEinternal(gridCOx,gridCOy,gridCOz,meshXYZ)

%Count the number of voxels in each direction:
voxcountX = numel(gridCOx);
voxcountY = numel(gridCOy);
voxcountZ = numel(gridCOz);

% Prepare logical array to hold the voxelised data:
gridOUTPUT = false(voxcountX,voxcountY,voxcountZ);

%Identify the min and max x,y coordinates (cm) of the mesh:
meshXmin = min(min(meshXYZ(:,1,:)));
meshXmax = max(max(meshXYZ(:,1,:)));
meshYmin = min(min(meshXYZ(:,2,:)));
meshYmax = max(max(meshXYZ(:,2,:)));
meshZmin = min(min(meshXYZ(:,3,:)));
meshZmax = max(max(meshXYZ(:,3,:)));

%Identify the min and max x,y coordinates (pixels) of the mesh:
meshXminp = find(abs(gridCOx-meshXmin)==min(abs(gridCOx-meshXmin)));
meshXmaxp = find(abs(gridCOx-meshXmax)==min(abs(gridCOx-meshXmax)));
meshYminp = find(abs(gridCOy-meshYmin)==min(abs(gridCOy-meshYmin)));
meshYmaxp = find(abs(gridCOy-meshYmax)==min(abs(gridCOy-meshYmax)));

%Make sure min < max for the mesh coordinates:
if meshXminp > meshXmaxp
  [meshXminp,meshXmaxp] = deal(meshXmaxp,meshXminp);
end %if
if meshYminp > meshYmaxp
  [meshYminp,meshYmaxp] = deal(meshYmaxp,meshYminp);
end %if

%Identify the min and max x,y,z coordinates of each facet:
meshXYZmin = min(meshXYZ,[],3);
meshXYZmax = max(meshXYZ,[],3);


%======================================================
% VOXELISE THE MESH
%======================================================

correctionLIST = [];   %Prepare to record all rays that fail the voxelisation.  This array is built on-the-fly, but since
                       %it ought to be relatively small should not incur too much of a speed penalty.
                       
% Loop through each x,y pixel.
% The mesh will be voxelised by passing rays in the z-direction through
% each x,y pixel, and finding the locations where the rays cross the mesh.
for loopY = meshYminp:meshYmaxp
  
  % - 1a - Find which mesh facets could possibly be crossed by the ray:
  possibleCROSSLISTy = find( meshXYZmin(:,2)<=gridCOy(loopY) & meshXYZmax(:,2)>=gridCOy(loopY) );
  
  for loopX = meshXminp:meshXmaxp
    
    % - 1b - Find which mesh facets could possibly be crossed by the ray:
    possibleCROSSLIST = possibleCROSSLISTy( meshXYZmin(possibleCROSSLISTy,1)<=gridCOx(loopX) & meshXYZmax(possibleCROSSLISTy,1)>=gridCOx(loopX) );

    if isempty(possibleCROSSLIST)==0  %Only continue the analysis if some nearby facets were actually identified
          
      % - 2 - For each facet, check if the ray really does cross the facet rather than just passing it close-by:
          
      % GENERAL METHOD:
      % A. Take each edge of the facet in turn.
      % B. Find the position of the opposing vertex to that edge.
      % C. Find the position of the ray relative to that edge.
      % D. Check if ray is on the same side of the edge as the opposing vertex.
      % E. If this is true for all three edges, then the ray definitely passes through the facet.
      %
      % NOTES:
      % A. If a ray crosses exactly on a vertex:
      %    a. If the surrounding facets have normal components pointing in the same (or opposite) direction as the ray then the face IS crossed.
      %    b. Otherwise, add the ray to the correctionlist.
      
      facetCROSSLIST = [];   %Prepare to record all facets which are crossed by the ray.  This array is built on-the-fly, but since
                             %it ought to be relatively small (typically a list of <10) should not incur too much of a speed penalty.
      
      %----------
      % - 1 - Check for crossed vertices:
      %----------
      
      % Find which mesh facets contain a vertex which is crossed by the ray:
      vertexCROSSLIST = possibleCROSSLIST( (meshXYZ(possibleCROSSLIST,1,1)==gridCOx(loopX) & meshXYZ(possibleCROSSLIST,2,1)==gridCOy(loopY)) ...
                                         | (meshXYZ(possibleCROSSLIST,1,2)==gridCOx(loopX) & meshXYZ(possibleCROSSLIST,2,2)==gridCOy(loopY)) ...
                                         | (meshXYZ(possibleCROSSLIST,1,3)==gridCOx(loopX) & meshXYZ(possibleCROSSLIST,2,3)==gridCOy(loopY)) ...
                                         );
      
      if isempty(vertexCROSSLIST)==0  %Only continue the analysis if potential vertices were actually identified

        checkindex = zeros(1,numel(vertexCROSSLIST));

        while min(checkindex) == 0
          
          vertexindex             = find(checkindex==0,1,'first');
          checkindex(vertexindex) = 1;
        
          [temp.faces,temp.vertices] = CONVERT_meshformat(meshXYZ(vertexCROSSLIST,:,:));
          adjacentindex              = ismember(temp.faces,temp.faces(vertexindex,:));
          adjacentindex              = max(adjacentindex,[],2);
          checkindex(adjacentindex)  = 1;
        
          coN = COMPUTE_mesh_normals(meshXYZ(vertexCROSSLIST(adjacentindex),:,:));
        
          if max(coN(:,3))<0 || min(coN(:,3))>0
            facetCROSSLIST    = [facetCROSSLIST,vertexCROSSLIST(vertexindex)];
          else
            possibleCROSSLIST = [];
            correctionLIST    = [ correctionLIST; loopX,loopY ];
            checkindex(:)     = 1;
          end
        
        end
        
      end
      
      %----------
      % - 2 - Check for crossed facets:
      %----------
      
      if isempty(possibleCROSSLIST)==0  %Only continue the analysis if some nearby facets were actually identified
          
        for loopCHECKFACET = possibleCROSSLIST'
  
          %Check if ray crosses the facet.  This method is much (>>10 times) faster than using the built-in function 'inpolygon'.
          %Taking each edge of the facet in turn, check if the ray is on the same side as the opposing vertex.
        
          Y1predicted = meshXYZ(loopCHECKFACET,2,2) - ((meshXYZ(loopCHECKFACET,2,2)-meshXYZ(loopCHECKFACET,2,3)) * (meshXYZ(loopCHECKFACET,1,2)-meshXYZ(loopCHECKFACET,1,1))/(meshXYZ(loopCHECKFACET,1,2)-meshXYZ(loopCHECKFACET,1,3)));
          YRpredicted = meshXYZ(loopCHECKFACET,2,2) - ((meshXYZ(loopCHECKFACET,2,2)-meshXYZ(loopCHECKFACET,2,3)) * (meshXYZ(loopCHECKFACET,1,2)-gridCOx(loopX))/(meshXYZ(loopCHECKFACET,1,2)-meshXYZ(loopCHECKFACET,1,3)));
        
          if (Y1predicted > meshXYZ(loopCHECKFACET,2,1) && YRpredicted > gridCOy(loopY)) || (Y1predicted < meshXYZ(loopCHECKFACET,2,1) && YRpredicted < gridCOy(loopY))
            %The ray is on the same side of the 2-3 edge as the 1st vertex.

            Y2predicted = meshXYZ(loopCHECKFACET,2,3) - ((meshXYZ(loopCHECKFACET,2,3)-meshXYZ(loopCHECKFACET,2,1)) * (meshXYZ(loopCHECKFACET,1,3)-meshXYZ(loopCHECKFACET,1,2))/(meshXYZ(loopCHECKFACET,1,3)-meshXYZ(loopCHECKFACET,1,1)));
            YRpredicted = meshXYZ(loopCHECKFACET,2,3) - ((meshXYZ(loopCHECKFACET,2,3)-meshXYZ(loopCHECKFACET,2,1)) * (meshXYZ(loopCHECKFACET,1,3)-gridCOx(loopX))/(meshXYZ(loopCHECKFACET,1,3)-meshXYZ(loopCHECKFACET,1,1)));
          
            if (Y2predicted > meshXYZ(loopCHECKFACET,2,2) && YRpredicted > gridCOy(loopY)) || (Y2predicted < meshXYZ(loopCHECKFACET,2,2) && YRpredicted < gridCOy(loopY))
              %The ray is on the same side of the 3-1 edge as the 2nd vertex.

              Y3predicted = meshXYZ(loopCHECKFACET,2,1) - ((meshXYZ(loopCHECKFACET,2,1)-meshXYZ(loopCHECKFACET,2,2)) * (meshXYZ(loopCHECKFACET,1,1)-meshXYZ(loopCHECKFACET,1,3))/(meshXYZ(loopCHECKFACET,1,1)-meshXYZ(loopCHECKFACET,1,2)));
              YRpredicted = meshXYZ(loopCHECKFACET,2,1) - ((meshXYZ(loopCHECKFACET,2,1)-meshXYZ(loopCHECKFACET,2,2)) * (meshXYZ(loopCHECKFACET,1,1)-gridCOx(loopX))/(meshXYZ(loopCHECKFACET,1,1)-meshXYZ(loopCHECKFACET,1,2)));
            
              if (Y3predicted > meshXYZ(loopCHECKFACET,2,3) && YRpredicted > gridCOy(loopY)) || (Y3predicted < meshXYZ(loopCHECKFACET,2,3) && YRpredicted < gridCOy(loopY))
                %The ray is on the same side of the 1-2 edge as the 3rd vertex.

                %The ray passes through the facet since it is on the correct side of all 3 edges
                facetCROSSLIST = [facetCROSSLIST,loopCHECKFACET];
            
              end %if
            end %if
          end %if
      
        end %for
    

        %----------
        % - 3 - Find the z coordinate of the locations where the ray crosses each facet or vertex:
        %----------

        gridCOzCROSS = zeros(size(facetCROSSLIST));
        for loopFINDZ = facetCROSSLIST

          % METHOD:
          % 1. Define the equation describing the plane of the facet.  For a
          % more detailed outline of the maths, see:
          % http://local.wasp.uwa.edu.au/~pbourke/geometry/planeeq/
          %    Ax + By + Cz + D = 0
          %    where  A = y1 (z2 - z3) + y2 (z3 - z1) + y3 (z1 - z2)
          %           B = z1 (x2 - x3) + z2 (x3 - x1) + z3 (x1 - x2)
          %           C = x1 (y2 - y3) + x2 (y3 - y1) + x3 (y1 - y2)
          %           D = - x1 (y2 z3 - y3 z2) - x2 (y3 z1 - y1 z3) - x3 (y1 z2 - y2 z1)
          % 2. For the x and y coordinates of the ray, solve these equations to find the z coordinate in this plane.

          planecoA = meshXYZ(loopFINDZ,2,1)*(meshXYZ(loopFINDZ,3,2)-meshXYZ(loopFINDZ,3,3)) + meshXYZ(loopFINDZ,2,2)*(meshXYZ(loopFINDZ,3,3)-meshXYZ(loopFINDZ,3,1)) + meshXYZ(loopFINDZ,2,3)*(meshXYZ(loopFINDZ,3,1)-meshXYZ(loopFINDZ,3,2));
          planecoB = meshXYZ(loopFINDZ,3,1)*(meshXYZ(loopFINDZ,1,2)-meshXYZ(loopFINDZ,1,3)) + meshXYZ(loopFINDZ,3,2)*(meshXYZ(loopFINDZ,1,3)-meshXYZ(loopFINDZ,1,1)) + meshXYZ(loopFINDZ,3,3)*(meshXYZ(loopFINDZ,1,1)-meshXYZ(loopFINDZ,1,2)); 
          planecoC = meshXYZ(loopFINDZ,1,1)*(meshXYZ(loopFINDZ,2,2)-meshXYZ(loopFINDZ,2,3)) + meshXYZ(loopFINDZ,1,2)*(meshXYZ(loopFINDZ,2,3)-meshXYZ(loopFINDZ,2,1)) + meshXYZ(loopFINDZ,1,3)*(meshXYZ(loopFINDZ,2,1)-meshXYZ(loopFINDZ,2,2));
          planecoD = - meshXYZ(loopFINDZ,1,1)*(meshXYZ(loopFINDZ,2,2)*meshXYZ(loopFINDZ,3,3)-meshXYZ(loopFINDZ,2,3)*meshXYZ(loopFINDZ,3,2)) - meshXYZ(loopFINDZ,1,2)*(meshXYZ(loopFINDZ,2,3)*meshXYZ(loopFINDZ,3,1)-meshXYZ(loopFINDZ,2,1)*meshXYZ(loopFINDZ,3,3)) - meshXYZ(loopFINDZ,1,3)*(meshXYZ(loopFINDZ,2,1)*meshXYZ(loopFINDZ,3,2)-meshXYZ(loopFINDZ,2,2)*meshXYZ(loopFINDZ,3,1));

          if abs(planecoC) < 1e-14
            planecoC=0;
          end
      
          gridCOzCROSS(facetCROSSLIST==loopFINDZ) = (- planecoD - planecoA*gridCOx(loopX) - planecoB*gridCOy(loopY)) / planecoC;
        
        end %for

        %Remove values of gridCOzCROSS which are outside of the mesh limits (including a 1e-12 margin for error).
        gridCOzCROSS = gridCOzCROSS( gridCOzCROSS>=meshZmin-1e-12 & gridCOzCROSS<=meshZmax+1e-12 );
      
        %Round gridCOzCROSS to remove any rounding errors, and take only the unique values:
        gridCOzCROSS = round(gridCOzCROSS*1e12)/1e12;
        gridCOzCROSS = unique(gridCOzCROSS);
      
        %----------
        % - 4 - Label as being inside the mesh all the voxels that the ray passes through after crossing one facet before crossing another facet:
        %----------

        if rem(numel(gridCOzCROSS),2)==0  % Only rays which cross an even number of facets are voxelised

          for loopASSIGN = 1:(numel(gridCOzCROSS)/2)
            voxelsINSIDE = (gridCOz>gridCOzCROSS(2*loopASSIGN-1) & gridCOz<gridCOzCROSS(2*loopASSIGN));
            gridOUTPUT(loopX,loopY,voxelsINSIDE) = 1;
          end %for
        
        elseif numel(gridCOzCROSS)~=0    % Remaining rays which meet the mesh in some way are not voxelised, but are labelled for correction later.
          
          correctionLIST = [ correctionLIST; loopX,loopY ];
        
        end %if

      end
      
    end %if

  end %for
end %for


%======================================================
% USE INTERPOLATION TO FILL IN THE RAYS WHICH COULD NOT BE VOXELISED
%======================================================
%For rays where the voxelisation did not give a clear result, the ray is
%computed by interpolating from the surrounding rays.
countCORRECTIONLIST = size(correctionLIST,1);

if countCORRECTIONLIST>0
    
  %If necessary, add a one-pixel border around the x and y edges of the
  %array.  This prevents an error if the code tries to interpolate a ray at
  %the edge of the x,y grid.
  if min(correctionLIST(:,1))==1 || max(correctionLIST(:,1))==numel(gridCOx) || min(correctionLIST(:,2))==1 || max(correctionLIST(:,2))==numel(gridCOy)
    gridOUTPUT     = [zeros(1,voxcountY+2,voxcountZ);zeros(voxcountX,1,voxcountZ),gridOUTPUT,zeros(voxcountX,1,voxcountZ);zeros(1,voxcountY+2,voxcountZ)];
    correctionLIST = correctionLIST + 1;
  end
  
  for loopC = 1:countCORRECTIONLIST
    voxelsforcorrection = squeeze( sum( [ gridOUTPUT(correctionLIST(loopC,1)-1,correctionLIST(loopC,2)-1,:) ,...
                                          gridOUTPUT(correctionLIST(loopC,1)-1,correctionLIST(loopC,2),:)   ,...
                                          gridOUTPUT(correctionLIST(loopC,1)-1,correctionLIST(loopC,2)+1,:) ,...
                                          gridOUTPUT(correctionLIST(loopC,1),correctionLIST(loopC,2)-1,:)   ,...
                                          gridOUTPUT(correctionLIST(loopC,1),correctionLIST(loopC,2)+1,:)   ,...
                                          gridOUTPUT(correctionLIST(loopC,1)+1,correctionLIST(loopC,2)-1,:) ,...
                                          gridOUTPUT(correctionLIST(loopC,1)+1,correctionLIST(loopC,2),:)   ,...
                                          gridOUTPUT(correctionLIST(loopC,1)+1,correctionLIST(loopC,2)+1,:) ,...
                                         ] ) );
    voxelsforcorrection = (voxelsforcorrection>=4);
    gridOUTPUT(correctionLIST(loopC,1),correctionLIST(loopC,2),voxelsforcorrection) = 1;
  end %for

  %Remove the one-pixel border surrounding the array, if this was added
  %previously.
  if size(gridOUTPUT,1)>numel(gridCOx) || size(gridOUTPUT,2)>numel(gridCOy)
    gridOUTPUT = gridOUTPUT(2:end-1,2:end-1,:);
  end
  
end %if

%disp([' Ray tracing result: ',num2str(countCORRECTIONLIST),' rays (',num2str(countCORRECTIONLIST/(voxcountX*voxcountY)*100,'%5.1f'),'% of all rays) exactly crossed a facet edge and had to be computed by interpolation.'])

end

function [varargout] = CONVERT_meshformat(varargin)
%CONVERT_meshformat  Convert mesh data from array to faces,vertices format or vice versa
%==========================================================================
% AUTHOR        Adam H. Aitkenhead
% CONTACT       adam.aitkenhead@christie.nhs.uk
% INSTITUTION   The Christie NHS Foundation Trust
%
% USAGE         [faces,vertices] = CONVERT_meshformat(meshXYZ)
%         or... [meshXYZ]        = CONVERT_meshformat(faces,vertices)
%
% IN/OUTPUTS    meshXYZ  - Nx3x3 array - An array defining the vertex
%                          positions for each of the N facets, with: 
%                            1 row for each facet
%                            3 cols for the x,y,z coordinates
%                            3 pages for the three vertices
%
%               vertices - Nx3 array   - A list of the x,y,z coordinates of
%                          each vertex in the mesh.
%
%               faces    - Nx3 array   - A list of the vertices used in
%                          each facet of the mesh, identified using the row
%                          number in the array vertices.
%==========================================================================

%==========================================================================
% VERSION  USER  CHANGES
% -------  ----  -------
% 100817   AHA   Original version
% 111104   AHA   Housekeeping tidy-up.
%==========================================================================


if nargin==2 && nargout==1

  faces  = varargin{1};
  vertex = varargin{2};
   
  meshXYZ = zeros(size(faces,1),3,3);
  for loopa = 1:size(faces,1)
    meshXYZ(loopa,:,1) = vertex(faces(loopa,1),:);
    meshXYZ(loopa,:,2) = vertex(faces(loopa,2),:);
    meshXYZ(loopa,:,3) = vertex(faces(loopa,3),:);
  end

  varargout(1) = {meshXYZ};
  
  
elseif nargin==1 && nargout==2

  meshXYZ = varargin{1};
  
  vertices = [meshXYZ(:,:,1);meshXYZ(:,:,2);meshXYZ(:,:,3)];
  vertices = unique(vertices,'rows');

  faces = zeros(size(meshXYZ,1),3);

  for loopF = 1:size(meshXYZ,1)
    for loopV = 1:3
        
      %[C,IA,vertref] = intersect(meshXYZ(loopF,:,loopV),vertices,'rows');
      %The following 3 lines are equivalent to the previous line, but are much faster:
      
      vertref = find(vertices(:,1)==meshXYZ(loopF,1,loopV));
      vertref = vertref(vertices(vertref,2)==meshXYZ(loopF,2,loopV));
      vertref = vertref(vertices(vertref,3)==meshXYZ(loopF,3,loopV));
      
      faces(loopF,loopV) = vertref;
      
    end
  end
  
  varargout(1) = {faces};
  varargout(2) = {vertices};
  
  
end


end %function

function [coordNORMALS,varargout] = COMPUTE_mesh_normals(meshdataIN,invertYN)
% COMPUTE_mesh_normals  Calculate the normals for each facet of a triangular mesh
%==========================================================================
% AUTHOR        Adam H. Aitkenhead
% CONTACT       adam.aitkenhead@physics.cr.man.ac.uk
% INSTITUTION   The Christie NHS Foundation Trust
% DATE          March 2010
% PURPOSE       Calculate the normal vectors for each facet of a triangular
%               mesh.  The ordering of the vertices
%               (clockwise/anticlockwise) is also checked for all facets if
%               this is requested as one of the outputs.
%
% USAGE         [coordNORMALS] = COMPUTE_mesh_normals(meshdataIN)
%       ..or..  [coordNORMALS,meshdataOUT] = COMPUTE_mesh_normals(meshdataIN,invertYN)
%
% INPUTS
%
%    meshdataIN   - (structure)  Structure containing the faces and
%                   vertices of the mesh, in the same format as that
%                   produced by the isosurface command.
%         ..or..  - (Nx3x3 array)  The vertex coordinates for each facet,
%                   with:  1 row for each facet
%                          3 columns for the x,y,z coordinates
%                          3 pages for the three vertices
%    invertYN     - (optional)  A flag to say whether the mesh is to be
%                   inverted or not.  Should be 'y' or 'n'.
%
% OUTPUTS
%
%    coordNORMALS - Nx3 array   - The normal vectors for each facet, with:
%                          1 row for each facet
%                          3 columns for the x,y,z components
%
%    meshdataOUT  - (optional)  - The mesh data with the ordering of the
%                   vertices (clockwise/anticlockwise) checked.  Uses the
%                   same format as <meshdataIN>.
%
% NOTES       - Computing <meshdataOUT> to check the ordering of the
%               vertices in each facet may be slow for large meshes.
%             - It may not be possible to compute <meshdataOUT> for
%               non-manifold meshes.
%==========================================================================

%==========================================================================
% VERSION  USER  CHANGES
% -------  ----  -------
% 100331   AHA   Original version
% 101129   AHA   Can now check the ordering of the facet vertices.
% 101130   AHA   <meshdataIN> can now be in either of two formats.
% 101201   AHA   Only check the vertex ordering if that is required as one
%                of the outputs, as it can be slow for large meshes.
% 101201   AHA   Add the flag invertYN and make it possible to invert the
%                mesh
% 111004   AHA   Housekeeping tidy-up
%==========================================================================

%======================================================
% Read the input parameters
%======================================================

if isstruct(meshdataIN)==1
  faces         = meshdataIN.faces;
  vertex        = meshdataIN.vertices;
  coordVERTICES = zeros(size(faces,1),3,3);
  for loopa = 1:size(faces,1)
    coordVERTICES(loopa,:,1) = vertex(faces(loopa,1),:);
    coordVERTICES(loopa,:,2) = vertex(faces(loopa,2),:);
    coordVERTICES(loopa,:,3) = vertex(faces(loopa,3),:);
  end
else
  coordVERTICES = meshdataIN;
end

%======================================================
% Invert the mesh if required
%======================================================

if exist('invertYN','var')==1 && isempty(invertYN)==0 && ischar(invertYN)==1 && ( strncmpi(invertYN,'y',1)==1 || strncmpi(invertYN,'i',1)==1 )
  coV           = zeros(size(coordVERTICES));
  coV(:,:,1)    = coordVERTICES(:,:,1);
  coV(:,:,2)    = coordVERTICES(:,:,3);
  coV(:,:,3)    = coordVERTICES(:,:,2);
  coordVERTICES = coV;
end

%======================
% Initialise array to hold the normal vectors
%======================

facetCOUNT   = size(coordVERTICES,1);
coordNORMALS = zeros(facetCOUNT,3);

%======================
% Check the vertex ordering for each facet
%======================

if nargout==2
  startfacet  = 1;
  edgepointA  = 1;
  checkedlist = false(facetCOUNT,1);
  waitinglist = false(facetCOUNT,1);

  while min(checkedlist)==0
    
    checkedlist(startfacet) = 1;

    edgepointB = edgepointA + 1;
    if edgepointB==4
      edgepointB = 1;
    end
    
    %Find points which match edgepointA
    sameX = coordVERTICES(:,1,:)==coordVERTICES(startfacet,1,edgepointA);
    sameY = coordVERTICES(:,2,:)==coordVERTICES(startfacet,2,edgepointA);
    sameZ = coordVERTICES(:,3,:)==coordVERTICES(startfacet,3,edgepointA);
    [tempa,tempb] = find(sameX & sameY & sameZ);
    matchpointA = [tempa,tempb];
    matchpointA = matchpointA(matchpointA(:,1)~=startfacet,:);
  
    %Find points which match edgepointB
    sameX = coordVERTICES(:,1,:)==coordVERTICES(startfacet,1,edgepointB);
    sameY = coordVERTICES(:,2,:)==coordVERTICES(startfacet,2,edgepointB);
    sameZ = coordVERTICES(:,3,:)==coordVERTICES(startfacet,3,edgepointB);
    [tempa,tempb] = find(sameX & sameY & sameZ);
    matchpointB = [tempa,tempb];
    matchpointB = matchpointB(matchpointB(:,1)~=startfacet,:);
  
    %Find edges which match both edgepointA and edgepointB -> giving the adjacent edge
    [memberA,memberB] = ismember(matchpointA(:,1),matchpointB(:,1));
    matchfacet = matchpointA(memberA,1);
  
    if numel(matchfacet)~=1
      if exist('warningdone','var')==0
        warning('Mesh is non-manifold.')
        warningdone = 1;
      end
    else
      matchpointA = matchpointA(memberA,2);
      matchpointB = matchpointB(memberB(memberA),2);
      
      if checkedlist(matchfacet)==0 && waitinglist(matchfacet)==0
        %Ensure the adjacent edge is traveled in the opposite direction to the original edge  
        if matchpointB-matchpointA==1 || matchpointB-matchpointA==-2
          %Direction needs to be flipped
          [ coordVERTICES(matchfacet,:,matchpointA) , coordVERTICES(matchfacet,:,matchpointB) ] = deal( coordVERTICES(matchfacet,:,matchpointB) , coordVERTICES(matchfacet,:,matchpointA) );
        end
      end
    end
  
    waitinglist(matchfacet) = 1;
    
    if edgepointA<3
      edgepointA = edgepointA + 1;
    elseif edgepointA==3
      edgepointA = 1;
      checkedlist(startfacet) = 1;
      startfacet = find(waitinglist==1 & checkedlist==0,1,'first');
    end
  
  end
end

%======================
% Compute the normal vector for each facet
%======================

for loopFACE = 1:facetCOUNT
  
  %Find the coordinates for each vertex.
  cornerA = coordVERTICES(loopFACE,1:3,1);
  cornerB = coordVERTICES(loopFACE,1:3,2);
  cornerC = coordVERTICES(loopFACE,1:3,3);
  
  %Compute the vectors AB and AC
  AB = cornerB-cornerA;
  AC = cornerC-cornerA;
    
  %Determine the cross product AB x AC
  ABxAC = cross(AB,AC);
    
  %Normalise to give a unit vector
  ABxAC = ABxAC / norm(ABxAC);
  coordNORMALS(loopFACE,1:3) = ABxAC;
  
end %loopFACE

%======================================================
% Prepare the output parameters
%======================================================

if nargout==2
  if isstruct(meshdataIN)==1
    [faces,vertices] = CONVERT_meshformat(coordVERTICES);
    meshdataOUT = struct('vertices',vertices,'faces',faces);
  else
    meshdataOUT = coordVERTICES;
  end
  varargout(1) = {meshdataOUT};
end


%======================================================
end %function

function fvOut = splitFV( f, v )
%SPLITFV Splits faces and vertices into connected pieces
%   FVOUT = SPLITFV(F,V) separates disconnected pieces inside a patch defined by faces (F) and
%   vertices (V). FVOUT is a structure array with fields "faces" and "vertices". Each element of
%   this array indicates a separately connected patch.
%
%   FVOUT = SPLITFV(FV) takes in FV as a structure with fields "faces" and "vertices"
%
%   For example:
%     fullpatch.vertices = [2 4; 2 8; 8 4; 8 0; 0 4; 2 6; 2 2; 4 2; 4 0; 5 2; 5 0];
%     fullpatch.faces = [1 2 3; 1 3 4; 5 6 1; 7 8 9; 11 10 4];
%     figure, subplot(2,1,1), patch(fullpatch,'facecolor','r'), title('Unsplit mesh');
%     splitpatch = splitFV(fullpatch);
%     colours = lines(length(splitpatch));
%     subplot(2,1,2), hold on, title('Split mesh');
%     for i=1:length(splitpatch)
%         patch(splitpatch(i),'facecolor',colours(i,:));
%     end
%
%   Note: faces and vertices should be defined such that faces sharing a coincident vertex reference
%   the same vertex number, rather than having a separate vertice defined for each face (yet at the
%   same vertex location). In other words, running the following command: size(unique(v,'rows') ==
%   size(v) should return TRUE. An explicit test for this has not been included in this function so
%   as to allow for the deliberate splitting of a mesh at a given location by simply duplicating
%   those vertices.
%
%   See also PATCH

%   Copyright Sven Holcombe
%   $Date: 2010/05/19 $

% Extract f and v
if nargin==1 && isstruct(f) && all(isfield(f,{'faces','vertices'}))
    v = f.vertices;
    f = f.faces;
elseif nargin==2
    % f and v are already defined
else
    error('splitFV:badArgs','splitFV takes a faces/vertices structure, or these fields passed individually')
end

% Organise faces into connected fSets that share nodes
fSets = zeros(size(f,1),1,'uint32');
currentSet = 0;

while any(fSets==0)
    currentSet = currentSet + 1;
    %fprintf('Connecting set #%d vertices...',currentSet);
    nextAvailFace = find(fSets==0,1,'first');
    openVertices = f(nextAvailFace,:);
    while ~isempty(openVertices)
        availFaceInds = find(fSets==0);
        [availFaceSub, ~] = find(ismember(f(availFaceInds,:), openVertices)); 
        fSets(availFaceInds(availFaceSub)) = currentSet;
        openVertices = f(availFaceInds(availFaceSub),:);
    end
    %fprintf(' done! Set #%d has %d faces.\n',currentSet,nnz(fSets==currentSet));
end
numSets = currentSet;

% Create separate faces/vertices structures for each fSet
fvOut = repmat(struct('faces',[],'vertices',[]),numSets,1);

for currentSet = 1:numSets
    setF = f(fSets==currentSet,:);
    [unqVertIds, ~, newVertIndices] = unique(setF);
    fvOut(currentSet).faces = reshape(newVertIndices,size(setF));
    fvOut(currentSet).vertices = v(unqVertIds,:);
end
end

function [node,elem]=binsurface(img,nface,x,y,z)
%
% [node,elem]=binsurface(img,nface)
%
% fast isosurface extraction from 3D binary images
%
% author: Qianqian Fang, <fangq at nmr.mgh.harvard.edu>
%
% input: 
%   img:  a 3D binary image
%   nface: nface=3 or ignored - for triangular faces, 
%          nface=4 - square faces
%          nface=0 - return a boundary mask image via node
%
% output:
%   elem: integer array with dimensions of NE x nface, each row represents
%         a surface mesh face element 
%   node: node coordinates, 3 columns for x, y and z respectively
%
% the outputs of this subroutine can be easily plotted using 
%     patch('Vertices',node,'faces',elem,'FaceVertexCData',node(:,3),
%           'FaceColor','interp');
% if the surface mesh has triangular faces, one can plot it with
%     trisurf(elem,node(:,1),node(:,2),node(:,3))
%
% -- this function is part of iso2mesh toolbox (http://iso2mesh.sf.net)
% 

dim=size(img);
newdim=dim+[1 1 1];

% find the jumps (0->1 or 1->0) for all directions
d1=diff(img,1,1);
d2=diff(img,1,2);
d3=diff(img,1,3);
[ix,iy]=find(d1==1|d1==-1);
[jx,jy]=find(d2==1|d2==-1);
[kx,ky]=find(d3==1|d3==-1);

% compensate the dim. reduction due to diff, and 
% wrap the indices in a bigger array (newdim)
ix=ix+1;
[iy,iz]=ind2sub(dim(2:3),iy);
iy=sub2ind(newdim(2:3),iy,iz);

[jy,jz]=ind2sub([dim(2)-1,dim(3)],jy);
jy=jy+1;
jy=sub2ind(newdim(2:3),jy,jz);

[ky,kz]=ind2sub([dim(2),dim(3)-1],ky);
kz=kz+1;
ky=sub2ind(newdim(2:3),ky,kz);

id1=sub2ind(newdim,ix,iy);
id2=sub2ind(newdim,jx,jy);
id3=sub2ind(newdim,kx,ky);

if(nargin==2 && nface==0)
	elem=[id1 id2 id3];
	node=zeros(newdim);
	node(elem)=1;
	node=node(2:end-1,2:end-1,2:end-1)-1;
	return
end

% populate all the triangles
xy=newdim(1)*newdim(2);

if(nargin==1 || nface==3)  % create triangular elements
    elem=[id1 id1+newdim(1) id1+newdim(1)+xy; id1 id1+newdim(1)+xy id1+xy];
    elem=[elem; id2 id2+1 id2+1+xy; id2 id2+1+xy id2+xy];
    elem=[elem; id3 id3+1 id3+1+newdim(1); id3 id3+1+newdim(1) id3+newdim(1)];
else                       % create box elements
    elem=[id1 id1+newdim(1) id1+newdim(1)+xy id1+xy];
    elem=[elem; id2 id2+1 id2+1+xy id2+xy];
    elem=[elem; id3 id3+1 id3+1+newdim(1) id3+newdim(1)];
end
% compress the node indices
nodemap=zeros(max(elem(:)),1);
nodemap(elem(:))=1;
id=find(nodemap);

nodemap=0;
nodemap(id)=1:length(id);
elem=nodemap(elem);

% create the coordiniates
[yi,xi,zi]=ind2sub(newdim,id);
xi = xi(:); yi = yi(:); zi=zi(:);

%interpolate to go from image coordinates to real coordinates
xi  = interp1(x,xi);
yi  = interp1(y,yi);
zi  = interp1(z,zi);
node=[xi(:),yi(:),zi(:)];

% 
end

function p=smoothsurf(node,mask,conn,iter,useralpha,usermethod,userbeta)
%
% p=smoothsurf(node,mask,conn,iter,useralpha,usermethod,userbeta)
%
% smoothing a surface mesh
%
% author: Qianqian Fang (fangq<at> nmr.mgh.harvard.edu)
% date: 2007/11/21
%
% input:
%    node:  node coordinates of a surface mesh
%    mask:  flag whether a node is movable: 0 movable, 1 non-movable
%           if mask=[], it assumes all nodes are movable
%    conn:  input, a cell structure of length size(node), conn{n}
%           contains a list of all neighboring node ID for node n,
%           this can be computed from meshconn function
%    iter:  smoothing iteration number
%    useralpha: scaler, smoothing parameter, v(k+1)=(1-alpha)*v(k)+alpha*mean(neighbors)
%    usermethod: smoothing method, including 'laplacian','laplacianhc' and 'lowpass'
%    userbeta: scaler, smoothing parameter, for 'laplacianhc'
%
% output:
%    p: output, the smoothed node coordinates
%
% recommendations
%    Based on [Bade2006], 'Lowpass' method outperforms 'Laplacian-HC' in volume
%    preserving and both are significantly better than the standard Laplacian method
%
%    [Bade2006]  R. Bade, H. Haase, B. Preim, "Comparison of Fundamental Mesh 
%                Smoothing Algorithms for Medical Surface Models," 
%                Simulation and Visualization, pp. 289-304, 2006. 
%
% -- this function is part of iso2mesh toolbox (http://iso2mesh.sf.net)
%

p=node;
if(isempty(mask))
    nn=size(node,1);
    idx=1:nn;
else
    idx=find(mask==0)';
    nn=length(idx);
end
alpha=0.5;
method='laplacian';
beta=0.5;
if(nargin>4)
    alpha=useralpha;
    if(nargin>5)
        method=usermethod;
        if(nargin>6)
            beta=userbeta;
        end
    end
end
ibeta=1-beta;
ialpha=1-alpha;

for i=1:nn
    if(length(conn{idx(i)})==0)
        idx(i)=0;
    end
end
idx=idx(idx>0);
nn=length(idx);

if(strcmp(method,'laplacian'))
    for j=1:iter
        for i=1:nn
            p(idx(i),:)=ialpha*p(idx(i),:)+alpha*mean(node(conn{idx(i)},:)); 
        end
        node=p;
    end
elseif(strcmp(method,'laplacianhc'))
    for j=1:iter
        q=p;
        for i=1:nn
            p(idx(i),:)=mean(q(conn{idx(i)},:));
        end
        b=p-(alpha*node+ialpha*q);
        for i=1:nn
            p(idx(i),:)=p(idx(i),:)-(beta*b(i,:)+ibeta*mean(b(conn{idx(i)},:))); 
        end
    end
elseif(strcmp(method,'lowpass'))
    beta=-1.02*alpha;
    ibeta=1-beta;
    for j=1:iter
        for i=1:nn
            p(idx(i),:)=ialpha*node(idx(i),:)+alpha*mean(node(conn{idx(i)},:)); 
        end
        node=p;
        for i=1:nn
            p(idx(i),:)=ibeta *node(idx(i),:)+beta *mean(node(conn{idx(i)},:)); 
        end
        node=p;
    end
end
end

function [conn,connnum,count]=meshconn(elem,nn)
%
% [conn,connnum,count]=meshconn(elem,nn)
%
% create node neighbor list from a mesh
%
% author: Qianqian Fang (fangq<at> nmr.mgh.harvard.edu)
% date: 2007/11/21
%
% input:
%    elem:  element table of a mesh
%    nn  :  total node number of the mesh
%
% output:
%    conn:  output, a cell structure of length nn, conn{n}
%           contains a list of all neighboring node ID for node n
%    connnum: vector of length nn, denotes the neighbor number of each node
%    count: total neighbor numbers
%
% -- this function is part of iso2mesh toolbox (http://iso2mesh.sf.net)
%

conn=cell(nn,1);
dim=size(elem);
for i=1:dim(1)
  for j=1:dim(2)
    conn{elem(i,j)}=[conn{elem(i,j)},elem(i,:)];
  end
end
count=0;
connnum=zeros(1,nn);
for i=1:nn
    if(length(conn{i})==0) continue; end
    %conn{i}=sort(setdiff(unique(conn{i}),i));
    neig=unique(conn{i});
    neig(neig==i)=[];
    connnum(i)=length(conn{i});
    count=count+connnum(i);
end
end

function [mtf,nps]=parseFitinputs(args)

mtf=[];
nps=[];
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'MTF'
                mtf = args{i+1};
            case 'NPS'
                nps = args{i+1};
                
        end
    end
    
end

end
