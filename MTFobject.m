classdef MTFobject < handle
    
    properties
        s = 2;
        h = .1;
        c = .4;
        m = .3;
        withEFF=false; %With or without edge enhancement
        Ny = 1;     %nyquist frequency
        N =  50;    %number of pixels
    end
    
    properties (Dependent = true)
        mtf
        f
        df
        fsym
        idealMTF
    end
    
    events
        MTFPropertyChanged
    end
    
    methods
        function mtf = get.mtf(MTF)
            s=MTF.s;
            h=MTF.h;
            c=MTF.c;
            m=MTF.m;
            f=MTF.f;
            
           
            
            if MTF.withEFF
                p0 = (2*pi^2*f*m)./sinh(2*pi^2*f*m);  
                p1 = c*sqrt(8*pi^3)*i*f*sqrt(2*s)/(2*s);
                p2 = exp(4*i*pi*f*h)-1;
                p3 = exp(-(2*i*pi*f*s*h+ ((pi*f).^2) )/s);
                mtf = abs(p0-p1.*p2.*p3);
            else
                mtf = abs(((2*pi^2*f*m)./sinh(2*pi^2*f*m)));
            end
            
            
            mtf(1)=1;
     
        end
        
        function f = get.f(MTF)
            f=linspace(0,MTF.Ny,MTF.N);
        end
        
        function fsym = get.fsym(MTF)
            fsym=linspace(-MTF.Ny,MTF.Ny,MTF.N);
        end
        
        function set.s(MTF,s)
            MTF.s=s;
            notify(MTF,'MTFPropertyChanged')
        end
        
        function set.h(MTF,h)
            MTF.h=h;
            notify(MTF,'MTFPropertyChanged')
        end
        
        function set.c(MTF,c)
            MTF.c=c;
            notify(MTF,'MTFPropertyChanged')
        end
        
        function set.m(MTF,m)
            MTF.m=m;
            notify(MTF,'MTFPropertyChanged')
        end
        
        function set.withEFF(MTF,withEFF)
            MTF.withEFF=withEFF;
            notify(MTF,'MTFPropertyChanged')
        end
        
        function newMTF = copy(MTF)
            newMTF=MTFobject;
            newMTF.s=MTF.s;
            newMTF.h=MTF.h;
            newMTF.c=MTF.c;
            newMTF.m=MTF.m;
            newMTF.withEFF=MTF.withEFF;
            newMTF.Ny=MTF.Ny;
            newMTF.N=MTF.N;
        end
        
        function Iblur = BlurWithMTFs3D(I,psize,MTFx,MTFy,MTFz)
            %Set up MTFs to match the array and pixel sizes of the image to
            %be blurred
            MTFx=copy(MTFx);
            MTFy=copy(MTFy);
            MTFz=copy(MTFz);
            MTFx.N=size(I,2);
            MTFy.N=size(I,1);
            MTFz.N=size(I,3);
            Ny = 1./(2*psize);
            MTFx.Ny=Ny(1);
            MTFy.Ny=Ny(2);
            MTFz.Ny=Ny(3);
            
            %Get the 3D MTF
            MTF3D = get3DMTF(MTFx,MTFy,MTFz);
            
            %Blur the image
            MV=mean(I(:)); %get mean value
            I=I-MV; %remove mean
            F=fftn(I); %Fourier Transform
            F(1)=0;     %Remove DC component (same as filtering with a notch filter)
            MTF3D=fftshift(MTF3D); %Shift MTF
            H=F.*MTF3D; %Apply filtering
            Iblur = real(ifftn(H))+MV; %Inverse fourier transform
            
            
        end
         
        function MTF3D = get3DMTF(MTFx,MTFy,MTFz)
            [U,V,W]=meshgrid(MTFx.fsym,MTFy.fsym,MTFz.fsym);
            
            %Interpolate the MTF in all 3 directions
            Mu = interp1(MTFx.f,MTFx.mtf,abs(U));
            Mv = interp1(MTFy.f,MTFy.mtf,abs(V));
            Mw = interp1(MTFz.f,MTFz.mtf,abs(W));
            
            %Compute the full MTF as the product of each direction MTF
            MTF3D = Mu.*Mv.*Mw;
            
        end
        
        function Iblur = BlurWithMTFs2D(I,psize,MTFx,varargin)
            switch nargin
                case 3
                    MTFx=copy(MTFx);
                    MTFx.N=size(I,2);
                    Ny = 1./(2*psize);
                    MTFx.Ny=Ny(1);
                    MTF2D = get2DMTF(MTFx);
                case 4
                    MTFx=copy(MTFx);
                    MTFy=copy(varargin{1});
                    MTFx.N=size(I,2);
                    MTFy.N=size(I,1);
                    Ny = 1./(2*psize);
                    MTFx.Ny=Ny(1);
                    MTFy.Ny=Ny(2);
                    MTF2D = get2DMTF(MTFx,MTFy);
            end
            
            
            MV=mean(I(:)); %get mean value
            I=I-MV; %remove mean
            F=fftn(I); %Fourier Transform
            F(1)=0;     %Remove DC component (same as filtering with a notch filter)
            MTF2D=fftshift(MTF2D); %Shift MTF
            H=F.*MTF2D; %Apply filtering
            Iblur = real(ifftn(H))+MV; %Inverse fourier transform
            
        end
        
        function MTF2D = get2DMTF(MTFx,varargin)
            switch nargin
                case 1
                    [U,V]=meshgrid(MTFx.fsym,MTFx.fsym);
                    R = sqrt(U.^2 + V.^2);
                    MTF2D = interp1(MTFx.f,MTFx.mtf,R,'Linear',0);
                case 2
                    MTFy = varargin{1};
                    [U,V]=meshgrid(MTFx.fsym,MTFy.fsym);
                    Mu = interp1(MTFx.f,MTFx.mtf,abs(U));
                    Mv = interp1(MTFy.f,MTFy.mtf,abs(V));
                    MTF2D = Mu.*Mv;
            end
            
            
        end
        
        
    end
    
end