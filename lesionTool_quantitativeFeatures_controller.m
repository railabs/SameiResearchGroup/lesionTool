classdef lesionTool_quantitativeFeatures_controller < handle
    
    properties
        handles %all the graphics handles
        position
        units='normalized';
        Visible = true;
        buff = 30; %buffer of graphics elements in pixels
        
        pspacing_resampled = .5     %size of the resampled voxels
        nLevels = 10;               %number of gray levels used when quantizing image (for texture analysis)
        RLmax = 10;                 %Maximum run length used when getting texture features
        
        lesionMeasurements = [];
        
    end
    
    properties (Dependent = true)
        visibleString
    end
    
    events
    end
    
    methods
        function h = lesionTool_quantitativeFeatures_controller(parent,position,tool)
            
            %set class properties
            h.handles.parent = parent;
            h.handles.tool=tool;
            h.position=position;
            
            %make the panel
            h.handles.panel = uipanel(parent,'Units',h.units,'Position',position,'Title','Quantitative features',...
                'BackgroundColor','k','ForegroundColor','w','HighlightColor','k',...
                'TitlePosition','lefttop','FontSize',14);
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,h);
            set(h.handles.panel,'ResizeFcn',fun)
            
            %Make the measure button
            fun=@(hObject,evnt) callbacks(hObject,evnt,h);
            h.handles.MeasureButton = uicontrol(h.handles.panel,'Style','pushbutton','String','Measure','Units','Normalized',...
                'TooltipString','Measure quanitative lesion features','HorizontalAlignment','center','Enable','Off','Callback',fun);
            
            %Make resampled voxel size edit box
            %Add edit box for pixel pitch
            h.handles.PixelPitchEdit = uicontrol(h.handles.panel,'Style','edit','String',num2str(h.pspacing_resampled),'Units','Normalized',...
                'TooltipString','Resampled isotropic voxel size','HorizontalAlignment','center','Enable','On','UserData',h.pspacing_resampled,'Callback',fun);
            h.handles.PixelPitchText = uicontrol(h.handles.panel,'Style','text','String',' mm resampled voxel size','Units','Normalized',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %Make nLevels edit box
            %Add edit box for nLevels
            h.handles.NLevelsEdit = uicontrol(h.handles.panel,'Style','edit','String',num2str(h.nLevels),'Units','Normalized',...
                'TooltipString','N gray levels to use for texture features','HorizontalAlignment','center','Enable','On','UserData',h.nLevels,'Callback',fun);
            h.handles.NLevelsText = uicontrol(h.handles.panel,'Style','text','String',' N gray levels','Units','Normalized',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %Make max run length edit box
            %Add edit box for max run length
            h.handles.RLMaxEdit = uicontrol(h.handles.panel,'Style','edit','String',num2str(h.RLmax),'Units','Normalized',...
                'TooltipString','Maximum run length for texture features','HorizontalAlignment','center','Enable','On','UserData',h.RLmax,'Callback',fun);
            h.handles.RLMaxText = uicontrol(h.handles.panel,'Style','text','String',' Max run length','Units','Normalized',...
                'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            
            %Make the clear measurements
            h.handles.ClearButton = uicontrol(h.handles.panel,'Style','pushbutton','String','Clear Measurements','Units','Normalized',...
                'TooltipString','Clear measurement table','HorizontalAlignment','center','Enable','Off','Callback',fun);
            
            %Make the results table
            h.handles.table = uitable(h.handles.panel,'Units','Normalized','Visible','off');
            
            %add listener for mask changing
            addlistener(tool.handles.imtool_CT,'maskChanged',@h.handlePropEvents);
            
            
            
        end
        
        function handlePropEvents(h,src,evnt)
            switch evnt.EventName
                case 'maskChanged'
                    mask = getMask(h.handles.tool.handles.imtool_CT);
                    if any(mask(:))
                        set(h.handles.MeasureButton,'Enable','on');
                    else
                        set(h.handles.MeasureButton,'Enable','off');
                    end
            end
        end
        
        function set.pspacing_resampled(h,psize)
            if psize > 0
                h.pspacing_resampled=psize;
                set(h.handles.PixelPitchEdit,'String',num2str(psize),'UserData',psize);
            end
        end
        
        function set.nLevels(h,nLevels)
            nLevels = round(nLevels);
            if nLevels > 0
                h.nLevels=nLevels;
                set(h.handles.NLevelsEdit,'String',num2str(nLevels),'UserData',nLevels);
            end
        end
        
        function set.RLmax(h,RLmax)
            RLmax = round(RLmax);
            if RLmax > 0
                h.RLmax=RLmax;
                set(h.handles.RLMaxEdit,'String',num2str(RLmax),'UserData',RLmax);
            end
        end
        
        function set.position(h,pos)
            try
                set(h.handles.panel,'Position',pos);
                h.position = pos;
            end
        end
        
        function set.units(h,units)
            set(h.handles.panel,'Units',units);
            h.units = units;
        end
        
        function set.lesionMeasurements(h,lesionMeasurements)
            h.lesionMeasurements=lesionMeasurements;
            updateTableView(h);
        end
        
        function makeMeasurement(h)
            %This function uses the currently loaded image data and mask to
            %make a lesion measurement.
            tool = h.handles.tool; %handle to lesionTool object
            
            if tool.CTDataLoaded
                %Get the mask and make sure there is stuff in it
                mask = getMask(tool.handles.imtool_CT);
                if any(mask(:))
                    set(tool.handles.Status,'String','Measuring lesion')
                    drawnow
                    try
                        %Make the measurement object
                        res = lesionTool_quantitativeFeatures(getImage(tool.handles.imtool_CT),mask,tool.CTInfo.pspacing,h.pspacing_resampled,h.nLevels,h.RLmax);
                        
                        %add measurement to list of measurements
                        if isempty(h.lesionMeasurements)
                            h.lesionMeasurements=res;
                        else
                            h.lesionMeasurements = [h.lesionMeasurements; res];
                        end
                        
                        
                        
                    catch ME
                         showErrorMessage(tool,'Error measuring lesion')
                    end
                    
                    set(tool.handles.Status,'String','Ready')
                else
                    showErrorMessage(tool,'Error, segmentation mask is empty')
                end
            else
                showErrorMessage(tool,'Error, no CT data loaded')
                
            end
            
            
        end
        
        function clearMeasurements(h)
            h.lesionMeasurements=[];
        end
        
        function updateTableView(h)
            %This function updates the results table
            if isempty(h.lesionMeasurements)
                set(h.handles.table,'Visible','off','Data',[]);
                set(h.handles.ClearButton,'Enable','off');
            else
                %Get the labels
                labs = h.lesionMeasurements(1).featureValuesLabels;
                %Get the values
                vals = zeros(length(labs),length(h.lesionMeasurements));
                colLab{1} = 'Feature';
                for i=1:length(h.lesionMeasurements)
                    vals(:,i) = h.lesionMeasurements(i).featureValues;
                    colLab{end+1} = num2str(i);
                    
                end

                %Convert values to cell array
                vals = mat2cell(vals,ones(size(vals,1),1),ones(size(vals,2),1));
                %Put data into table
                set(h.handles.table,'Visible','on','Data',[labs vals],'RowName','','ColumnName',colLab);
                set(h.handles.ClearButton,'Enable','on');
            end
        end
        
        function str = get.visibleString(h)
            if h.Visible
                str = 'on';
            else
                str = 'off';
            end
        end
        
        function set.Visible(h,Visible)
            h.Visible=Visible;
            set(h.handles.panel,'Visible',h.visibleString)
        end
        
    end
    
end

function panelResizeFunction(hObject,events,h)
%This code runs when the size of the panel changes
pos = getPixelPosition(hObject);
buff=h.buff;
set(h.handles.MeasureButton,'Position',[0 4/5 1/3 1/5]);
set(h.handles.PixelPitchEdit,'Position',[0 3/5 1/6 1/5]);
set(h.handles.PixelPitchText,'Position',[1/6 3/5 1/6 1/5]);
set(h.handles.NLevelsEdit,'Position',[0  2/5 1/6 1/5]);
set(h.handles.NLevelsText,'Position',[1/6 2/5 1/6 1/5]);
set(h.handles.RLMaxEdit,'Position',[0 1/5 1/6 1/5]);
set(h.handles.RLMaxText,'Position',[1/6 1/5 1/6 1/5]);
set(h.handles.ClearButton,'Position',[0 0 1/3 1/5]);
set(h.handles.table,'Position',[1/3 0 2/3 1]);


end

function callbacks(hObject,evnt,h)
    switch hObject
        case h.handles.MeasureButton
            makeMeasurement(h)
        case h.handles.ClearButton
            clearMeasurements(h)
        case h.handles.PixelPitchEdit
            checkEditBoxValue(hObject,'GreaterThanZero');
            h.pspacing_resampled = get(hObject,'UserData');
        case h.handles.NLevelsEdit
            checkEditBoxValue(hObject,'GreaterThanZeroInt');
            h.nLevels=get(hObject,'UserData');
        case h.handles.RLMaxEdit
            checkEditBoxValue(hObject,'GreaterThanZeroInt');
            h.RLmax=get(hObject,'UserData');
            
    end
end