# lesionTool
lesionTool is a lesion simulation/insertion package for creating "hybrid" CT images (i.e., real CT images infused with virtually inserted lesion models).
This repository is written in Matlab with its object-oriented features. The two primary classes of interest are 
* lesionTool.m - a GUI interface which lets you desing and insert lesion models into CT data. It also lets you segment and fit a lesion to a real lesion. Open an instance of lesionTool by running the command: tool = lesionTool; in the MATLAB command line.
* lesionModel.m - the underlying class that defines the properties of a lesion. lesionTool is essentially a graphical interface that lets you adjust properties of a lesionModel and immediatly visualize the results. The lesionModel class is completely indepedent of the lesionTool class (i.e., you don't need to use the graphical interface to create and manipulate lesion models)

Using the handle to a lesionTool instance also allows you to create scripts to do repetetive tasks. Here are some examples of things that have been done in the past:
* Insert the same lesion into 100 different cases.
* Randomly generate a population of lesions and insert them at random locations into a large number of CT series.
* Insert the same lesion with slightly different properties into different locations in a single CT series.

These are just examples. Basically anything you want to do could probably be automated. A few example scripts are provided for how to perform common tasks using lesionTool.
FYI, the first time you run lesionTool, it will create an empty folder called “SavedLesions” which is used by LesionTool as a (very) poor man’s database of saved lesionModels.

## Dependencies
This repository requires Matlab's image processing toolbox (and possibly others). Additionally, the following RAI labs repositores are required:
* [ImageQuality](https://gitlab.oit.duke.edu/railabs/SameiResearchGroup/ImageQuality)
* [imtool3D](https://gitlab.oit.duke.edu/railabs/SameiResearchGroup/imtool3D)
* [UsefulTools](https://gitlab.oit.duke.edu/railabs/SameiResearchGroup/UsefulTools)
* [QuantitativeTools](https://gitlab.oit.duke.edu/railabs/SameiResearchGroup/QuantitativeTools) (Optional, if not available, quantitative measurement features will be hidden from the user in GUI)

## Authors
Justin Solomon, justin.solomon@duke.edu