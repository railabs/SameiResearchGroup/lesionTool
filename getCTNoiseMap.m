function noise = getCTNoiseMap(im,varargin)
%This function creates a noise map of a CT image

%Get the input parameters
[nhood, T, DilateMask, Gsig]=parseInputs(varargin);

%get the intitial noise map
noise=stdfilt(im,nhood);

%Create histogram
[P,EDGES] = histcounts(noise(:),'Normalization','probability');

%Get mask of noise outlier noise regions that will be filled in
[~,ind_mode]=max(P); %index of mode
ind_bins = find(P<T); %all bins that occur less than the threshold amount
ind=ind_bins>ind_mode;
ind_bins=ind_bins(ind);
T=EDGES(ind_bins(1));
mask = noise>T;

%Dilate mask if desired
if DilateMask
    mask=imdilate(mask,ones(5));
end

%Fill in the mask regions
noise  = regionfill(noise,mask);

%Smooth the mask with a Gaussian kernel
noise = imgaussfilt(noise,Gsig);

end

function [nhood, T, DilateMask, Gsig]=parseInputs(args)

nhood = strel('disk',5,0);
nhood = nhood.Neighborhood;
T = .04;
DilateMask = true;
Gsig = 25;

if length(args)>1
    for i=1:2:length(args)
        switch args{i}
            case 'NHOOD'
                nhood = strel('disk',ceil(args{i+1}/2),0);
                nhood = nhood.Neighborhood;
            case 'Thresh'
                T=args{i+1};
            case 'DilateMask'
                DilateMask=args{i+1};
            case 'Gsig'
                Gsig = args{i+1};
            otherwise
                disp(['Did not recognize setting: ' args{i}])
        end
    end
        
end


end