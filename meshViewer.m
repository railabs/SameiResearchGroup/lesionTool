classdef meshViewer < handle
    
    properties
        handles
        fv          %structured variable with the faces and vertices of the mesh
        FaceColor = 'none';
        EdgeColor = 'w';
        alpha = .9;
        FOV
        position = [0 0 1 1];
        units   = 'normalized';
        visible = 'on';
    end
    
    properties (Constant)
        tbuff = .1;
        h = 30;
        wbutt = 20;
    end
    
    properties (Dependent)
        center
    end
    
    events
        meshUpdated
    end
    
    methods
        %Constructor
        function viewer = meshViewer(fv,parent)
            viewer.handles.fig=getParentFigure(parent);
            
            h=viewer.h; %Pixel height of the top panel
            wbutt = viewer.wbutt; %Pixel size of the buttons
            
            %Make the panels
            viewer.handles.largePanel = uipanel(parent,'Units',viewer.units,'Position',viewer.position,'Title','','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','FontSize',12,'TitlePosition','leftbottom');
            set(viewer.handles.largePanel,'Units','Pixels'); pos=get(viewer.handles.largePanel,'Position'); set(viewer.handles.largePanel,'Units',viewer.units);
            viewer.handles.toolPanel = uipanel(viewer.handles.largePanel,'Units','Pixels','Position',[0 pos(4)-h pos(3) h],'Title','','BackgroundColor','k','ForegroundColor','w','HighlightColor','k');
            viewer.handles.axesPanel = uipanel(viewer.handles.largePanel,'Units','Pixels','Position',[0 0 pos(3) pos(4)-h],'Title','','BackgroundColor','k','ForegroundColor','w','HighlightColor','k');
            viewer.handles.parent = parent;
            
            %Set up the resize function
            fun=@(x,y) panelResizeFunction(x,y,viewer,h);
            set(viewer.handles.largePanel,'ResizeFcn',fun)
            
            %create the check boxes
            wp=h;
            w=wbutt;
            buff=(wp-w)/2;
            lp=0;
            
            %Create scale button
            viewer.handles.Button_Scale     =   uicontrol(viewer.handles.toolPanel,'Style','pushbutton','String','Scale','Position',[lp 0 3.5*w w],'TooltipString','Scale the size of the mesh by the specified amount');
            fun=@(hObject,evnt) callbacks(hObject,evnt,viewer,'ScaleShape');
            set(viewer.handles.Button_Scale,'Callback',fun)
            lp=lp+buff+3.5*w;
            
            %Create scale edit box
            viewer.handles.Edit_Scale     =   uicontrol(viewer.handles.toolPanel,'Style','edit','String','1','Position',[lp 0 w w],'TooltipString','Scale the size of the mesh by this amount','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,viewer,'Edit_Scale');
            set(viewer.handles.Edit_Scale,'Callback',fun)
            lp=lp+buff+w;
            
            %Create Grid checkbox
            viewer.handles.Checkbox_Grid     =   uicontrol(viewer.handles.toolPanel,'Style','Checkbox','String','Grid?','Position',[lp 0 3.5*w w],'TooltipString','Show Grid','BackgroundColor','k','ForegroundColor','w');
            fun=@(hObject,evnt) Checkbox_Callback(hObject,evnt,viewer,'Grid');
            set(viewer.handles.Checkbox_Grid,'Callback',fun)
            lp=lp+buff+3.5*w;
            
            %create the axis
            viewer.handles.Axes = axes('Parent',viewer.handles.axesPanel,'Color','none','XColor','w','YColor','w','Zcolor','w','YDir','normal','Position',[0 0 1 1]);
            axis equal; view(3); hold on; axis off; axis vis3d;
            fun=@(hObject,eventdata) axesButtonDownFunction(hObject,eventdata,viewer,'Axes');
            set(viewer.handles.Axes,'ButtonDownFcn',fun)
            
            %create the mesh
            viewer.handles.mesh  = patch(fv);
            set(viewer.handles.mesh,'FaceColor',viewer.FaceColor,'EdgeColor',viewer.EdgeColor);
            set(viewer.handles.mesh,'FaceAlpha',viewer.alpha);
            %light('Position',[-15 -15 15],'Style','Local');
            camlight;
            lighting gouraud
            fun=@(hObject,eventdata) axesButtonDownFunction(hObject,eventdata,viewer,'Mesh');
            set(viewer.handles.mesh,'ButtonDownFcn',fun)
            
            %create the center point
            viewer.handles.center = scatter3(0,0,0,'or','Filled');
            fun=@(hObject,eventdata) axesButtonDownFunction(hObject,eventdata,viewer,'Point');
            set(viewer.handles.center,'ButtonDownFcn',fun)
            
            %set the Faces and vertices property
            viewer.fv=fv;
            
            %set the axis limits
            viewer.FOV = [min(viewer.fv.vertices); max(viewer.fv.vertices)];
            
            
            
            
        end
        
        function set.units(viewer,units)
            set(viewer.handles.largePanel,'Units',units);
        end
        
        function set.position(viewer,position)
            set(viewer.handles.largePanel,'Position',position);
        end
        
        function set.FOV(viewer,FOV)
            viewer.FOV=FOV;
            set(viewer.handles.Axes,'Xlim',[FOV(1,1) FOV(2,1)],'Ylim',[FOV(1,2) FOV(2,2)],'Zlim',[FOV(1,3) FOV(2,3)]);
        end
        
        function set.visible(viewer,visible)
            set(viewer.handles.largePanel,'visible',visible)
        end
        
        function center = get.center(viewer)
            center = mean(viewer.fv.vertices,1);
        end
        
        function scaleMesh(viewer,scale)
            verts=viewer.fv.vertices;
            center = repmat(viewer.center,[size(verts,1) 1]);
            verts = center+scale*(verts-center);
            viewer.fv.vertices = verts;
        end
        
        function set.fv(viewer,fv)
            viewer.fv=fv;
            set(viewer.handles.mesh,'Faces',fv.faces,'Vertices',fv.vertices);
            center = viewer.center;
            set(viewer.handles.center,'XData',center(1),'YData',center(2),'ZData',center(3));
            notify(viewer,'meshUpdated')
        end
    end
    
    
    
end

function callbacks(hObject,evnt,viewer,label)

switch label
    case 'ScaleShape'
        
        scale = get(viewer.handles.Edit_Scale,'UserData');
        scaleMesh(viewer,scale);
        
    case 'Edit_Scale'
        %Get the old value
        old = get(hObject,'UserData');
        %get the new value
        new = str2num(get(hObject,'String'));
        if isempty(new)
            new = old;
        end
        if new<=0 || ~isreal(new)
            new = old;
        end
        set(hObject,'String',new);
        set(hObject,'UserData',new);
end

end

function Checkbox_Callback(hObject,evnt,viewer,label)
checked = get(hObject,'Value');
switch label
    case 'Grid'
        axes_old = gca;
        h = viewer.handles.Axes;
        axes(h);
        if checked
            axis on
            grid on
            box on
        else
            axis off
        end
        axes(axes_old);
end

end

function axesButtonDownFunction(hObject,eventdata,viewer,label)
%get the parent figure handle
fig = viewer.handles.fig;

%get the current button motion and button up functions of the figure
WBMF_old = get(fig,'WindowButtonMotionFcn');
WBUF_old = get(fig,'WindowButtonUpFcn');

switch label
    case 'Axes'
        Cmode = 'Rotate';
        data.bp = get(0,'PointerLocation');
        [data.Az, data.El] = view(viewer.handles.Axes);
        setptr(gcf,'rotate');
    case 'Mesh'
        Cmode = 'Rotate';
        data.bp = get(0,'PointerLocation');
        [data.Az, data.El] = view(viewer.handles.Axes);
        setptr(gcf,'rotate');
    case 'Point'
        Cmode = 'Rotate';
        data.bp = get(0,'PointerLocation');
        [data.Az, data.El] = view(viewer.handles.Axes);
        setptr(gcf,'rotate');
    otherwise
        Cmode = '';
end


%set the new window button motion function and button up function of the figure
fun = @(src,evnt) ButtonMotionFunction(src,evnt,viewer,Cmode,data);
fun2=@(src,evnt)  ButtonUpFunction(src,evnt,viewer,WBMF_old,WBUF_old);
set(fig,'WindowButtonMotionFcn',fun,'WindowButtonUpFcn',fun2);
end

function ButtonMotionFunction(src,evnt,viewer,Cmode,data)
switch Cmode
    case 'Rotate'
        cp = get(0,'PointerLocation');
        dp = cp - data.bp;
        az = data.Az-dp(1);
        el = data.El-dp(2);
        view(viewer.handles.Axes,az,el)
end

end

function ButtonUpFunction(src,evnt,viewer,WBMF_old,WBUF_old)
fig = viewer.handles.fig;
set(fig,'WindowButtonMotionFcn',WBMF_old,'WindowButtonUpFcn',WBUF_old);
setptr(fig,'arrow');
end

function panelResizeFunction(hObject,events,viewer,h)
units=get(viewer.handles.largePanel,'Units');
set(viewer.handles.largePanel,'Units','Pixels')
pos=get(viewer.handles.largePanel,'Position');
set(viewer.handles.largePanel,'Units',units);
set(viewer.handles.toolPanel,'Position',[0 pos(4)-h pos(3) h]);
set(viewer.handles.axesPanel,'Position',[0 0 pos(3) pos(4)-h])
end

function fig = getParentFigure(fig)
% if the object is a figure or figure descendent, return the
% figure. Otherwise return [].
while ~isempty(fig) & ~strcmp('figure', get(fig,'type'))
    fig = get(fig,'parent');
end
end