classdef scoutImageViewer < handle
    
    properties
        handles
    end
    
    properties (Dependent = true, Access = protected)
        position
    end
    
    methods
        
        %constructor
        function viewer = scoutImageViewer(parent,position,tool,lesionTool)
            
            %set the properties
            viewer.handles.parent = parent;
            viewer.handles.tool = tool;
            viewer.handles.lesionTool = lesionTool;
            
            %make the panel containing the MIPS
            viewer.handles.Panel = uipanel(parent,'Position',position,'Title','','BackgroundColor','k','ForegroundColor','w','HighlightColor','k');
            
            %make the axes for the MIP images
            viewer.handles.Axes_Sagittal = axes('Parent',viewer.handles.Panel,'Color','none','XColor','w','YColor','w','Zcolor','w','YDir','normal','OuterPosition',[0 0 .5 1]);
            hold on;
            title('Sagittal','Color','w')
            viewer.handles.Image_Sagittal = imshow(zeros(1),'Parent',viewer.handles.Axes_Sagittal,'Border','tight');
            axis on; box on; set(viewer.handles.Axes_Sagittal,'XTick',[],'YTick',[]);
            viewer.handles.Line_Sagittal = plot([.5 1.5],[1 1],'r','LineWidth',1,'Parent',viewer.handles.Axes_Sagittal,'Visible','off');
            
            viewer.handles.Axes_Coronal = axes('Parent',viewer.handles.Panel,'Color','none','XColor','w','YColor','w','Zcolor','w','YDir','normal','OuterPosition',[.5 0 .5 1]);
            axis off; hold on;
            title('Coronal','Color','w')
            viewer.handles.Image_Coronal = imshow(zeros(1),'Parent',viewer.handles.Axes_Coronal,'Border','tight');
            axis on; box on; set(viewer.handles.Axes_Coronal,'XTick',[],'YTick',[]);
            viewer.handles.Line_Coronal = plot([.5 1.5],[1 1],'r','LineWidth',1,'Parent',viewer.handles.Axes_Coronal,'Visible','off');
            
            
            %Add the needed listeners
            addlistener(lesionTool,'CTimageChanged',@viewer.handlePropEvents);
            h=getHandles(tool);
            addlistener(h.Slider,'Value','PostSet',@viewer.handlePropEvents);
        end
        
        function handlePropEvents(viewer,src,evnt)
            switch evnt.EventName
                case 'PostSet'
                    %This code is run whenever the user changes the slice
                     slice = round(get(evnt.AffectedObject,'Value'));
                     set(viewer.handles.Line_Sagittal,'Visible','on','Xdata',[slice+.5 slice+.5])
                     set(viewer.handles.Line_Coronal,'Visible','on','Ydata',[slice+.5 slice+.5])

                case 'CTimageChanged'
                    %This is the code that runs when a new CT image is
                    %loaded into the CT image viewer
                    
                    %Only run if there is actually some image data to show
                    format = viewer.handles.lesionTool.CTInfo.format;
                    
                    if strcmp(format,'none')
                        set(viewer.handles.Image_Sagittal,'CData',0)
                        set(viewer.handles.Image_Coronal,'CData',0)
                        set(viewer.handles.Line_Sagittal,'Visible','off')
                        set(viewer.handles.Line_Coronal,'Visible','off')
                    else
                        %get the image data
                        I = getImage(viewer.handles.tool);
                        
                        %Compute the Sagittal MIP
                        Sag = mat2gray(squeeze(max(I,[],2)),[0 1200]);
                        set(viewer.handles.Image_Sagittal,'CData',Sag)
                        set(viewer.handles.Line_Sagittal,'Visible','on','Ydata',[.5 size(Sag,1)+.5])
                        %Set the aspect ratio correctly
                        psize = [viewer.handles.lesionTool.CTInfo.pspacing(3) viewer.handles.lesionTool.CTInfo.pspacing(2) viewer.handles.lesionTool.CTInfo.pspacing(1)];
                        set(viewer.handles.Axes_Sagittal,'DataAspectRatio',1./psize,'Xlim',[0 size(Sag,2)+1],'Ylim',[0 size(Sag,1)+1])
                        
                        %Compute the coronal MIP
                        Cor = mat2gray(squeeze(max(I,[],1)),[0 1200])';
                        set(viewer.handles.Image_Coronal,'CData',Cor)
                        set(viewer.handles.Line_Coronal,'Visible','on','XData',[.5 size(Cor,2)+.5])
                        %Set the aspect ratio correctly
                        psize = [viewer.handles.lesionTool.CTInfo.pspacing(1) viewer.handles.lesionTool.CTInfo.pspacing(3) viewer.handles.lesionTool.CTInfo.pspacing(2)];
                        set(viewer.handles.Axes_Coronal,'DataAspectRatio',1./psize,'Xlim',[0 size(Cor,2)+1],'Ylim',[0 size(Cor,1)+1])
                        
                    end
                    
                    
                    
                    
                    
            end
        end
        
        function position = get.position(viewer)
            position  = get(viewer.handles.Panel,'Position');
        end
        
        
    end
end