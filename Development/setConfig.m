%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setConfig.m
%
% DESCRIPTION:
% Creates objects for storing configurable parameters, should be the main 
% place for user input. Supplants configuration files in breastMass.cxx 
% (Christian Graff, Oct 19, 2015), which was based on "A computational 
% model to generate simulated three-dimensional breast masses", Sisternes 
% et al., Med. Phys 42(2) 2015.
%
% Produced for lesionGenPipeline
%
% SEE ALSO: setDefaultConfig.m
%
% Rafael Fricks
% Created March 7, 2019
% RAI Labs, Duke University
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [base, mass, spicule] = setConfig()
% Base options
base.imgRes = 0.25;         %voxel size (mm)
base.seed = 50;             %random number generator seed
base.complexity = 2.0;      %complexity scaling, 1=normal
    
% Mass options
mass.lMax = 4;              %maximum spherical harmonic order [int]
mass.alpha = 10;            %mean mass radius (mm)
mass.meanSigma2 = 0.31;     %mean mass surface irregularity variance (mm^2)
mass.stdSigma2 = 0.04;      %mass surface irregularity standard deviation (mm^2)
mass.powerLaw = 4.0;        %covariance power law index
mass.meanLF = 688.7;        %mean number of low freq. modifications
mass.stdLF = 61.3;          %std. deviation of number of low freq. modifications
mass.meanShape = 0.59;      %mean LF shape distribution 0=spike, 1=bump
mass.stdShape = 0.15;       %std. deviation of LF shape 0=spike, 1=bump
mass.meanLFRad = 0.216;     %mean relative LF radius
mass.stdLFRad = 0.046;      %std. deviation of LF radius
mass.meanLFLen = 0.109;     %mean relative LF length
mass.stdLFLen = 0.012;      %std. deviation of LF length
mass.meanFuzzAlpha = 0.015; %mean fuzzy alpha
mass.stdFuzzAlpha = 0.0;    %std. deviation of fuzzy alpha

% Spiculation options
spicule.meanInitial = 1000; %mean number of initial segments
spicule.stdInitial = 60; %std. deviation number of initial segments
spicule.meanNeigh = 8.98; %mean max number of neighbor segments
spicule.stdNeigh = 1.89; %std. deviation max number of neighbor segments
spicule.meanInitRad = 0.024; %mean initial relative radius
spicule.stdInitRad = 0.0053; %std. deviation initial relative radius
spicule.meanRadDec = 0.93; %mean radius decrease
spicule.stdRadDec = 0.05; %std. deviation radius decrease
spicule.meanInitLen = 0.173; %mean initial relative length
spicule.stdInitLen = 0.018; %std. deviation initial relative length
spicule.meanLenDec = 0.95; %mean length decrease
spicule.stdLenDec = 0.05; %std. deviation length decrease
spicule.meanContProb = 0.717; %mean continue prob.
spicule.stdContProb = 0.057; %std. deviation continue prob.
%%% Assuming equi-probably symmetric and asymmetric branching
spicule.meanSymBifProb = 0.142; %mean symmentric bifurcation prob.
spicule.stdSymBifProb = 0.028; %std. deviation symm bif. prob.
spicule.meanAsymBifProb = 0.142; %mean asymmentric bifurcation prob.
spicule.stdAsymBifProb = 0.028; %std. deviation asymm bif. prob.
spicule.meanBranchAng = 6.55; %mean asymmentric bifurcation prob.
spicule.stdBranchAng = 0.62; %std. deviation asymm bif. prob.