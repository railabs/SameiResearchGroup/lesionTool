%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% createBranch.m
%
% DESCRIPTION:
% Recursive spiculation function for adding spiculation to generated lesions. 
% Based on breastMass.cxx (Christian Graff, Oct 19, 2015), which was based 
% on "A computational model to generate simulated three-dimensional breast 
% masses", Sisternes et al., Med. Phys 42(2) 2015.
%
% Produced for lesionGenPipeline
%
% Rafael Fricks
% Created March 7, 2019
% RAI Labs, Duke University
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Starts at line ~647 of original c++
function massVox = createBranch(r, theta, phi, thetaDir, phiDir, rad, len, contProb, gamma, massVox, massExtent, base, mass, spicule, bn)
% createBranch(r,theta,phi,thetaDir,phiDir,rad,len,contProb,gamma,massVox,rgen,rNormGen,vm)
bn = bn + 1; % branch number, for keeping track of recursion depth
% disp(['Initiating branch ' num2str(bn) ': ' datestr(datetime('now'))])

endRad = max(min((spicule.meanRadDec + randn(1).*spicule.stdRadDec)*rad, rad),0);

startPos = [r*sin(theta)*cos(phi) r*sin(theta)*sin(phi) r*cos(theta)];

startPix = ceil(massExtent([2 4 6])+startPos./base.imgRes)+1;
inROI = prod([(startPix <= size(massVox)) (startPix > [0 0 0])]); %is this pixel coordinate within the volume?
searchBox = zeros(1,6);    
if(inROI)
    searchRad = ceil(rad/base.imgRes);
    endRadPix = ceil(endRad/base.imgRes);
    
    for q = 1:3
        ind = 2*(q-1) + 1;
        searchBox(ind) = max(startPix(q)-searchRad, 0);
        searchBox(ind+1) = min(startPix(q)+searchRad, size(massVox,q));
    end
    
    a = (searchBox(1):1:searchBox(2));% - startPix(1);
    b = (searchBox(3):1:searchBox(4));% - startPix(2);
    c = (searchBox(5):1:searchBox(6));% - startPix(3);    
    [A,B,C] = meshgrid(a,b,c);
    
    dist = base.imgRes.*sqrt((A-startPix(1)).^2 + (B-startPix(2)).^2 + (C-startPix(3)).^2);
    withinDist = dist<=rad.*sqrt(2); %%sqrt(2) factor added to smoothen appearance in voxel rendering
    
    massVox(A(withinDist(:)),B(withinDist(:)),C(withinDist(:))) = 1;
%     for a=searchBox(1):1:searchBox(2)
%         for b=searchBox(3):1:searchBox(4)
%            for c=searchBox(5):1:searchBox(6)
%                dist = base.imgRes*sqrt((a-startPix(1))^2 +(b-startPix(2))^2 + (c-startPix(3))^2);
%                if(dist <= rad)
%                   massVox(a,b,c) = 1; 
%                end
%            end
%         end
%     end
    
    endPos = [startPos(1)+len*sin(thetaDir)*cos(phiDir) startPos(2)+len*sin(thetaDir)*sin(phiDir) startPos(3)+len*cos(thetaDir)];
    endPix = ceil(massExtent([2 4 6]) + endPos./base.imgRes)+1;
    endOK = prod([(endPix <= size(massVox)) (endPix > [0 0 0])]);
    
    % unit vector
    uvec = [sin(thetaDir)*cos(phiDir) sin(thetaDir)*sin(phiDir) cos(thetaDir)];
    
    % determine search box for segment
    for q = 1:3
        ind = 2*(q-1) + 1;
        searchBox(ind) = min(searchBox(ind), endPix(q)-endRadPix);
        searchBox(ind+1) = max(searchBox(ind+1), endPix(q)+endRadPix);
    end
    
    searchBox([1 3 5]) = max(searchBox([1 3 5]),[0 0 0]);
    searchBox([2 4 6]) = min(searchBox([2 4 6]),size(massVox)); 
    
    %fill segment
    a = (searchBox(1):1:searchBox(2));
    b = (searchBox(3):1:searchBox(4));
    c = (searchBox(5):1:searchBox(6));
    [A,B,C] = meshgrid(a,b,c);
    
    curPos = ([A(:) B(:) C(:)] - 1 - massExtent([2 4 6])).*base.imgRes; %%%% THIS IS IN position
    ldist = sum(uvec.*(curPos - startPos),2); 
    
    % current radius
    thisRad = rad + (endRad-rad).*ldist/len;
    % radial distance - pythagoras
    curRad = sqrt(sum((curPos-startPos).^2,2)-ldist.^2);

    isGood = (ldist<= len)&(ldist>=0);
    inds = isGood&(curRad <= thisRad.*sqrt(2)); %%sqrt(2) factor added to smoothen appearance in voxel rendering
    
    massVox(A(inds),B(inds),C(inds)) = 1;
    
    % fill ending sphere
    searchBox([1 3 5]) = max(endPix-endRadPix, [0 0 0]);
    searchBox([2 4 6]) = min(endPix+endRadPix, size(massVox));
    
    a = (searchBox(1):1:searchBox(2));
    b = (searchBox(3):1:searchBox(4));
    c = (searchBox(5):1:searchBox(6));
    [A,B,C] = meshgrid(a,b,c);
    
    dist = base.imgRes.*sqrt((A-endPix(1)).^2 + (B-endPix(2)).^2 + (C-endPix(3)).^2);
    withinDist = dist<=endRad.*sqrt(2); %%1.75 factor added to smoothen appearance in voxel rendering
    massVox(A(withinDist(:)),B(withinDist(:)),C(withinDist(:))) = 1;    

    % create children
    if(endRad>base.imgRes/2 && endOK >=0)
        newR = norm(endPos);
        newTheta = acos(endPos(3)/newR);
        newPhi = atan2(endPos(2), endPos(1));
        newLen = max(min(len.*(spicule.meanLenDec + randn(1).*spicule.stdLenDec),len),0);
        
        if(rand(1)< contProb)
            %one child
            
            %branching angle
            newthetaDir = thetaDir + gamma.*randn(1);
            newPhiDir = phiDir + gamma.*randn(1);
            
            massVox = createBranch(newR,newTheta,newPhi,newthetaDir,newPhiDir,endRad,newLen,contProb,gamma,massVox,massExtent,base,mass,spicule,bn);

        else
            %two children
            if(rand(1)< .5)
                %symmetric bifurcation
                dr = (.8 - .5)*rand(1) + .5;
                
                newThetaDir = thetaDir + gamma.*(randn(1)+1);
                newPhiDir = phiDir + gamma.*randn(1);
                massVox = createBranch(newR,newTheta,newPhi,newThetaDir,newPhiDir,endRad*(dr^(1/2.6)),newLen*(dr^(1/2.6)),contProb,gamma,massVox,massExtent,base,mass,spicule,bn);
                
                newThetaDir = thetaDir + gamma.*(randn(1)-1);
                newPhiDir = phiDir + gamma.*randn(1);
                massVox = createBranch(newR,newTheta,newPhi,newThetaDir,newPhiDir,endRad*((1-dr)^(1/2.6)),newLen*((1-dr)^(1/2.6)),contProb,gamma,massVox,massExtent,base,mass,spicule,bn);

            else
                %asymmetric bifurcation
                dr = (1-.8)*rand(1) + .8;
                newThetaDir = thetaDir + gamma.*randn(1);
                newPhiDir = phiDir + gamma.*randn(1);
                massVox = createBranch(newR,newTheta,newPhi,newThetaDir,newPhiDir,endRad*(dr^(1/2.6)),newLen*(dr^(1/2.6)),contProb,gamma,massVox,massExtent,base,mass,spicule,bn);
                
                newThetaDir = thetaDir + gamma.*randn(1);
                if(rand(1)<0.5)
                    newThetaDir = newThetaDir + pi/5;
                else
                    newThetaDir = newThetaDir - pi/5;
                end
                newPhiDir = phiDir + gamma*randn(1);
                massVox = createBranch(newR,newTheta,newPhi,newThetaDir,newPhiDir,endRad*((1-dr)^(1/2.6)),newLen*((1-dr)^(1/2.6)),contProb,gamma,massVox,massExtent,base,mass,spicule,bn);

            end
        end
        
    end
    
end