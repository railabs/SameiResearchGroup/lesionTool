%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lesionGenMk2.m
%
% DESCRIPTION:
% Spiculated lesion generation for breast tissue, to be used for lung
% nodules. Based on breastMass.cxx (Christian Graff, Oct 19, 2015), which
% was based on "A computational model to generate simulated three-dimensional
% breast masses", Sisternes et al., Med. Phys 42(2) 2015.
%
% Produced for lesionGenPipeline
%
% REQUIRES: setConfig.m, harmonicY.m, createBranch.m
%
% INPUTS: Configuration objects generated by setConfig.m
%
% OUTPUTS: Voxel mask stored in massVox variable
%
% Rafael Fricks
% Created March 7, 2019
% RAI Labs, Duke University
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Configuration variables and default values,
%%% in double precision floating point unless otherwise [indicated]
tic
disp(' ')

[base, mass, spicule] = setConfig();
% [base, mass, spicule] = setDefaultConfig();

%%% IGNORING THE CONFIG FILE READER HERE %%%

rng(base.seed);

%%% Start with the GRS model, Fig1a in paper or line ~143 in c++
% Sigma2 is normally distributed with params in config, or 0 if a
% negative value is generated
sigma2 = max(mass.meanSigma2.*base.complexity + randn(1).*mass.stdSigma2,0);

%Index error correction
pLawSum = sum(1./((1:mass.lMax).^mass.powerLaw));

Ctilde = log(1+sigma2).*(1./pLawSum);


% Random Spherical Harmonic Coefficients, line 159
slm = zeros(mass.lMax+1,mass.lMax+1);
Cl = Ctilde./((2:mass.lMax).^mass.powerLaw);
Cl = [0 0 Cl]; %Cl is 0 for l = 0, 1

for l = 0:mass.lMax %% can probably skip l = 0, 1, due to how Cl is defined
    coeffL = sqrt(2*pi*Cl(l+1)/(2*l+1));
    
    for m = 0:l
        if(m == 0)
            slm(l+1,m+1) = coeffL.*(randn(1)*sqrt(2) + 1i*0);
        else
            slm(l+1,m+1) = coeffL.*(randn(1) + 1i*randn(1));
        end
    end
end
% Equivalent to line ~189 in c++ program

%%% Determine number of spikes and bumps, for Fig1b stuff %%%
numLF = max(round(mass.meanLF + randn(1).*mass.stdLF),0);

typeFrac = min(max(mass.meanShape + randn(1).*mass.stdShape,0),1); %%% terrible truncation, as done in other script

numBump = round(numLF*typeFrac);
numSpike = numLF - numBump;
% Equivalent to line ~214 in c++ program

%%% Pick locations and sizes of low-frequency stuff Fig1b %%%
% pre-allocate LF for higher efficiency
for k = 1:numLF
    LF(k).length =  max((mass.meanLFLen.*base.complexity + randn(1).*mass.stdLFLen).*mass.alpha,0);
    LF(k).rad =   max((mass.meanLFRad + randn(1).*mass.stdLFRad).*mass.alpha,0);
    
    LF(k).spike = (k < numSpike);
    
    LF(k).phi = pi*(2*rand(1)-1); %%% these are uniformly distributed values
    LF(k).theta = acos(2*rand(1)-1);
    
    s = 0 + 1i*0;
    for l = 0:mass.lMax
        for m = 0:l
            s = s + slm(l+1,m+1) .* harmonicY(l, m, LF(k).theta, LF(k).phi);
            if(m>0)
                % do the negative index (line 244)
                s = s + (-1^m)*conj(slm(l+1,m+1)) .* harmonicY(l, -1*m, LF(k).theta, LF(k).phi);
            end
        end
    end
    
    sexp = exp(s);
    LF(k).r = (mass.alpha./sqrt(1 + sigma2)).*real(sexp); %Eq 1
end
disp([datestr(datetime('now')) ' : Initialization Complete'])
toc %%% approx. 2-3 sec runtime until here
% Equivalent to line ~256 in c++ program

%%% Create radial extent map 
ustep = ceil(mass.alpha*2*pi/base.imgRes);
vstep = ceil(mass.alpha*pi/base.imgRes);

% track max value
rmax = 0;

rmap = zeros(ustep,vstep);
rmapSpacing = [1./(ustep-1) 1./(vstep-1) 1];

% uval = (0:ustep-1)*rmapSpacing(1);
% vval = (0:vstep-1)*rmapSpacing(2);
% 
% theta = acos(2.*vval-1);
% phi = -1*pi + 2*pi.*uval;
% 
% inds = zeros(mass.lMax+1,mass.lMax+1);

disp(' ')
disp([datestr(datetime('now')) ' : Generating Random Gaussian Sphere (RGS) + Low Frequency (LF) Modifications'])     

% Fill in rmap
for i=1:ustep % note: does not match C++ indices
    uval = (i-1)*rmapSpacing(1); %index correction
    for j = 1:vstep % note: does not match C++ indices
        vval = (j-1)*rmapSpacing(2); %index correction
        
        theta = acos(2*vval-1);
        phi = -pi + 2*pi*uval;
        
        s = 0 + 1i*0;
        for l = 0:mass.lMax
            for m = 0:l
                s = s + slm(l+1,m+1) .* harmonicY(l, m, theta, phi);
                inds(l+1,m+1) = 1;
                if(m>0)
                % do the negative index (line 296)
                    s = s + (-1^m)*conj(slm(l+1,m+1)) .* harmonicY(l, -1*m, theta, phi);
                end
            end
        end
        
        
        sexp = exp(s);
        
        rGRS = (mass.alpha./sqrt(1 + sigma2)).*real(sexp); %Eq 1
        
        rLF = rGRS;
        
        for a = 1:numLF %this was in parallel using OpenMP
            dist = (rGRS*sin(theta)*sin(phi) - LF(a).r*sin(LF(a).theta)*sin(LF(a).phi)).^2 ...
            + (rGRS*sin(theta)*cos(phi) - LF(a).r*sin(LF(a).theta)*cos(LF(a).phi)).^2 ...
            + (rGRS*cos(theta) - LF(a).r*cos(LF(a).theta)).^2;
            
            dist = sqrt(dist);
            
            if(dist <= LF(a).rad)
                if(LF(a).spike) %% if its a spike
                    dr = LF(a).length.*(((LF(a).rad - dist)./LF(a).rad).^4); %Eq. 5
                else %% if its a bump
                    dr = LF(a).length.*(exp((-1*dist/2)/((LF(a).rad/2).^2)) - exp(-4.5)); %Eq. 6
                end
                
                rLF = rLF + dr;
            end
        end
        
        
        
        rFuzz = rLF;
        fuzzAlpha = mass.meanFuzzAlpha + randn(1).*mass.stdFuzzAlpha;
        
        rFuzz = rFuzz*(1 + fuzzAlpha*randn(1)); % Eq. 7
        
        % save in rmap
        rmap(i,j) = rFuzz;
        
%         rmax = max(rFuzz,rmax);
    end
end
rmax = max(rmap(:));
% toc
%%% up to line 354 in c++ program

%%% CREATE MASS %%%
% mass = zeros();

massSpacing = [base.imgRes base.imgRes base.imgRes];
massDim = ceil(4*rmax/base.imgRes); %why 2*2 in the original? is 4.

massExtent = ceil([-massDim/2, -massDim/2+massDim-1, -massDim/2, -massDim/2+massDim-1, -massDim/2, -massDim/2+massDim-1]);
massVox = zeros(massDim,massDim,massDim);

%%% for interpolation step %%%
% uval = (0:ustep-1).*rmapSpacing(1);
% vval = (0:vstep-1).*rmapSpacing(2);
% theta = acos(2.*vval-1);
% phi = -pi + 2.*pi.*uval;
% [Theta,Phi] = meshgrid(theta,phi);

%%% original implementation %%%
% x_ind = 1;
% y_ind = 1;
% z_ind = 1;

% for i = massExtent(1):1:massExtent(2)
%     y_ind = 1;
%     for j = massExtent(3):1:massExtent(4)
%         z_ind = 1;
%        for k = massExtent(5):1:massExtent(6)
%            % convert to spherical coordinates
%            r = sqrt(i*i* + j*j + k*k)*base.imgRes;
%            theta = acos((k*base.imgRes)/r);
%            phi = atan2(j*base.imgRes, i*base.imgRes);
%            
%            uval = 0.5 + (phi/2)/pi;
%            vval = (cos(theta)+1)/2;
%            
%            coord = [uval, vval, 0];
%            %%% INTERP THE RVAL FROM COORD %%%
%            rval = interp2(Theta,Phi,rmap,coord(1),coord(2));
%            
%            if(r <= rval)
%                massVox(x_ind,y_ind,z_ind) = 1;
%            else
%                massVox(x_ind,y_ind,z_ind) = 0;
%            end
%            z_ind = z_ind + 1;
%        end
%        y_ind = y_ind + 1;
%     end
%     x_ind = x_ind + 1;
% end

%%% vectorized implementation %%%
uval = (0:ustep-1).*rmapSpacing(1);
vval = (0:vstep-1).*rmapSpacing(2);

theta = acos(2.*vval-1);
phi = -pi + 2.*pi.*uval;
% 
% dTheta = mean(theta(2:end)-theta(1:end-1));
% theta = [theta-max(theta)-dTheta theta theta+max(theta)+dTheta];
% 
% dPhi = phi(2) - phi(1);
% phi = [phi-max(phi)-dPhi phi phi+max(phi)+dPhi];

[Theta,Phi] = meshgrid(theta,phi);

x = massExtent(1):1:massExtent(2);
y = massExtent(3):1:massExtent(4);
z = massExtent(5):1:massExtent(6);
[X,Y,Z] = meshgrid(x,y,z);

r = sqrt(X.*X + Y.*Y + Z.*Z).*base.imgRes;

theta = acos((Z.*base.imgRes)./r);
phi = atan2(Y.*base.imgRes, X.*base.imgRes);

% Uval = 0.5 + (phi./2)./pi;
% Vval = (cos(theta)+1)./2;

rval = interp2(Theta,Phi,rmap,theta,phi,'linear');
% rval = interp2(Theta,Phi,rmap,Vval,Uval,'linear');
% [Theta, Phi, rmap] = wrapSpherical(Theta(:),Phi(:),rmap(:));
% F = scatteredInterpolant([Theta(:) Phi(:)],rmap(:),'linear');
% rval = F(Vval,Uval);

massVox = double(r <= rval);
disp([datestr(datetime('now')) ' : RGS + LF Complete'])
toc
disp(' ')


%% Add spiculations here (line 418 in c++)
disp([datestr(datetime('now')) ' : Adding Spiculations'])     

numInitial = max(round(spicule.meanInitial + randn(1).*spicule.stdInitial),0);

contProb = min(max(spicule.meanContProb + randn(1).*spicule.stdContProb,0),1);

gamma = min(max((spicule.meanBranchAng + randn(1).*spicule.stdBranchAng).*(pi/180),0),pi);

initialSeg = 0;

while(initialSeg<numInitial)
    phiIni = (2.*rand(1)-1).*pi;
    thetaIni = acos(2.*rand(1)-1);
    numSeg = max(round(spicule.meanNeigh + randn(1).*spicule.stdNeigh),0);
    
    nDone = 0;
    
    while((nDone < numSeg)&& (initialSeg<numInitial))
        % branch angle
        phi = phiIni + (floor(log2(nDone+1)) + rand(1)) * (pi/50);
        theta = thetaIni + (floor(log2(nDone+1)) + rand(1)) * (pi/50);
        
        rad = max((spicule.meanInitRad + randn(1).*spicule.stdInitRad).*mass.alpha,0);        
        
        len = max((spicule.meanInitLen + randn(1).*spicule.stdInitLen).*mass.alpha,0);
            
        thetaDir = theta + randn(1).*(15*pi/180);
        phiDir = phi + randn(1).*(15*pi/180);
        
%         uval = 0.5 + (phi/2)/pi;        
%         vval = (cos(theta)+1)/2;
        
        %%% INTERP THE RVAL FROM COORD %%%
        r = interp2(Theta,Phi,rmap,theta,phi);
%         r = interp2(Theta,Phi,rmap,vval,uval);
        
        massVox = createBranch(r,theta,phi,thetaDir,phiDir,rad,len,contProb,gamma,massVox,massExtent,base,mass,spicule,-1);
        
        nDone = nDone + 1;
        initialSeg = initialSeg + 1;
        
    end
    
    
end
disp([datestr(datetime('now')) ' : Lesion Generation Complete'])     
toc
disp(' ')




