%Here are some example scripts for common lesionTool tasks
clear; close all;
%% Open lesionTool, load CT images, generate a lesion, insert the lesion, write out images
%Input settings------------------------------------------------------------
imageDir = '/Path/To/Your/DICOMS/'; %Path folder containing dicoms
output = '/Path/To/Output/DICOMS/'; %Path to folder to save manipulated dicoms
pos = [177 267 40]; %Insertion location (in pixel coordinates). Center of lesion will be located here.


%Open an instance of lesionTool
tool = lesionTool;

%Load in DICOMS
loadCTdata(tool,imageDir,'DICOMs');

%Generate a random lung nodule
lesion = generateRandomLesionModel('Lung');

%Push the lesion model into lesionTool GUI
setUpdatedLesion(tool,lesion);

%Insert the lesion using default settings (simple superposition)
insertLesionAtLocation(tool,pos,'Pixel');

%Write out new dicoms
if ~isdir(output)
    mkdir(output)
end
exportImage(tool,output)

