classdef lesionTool < handle
    properties
        handles     %structured variable with all the handles
        paths       %structured variable with the folders paths containing assets and saved lesion
        lesion      %lesion model object
        lesionTable %Table of all the lesions in the library
        CTInfo      %structured variable with information about the loaded CT data
        insertedLesion %structured variable with info about inserted lesions
        meshViewer  %
        NPS
        MTF_xy
        MTF_z
        Visible = true;
        projectionDistanceScaling=1;
    end
    
    properties (GetAccess = public, SetAccess = private)
        noiseMeasured = false;
        noiseGenerated = false;
        generatedNoise=[];
    end
    
    properties (Constant = true)
        dependentRepositories = {'lesionTool';'ImageQuality';'imtool3D';'UsefulTools'};
        optionalRepositories = {'QuantitativeTools'};
    end
    
    properties (SetAccess = immutable)
        gitSHA1CheckSums %Cell array of git branch unique id's corresponding to each dependent repository
        gitBranchNames   %Cell array of git branch names corresponding  to each dependent repository
        gitSHA1CheckSums_optional %Cell array of git branch unique id's corresponding to each optional dependent repository
        gitBranchNames_optional   %Cell array of git branch names corresponding  to each optional dependent repository
    end
    
    properties (Dependent = true)
        lesionTableIndex    %row index of the currently selected lesion in the library
        filteredLesionTable %the lesion table that has been filtered by the type and tag
        CTDataLoaded
    end
    
    events
        fittingLesion
        doneFittingLesion
        insertingLesion
        doneInsertingLesion
        updatingLesion
        updatedLesion
        LesionPropertyChanged
        TexturePropertyChanged
        CTimageChanged
        readingCTData
        readCTData
        warningMessage
        exportingImages
        exportedImages
    end
    
    methods
        
        function tool = lesionTool(varargin) %constructor
            %Check for the dependent repositories
            %Get git info (only when running within Matlab environment);
            if ~isdeployed
                %Check to make sure that dependent repositories are available
                [proceed,RepoPaths] = checkForDependentRespositories(tool.dependentRepositories,true);
                if any(~proceed)
                    warning('Missing one or more dependent repositories')
                end
                
                %Get the repository information
                %for i=1:length(RepoPaths)
                %    [ids{i,1},branchNames{i,1}] = getGitCommitID(RepoPaths{i});
                %end
            else
                ids=repmat({'UNKOWN'},size(tool.dependentRepositories));
                branchNames=repmat({'UNKOWN'},size(tool.dependentRepositories));
                
                
            end
            
            %  tool.gitSHA1CheckSums=ids;
            %  tool.gitBranchNames=branchNames;
            
            %Check for the optional dependent repositories
            %Get git info (only when running within Matlab environment);
            clear ids branchNames;
            if ~isdeployed
                %Check to make sure that dependent repositories are available
                [~,RepoPaths] = checkForDependentRespositories(tool.optionalRepositories,false);
                
                %Get the repository information
                %for i=1:length(RepoPaths)
                %    [ids{i,1},branchNames{i,1}] = getGitCommitID(RepoPaths{i},true);
                %end
            else
                ids=repmat({'UNKOWN'},size(tool.optionalRepositories));
                branchNames=repmat({'UNKOWN'},size(tool.optionalRepositories));
                
                
            end
            
            tool.gitSHA1CheckSums_optional=ids;
            tool.gitBranchNames_optional=branchNames;
            
            
            
            %make the figure-----------------------------------------------
            tool.handles.fig = figure('Units','Normalized','Position',[.1 .1 .8 .8],'Color','k','MenuBar','none','Name','Lesion Tool','NumberTitle','off');
            %--------------------------------------------------------------
            
            
            %get the paths-------------------------------------------------
            p = mfilename('fullpath');
            [root, ~, ~] = fileparts(p);
            root = [root filesep];
            tool.paths.root = root;
            %check if the SavedLesions folder exists (else create it)
            if ~exist(fullfile(root,'SavedLesions'),'dir')
                mkdir(root,'SavedLesions')
                disp('Initializing Saved Lesions Folder')
            end
            tool.paths.SavedLesions = fullfile(root,['SavedLesions' filesep]);
            %--------------------------------------------------------------
            
            %Set up the lesion table---------------------------------------
            tool.lesionTable = createLesionTable(tool);
            %--------------------------------------------------------------
            %make the tab group
            tool.handles.bigTabGroup = uitabgroup(tool.handles.fig,'Units','Normalized','Position',[0 .05 1 .95],'TabLocation','top');
            tool.handles.LesionTab = uitab(tool.handles.bigTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Lesion');
            tool.handles.CTTab = uitab(tool.handles.bigTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','CT Data');
            %make the status panel
            tool.handles.StatusTab = uipanel(tool.handles.fig,'Position',[0 0 1 .05],'Title','Status','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            tool.handles.Status=uicontrol(tool.handles.StatusTab,'Style','text','String','','Units','Normalized','Position',[0 0 1 1],'BackgroundColor','k','ForegroundColor','r','FontSize',12,'HorizontalAlignment','Left');
            %make the command line tab
            tool.handles.CommandTab = uipanel(tool.handles.fig,'Position',[.5 0 .5 .05],'Title','Command Line','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14,'Visible','off');
            tool.handles.CommandLine=uicontrol(tool.handles.CommandTab,'Style','edit','String','','Units','Normalized','Position',[0 0 1 1],'BackgroundColor','k','ForegroundColor','w','FontSize',12,'HorizontalAlignment','Left');
            %Add listeners for when the lesion is updated to set the status string
            addlistener(tool,'updatingLesion',@tool.handlePropEvents);
            addlistener(tool,'updatedLesion',@tool.handlePropEvents);
            
            
            
            
            %make components of the lesion tab-----------------------------
            tool.lesion = lesionModel;  %create a default lesion
            tool.lesion.scale = 4;
            I = voxelizeLesion(tool.lesion);
            tool.handles.imtool_Panel = uipanel(tool.handles.LesionTab,'Position',[0 .5 .5 .5],'Title','2D viewer','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            slice = round(size(I,3)/2);
            tool.handles.imtool_Lesion = imtool3D(I,[0 0 1 1],tool.handles.imtool_Panel);
            h=getHandles(tool.handles.imtool_Lesion);
            set(h.Tools.Crop,'Visible','off')
            set(h.Tools.Help,'Visible','off')
            set(h.Tools.PaintBrush,'Visible','off')
            set(h.Tools.SmartBrush,'Visible','off')
            setCurrentSlice(tool.handles.imtool_Lesion,slice)
            tool.handles.viewer3D_Panel = uipanel(tool.handles.LesionTab,'Position',[0 0 .5 .5],'Title','3D viewer','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            tool.handles.viewer = lesion3DViewer(tool.handles.viewer3D_Panel,[0 0 1 1],tool.lesion,tool.handles.imtool_Lesion,tool);
            tool.handles.propertiesTabGroup = uitabgroup(tool.handles.LesionTab,'Units','Normalized','Position',[.5 .1 .5 .9],'TabLocation','top');
            tool.handles.ShapeTab = uitab(tool.handles.propertiesTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Size/Shape');
            tool.handles.ProfileTab = uitab(tool.handles.propertiesTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Profile');
            tool.handles.TextureTab = uitab(tool.handles.propertiesTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Texture');
            tool.handles.VoxelizeTab = uitab(tool.handles.propertiesTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Voxelization');
            tool.handles.RandomTab = uitab(tool.handles.propertiesTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Random Generation');
            tool.handles.LibraryTab = uitab(tool.handles.propertiesTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Library');
            tool.handles.ButtonLesionRefresh = uicontrol(tool.handles.LesionTab,'Style','pushbutton','String','Refresh Lesion','Units','Normalized','TooltipString','Push to apply all changes made to the lesion','Position',[.5 0 .5 .1],'Enable','off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Button_LesionRefresh');
            set(tool.handles.ButtonLesionRefresh,'Callback',fun)
            addlistener(tool,'LesionPropertyChanged',@tool.handlePropEvents);
            %--------------------------------------------------------------
            
            
            %make components of the size/shape tab-------------------------
            buff = 30;  %buffer in pixels
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'SizeShape');
            set(tool.handles.ShapeTab,'ResizeFcn',fun)
            %shapeType pulldown menu
            names={'Spoke','Mesh','Simple Mesh'};
            tool.handles.Pulldown_shapeType  =   uicontrol(tool.handles.ShapeTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Select a shape type');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_shapeType');
            set(tool.handles.Pulldown_shapeType,'Callback',fun)
            %Plus size button
            tool.handles.PlusShapeButton = uicontrol(tool.handles.ShapeTab,'Style','pushbutton','String','+','Units','Pixels','TooltipString','Add constant to size','HorizontalAlignment','left','Enable','On');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'PlusShapeButton');
            set(tool.handles.PlusShapeButton,'Callback',fun)
            %Multiply size button
            tool.handles.MultiplyShapeButton = uicontrol(tool.handles.ShapeTab,'Style','pushbutton','String','x','Units','Pixels','TooltipString','Multiply size by constant','HorizontalAlignment','left','Enable','On');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MultiplyShapeButton');
            set(tool.handles.MultiplyShapeButton,'Callback',fun)
            %Multiply constant factor
            tool.handles.MultiplyTextEdit = uicontrol(tool.handles.ShapeTab,'Style','edit','String','1','Units','Pixels','TooltipString','Add/Multiply constant','HorizontalAlignment','left','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MultiplyTextEdit');
            set(tool.handles.MultiplyTextEdit,'Callback',fun)
            %Spoke Shape Table
            [rowNames,columnNames,columnWidths] = getShapeTableNames(tool.lesion);
            data.theta_b = tool.lesion.shape.theta_b; data.phi_b = tool.lesion.shape.phi_b;
            tool.handles.SpokeShapeTable = uitable(tool.handles.ShapeTab,'Data',tool.lesion.shape.Rb,'Units','Pixels','RowName',rowNames,'ColumnName',columnNames,'ColumnWidth',columnWidths,'ColumnEditable',true,'UserData',data);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'ShapeTableEdit');
            set(tool.handles.SpokeShapeTable,'CellEditCallback',fun)
            %Shape Filter field
            tool.handles.ShapeFilterTextEdit = uicontrol(tool.handles.ShapeTab,'Style','edit','String',num2str(tool.lesion.shape.f),'Units','Pixels','TooltipString','Size of the shape filter','HorizontalAlignment','left','UserData',tool.lesion.shape.f);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'ShapeFilterTextEdit');
            set(tool.handles.ShapeFilterTextEdit,'Callback',fun)
            tool.handles.ShapeFilterText = uicontrol(tool.handles.ShapeTab,'Style','text','String','Shape filter size','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Mesh Shape file field
            tool.handles.MeshShapeTextEdit = uicontrol(tool.handles.ShapeTab,'Style','edit','String',tool.paths.root,'Units','Pixels','TooltipString','Enter the path to the mesh file','HorizontalAlignment','left','Visible','Off','UserData',tool.paths.root);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Edit_meshPath');
            set(tool.handles.MeshShapeTextEdit,'Callback',fun)
            %Choose mesh file button
            tool.handles.MeshShapeButton = uicontrol(tool.handles.ShapeTab,'Style','pushbutton','String','...','Units','Pixels','TooltipString','Choose file','HorizontalAlignment','left','Visible','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Button_meshPath');
            set(tool.handles.MeshShapeButton,'Callback',fun)
            %Make the mesh viewer
            [F,V] = getDefaultSphereMesh; fv.faces=F; fv.vertices=V;
            tool.handles.meshViewer = meshViewer(fv,tool.handles.ShapeTab); tool.handles.meshViewer.units='Pixels'; tool.handles.meshViewer.visible='Off';
            tool.meshViewer = tool.handles.meshViewer;
            %Load mesh file button
            tool.handles.LoadMeshShapeButton = uicontrol(tool.handles.ShapeTab,'Style','pushbutton','String','Load Mesh','Units','Pixels','TooltipString','Choose file','HorizontalAlignment','left','Visible','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'LoadMeshShapeButton');
            set(tool.handles.LoadMeshShapeButton,'Callback',fun)
            %--------------------------------------------------------------
            
            
            %make components of profile tab--------------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'Profile');
            set(tool.handles.ProfileTab,'ResizeFcn',fun)
            pos = getPixelPosition(tool.handles.ProfileTab);
            %Make background invisible axes
            tool.handles.ProfileBackgroundAxes = axes('Parent',tool.handles.ProfileTab,'Position',[0 0 1 1],'Xlim',[0 pos(3)],'Ylim',[0 pos(4)],'Visible','off');
            %Profile type pulldown menu
            names = {'Designer','Flat'};
            tool.handles.Pulldown_profileType  =   uicontrol(tool.handles.ProfileTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Select a profile type');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_profileType');
            set(tool.handles.Pulldown_profileType,'Callback',fun)
            %Profile equation text
            tool.handles.ProfileText = text(7*buff,pos(4)-2*buff,'$$C = C_p \left( 1- \left( \frac{r}{R_{\theta , \phi }} \right)^2 \right)^n$$','Interpreter','latex','Color','w','HorizontalAlignment','left','VerticalAlignment','bottom','Parent',tool.handles.ProfileBackgroundAxes,'FontSize',14);
            %Make designer nodule option panel
            tool.handles.DesignerNodulePanel = uipanel(tool.handles.ProfileTab,'Units','Pixels','Title','Designer Nodule Settings','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            %Peak Contrast edit box
            tool.handles.PeakContrastTextEdit = uicontrol(tool.handles.ProfileTab,'Style','edit','String',num2str(tool.lesion.contrast),'Units','Pixels','TooltipString','Peak contrast','HorizontalAlignment','left','UserData',tool.lesion.contrast);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'PeakContrastTextEdit');
            set(tool.handles.PeakContrastTextEdit,'Callback',fun)
            tool.handles.PeakContrastText = uicontrol(tool.handles.ProfileTab,'Style','text','String','Contrast','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %n edit box
            tool.handles.nTextEdit = uicontrol(tool.handles.DesignerNodulePanel,'Style','edit','String',num2str(tool.lesion.profile.n),'Units','Pixels','Position',[buff/4 buff/4 buff buff],'TooltipString','n value (i.e., exponential)','HorizontalAlignment','left','UserData',tool.lesion.profile.n);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'nTextEdit');
            set(tool.handles.nTextEdit,'Callback',fun)
            tool.handles.nText = uicontrol(tool.handles.DesignerNodulePanel,'Style','text','String','n','Units','Pixels','Position',[buff/4+buff buff/4 buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Blur model pulldown menu
            names = {'Gaussian','None'};
            tool.handles.Pulldown_blurType  =   uicontrol(tool.handles.ProfileTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Select a blur model');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_blurType');
            set(tool.handles.Pulldown_blurType,'Callback',fun)
            %Make Gaussian blur option panel
            tool.handles.GaussianBlurPanel = uipanel(tool.handles.ProfileTab,'Units','Pixels','Title','Gaussian Blur Settings','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            %Bx edit box
            tool.handles.BxEdit = uicontrol(tool.handles.GaussianBlurPanel,'Style','edit','String',num2str(tool.lesion.blur.PSF(1)),'Units','Pixels','Position',[buff/4 buff/4 buff buff],'TooltipString','x-direction blurring','HorizontalAlignment','left','UserData',tool.lesion.blur.PSF(1));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'BlurTextEdit');
            set(tool.handles.BxEdit,'Callback',fun)
            tool.handles.BxText = uicontrol(tool.handles.GaussianBlurPanel,'Style','text','String','Bx','Units','Pixels','Position',[1.5*buff/4+buff buff/4 buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %By edit box
            tool.handles.ByEdit = uicontrol(tool.handles.GaussianBlurPanel,'Style','edit','String',num2str(tool.lesion.blur.PSF(2)),'Units','Pixels','Position',[1.5*buff/4+2*buff buff/4 buff buff],'TooltipString','y-direction blurring','HorizontalAlignment','left','UserData',tool.lesion.blur.PSF(2));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'BlurTextEdit');
            set(tool.handles.ByEdit,'Callback',fun)
            tool.handles.ByText = uicontrol(tool.handles.GaussianBlurPanel,'Style','text','String','By','Units','Pixels','Position',[2*buff/4+3*buff buff/4 buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Bz edit box
            tool.handles.BzEdit = uicontrol(tool.handles.GaussianBlurPanel,'Style','edit','String',num2str(tool.lesion.blur.PSF(3)),'Units','Pixels','Position',[1.5*buff/4+4*buff buff/4 buff buff],'TooltipString','z-direction blurring','HorizontalAlignment','left','UserData',tool.lesion.blur.PSF(3));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'BlurTextEdit');
            set(tool.handles.BzEdit,'Callback',fun)
            tool.handles.BzText = uicontrol(tool.handles.GaussianBlurPanel,'Style','text','String','Bz','Units','Pixels','Position',[2*buff/4+5*buff buff/4 buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Make profile axes
            tool.handles.ProfileAxes = axes('Parent',tool.handles.ProfileTab,'Position',[1.5*buff/pos(3) 1.5*buff/pos(4) 1-2*(1.5*buff/pos(3)) .5],'Color','none','XColor','w','YColor','w');
            grid on; box on; hold on;
            xlabel('Distance (mm)'); ylabel('Contrast (HU)');
            [x,y] = getAverageProfile(tool.lesion);
            tool.handles.AverageProfile = plot(tool.handles.ProfileAxes,x,y,'o-w');
            set(tool.handles.ProfileAxes,'Ylim',[0 tool.lesion.contrast]);
            %--------------------------------------------------------------
            
            
            %make components of voxelization tab--------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'Voxelize');
            set(tool.handles.VoxelizeTab,'ResizeFcn',fun)
            %x y and z text
            tool.handles.xText = uicontrol(tool.handles.VoxelizeTab,'Style','text','String','x','Units','Pixels','HorizontalAlignment','center','ForegroundColor','w','BackgroundColor','k');
            tool.handles.yText = uicontrol(tool.handles.VoxelizeTab,'Style','text','String','y','Units','Pixels','HorizontalAlignment','center','ForegroundColor','w','BackgroundColor','k');
            tool.handles.zText = uicontrol(tool.handles.VoxelizeTab,'Style','text','String','z','Units','Pixels','HorizontalAlignment','center','ForegroundColor','w','BackgroundColor','k');
            %FOVx edit box
            tool.handles.FOVxEdit = uicontrol(tool.handles.VoxelizeTab,'Style','edit','String',num2str(tool.lesion.FOV(1)),'Units','Pixels','TooltipString','FOV in x direction','HorizontalAlignment','left','UserData',tool.lesion.FOV(1));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'FOVTextEdit');
            set(tool.handles.FOVxEdit,'Callback',fun)
            %FOVy edit box
            tool.handles.FOVyEdit = uicontrol(tool.handles.VoxelizeTab,'Style','edit','String',num2str(tool.lesion.FOV(2)),'Units','Pixels','TooltipString','FOV in y direction','HorizontalAlignment','left','UserData',tool.lesion.FOV(2));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'FOVTextEdit');
            set(tool.handles.FOVyEdit,'Callback',fun)
            %FOVz edit box
            tool.handles.FOVzEdit = uicontrol(tool.handles.VoxelizeTab,'Style','edit','String',num2str(tool.lesion.FOV(3)),'Units','Pixels','TooltipString','FOV in z direction','HorizontalAlignment','left','UserData',tool.lesion.FOV(3));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'FOVTextEdit');
            set(tool.handles.FOVzEdit,'Callback',fun)
            %FOV text label
            tool.handles.FOVText = uicontrol(tool.handles.VoxelizeTab,'Style','text','String','FOV (mm)','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Nx edit box
            tool.handles.NxEdit = uicontrol(tool.handles.VoxelizeTab,'Style','edit','String',num2str(tool.lesion.n(1)),'Units','Pixels','TooltipString','Pre-interpolated # of voxels in x direction','HorizontalAlignment','left','UserData',tool.lesion.n(1));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'NTextEdit');
            set(tool.handles.NxEdit,'Callback',fun)
            %Ny edit box
            tool.handles.NyEdit = uicontrol(tool.handles.VoxelizeTab,'Style','edit','String',num2str(tool.lesion.n(2)),'Units','Pixels','TooltipString','Pre-interpolated # of voxels in y direction','HorizontalAlignment','left','UserData',tool.lesion.n(2));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'NTextEdit');
            set(tool.handles.NyEdit,'Callback',fun)
            %Nz edit box
            tool.handles.NzEdit = uicontrol(tool.handles.VoxelizeTab,'Style','edit','String',num2str(tool.lesion.n(3)),'Units','Pixels','TooltipString','Pre-interpolated # of voxels in z direction','HorizontalAlignment','left','UserData',tool.lesion.n(3));
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'NTextEdit');
            set(tool.handles.NzEdit,'Callback',fun)
            %N text label
            tool.handles.NText = uicontrol(tool.handles.VoxelizeTab,'Style','text','String','N (pixels)','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Scale edit box
            tool.handles.ScaleEdit = uicontrol(tool.handles.VoxelizeTab,'Style','edit','String',num2str(tool.lesion.scale),'Units','Pixels','TooltipString','Upscale Factor','HorizontalAlignment','left','UserData',tool.lesion.scale);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'ScaleEdit');
            set(tool.handles.ScaleEdit,'Callback',fun)
            %Scale text label
            tool.handles.ScaleText = uicontrol(tool.handles.VoxelizeTab,'Style','text','String','Upscale Factor','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Interpolation type pulldown menu
            names = {'linear','nearest','cubic','spline'};
            tool.handles.Pulldown_interpType  =   uicontrol(tool.handles.VoxelizeTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Upsampling interpolation method');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_interpType');
            set(tool.handles.Pulldown_interpType,'Callback',fun)
            %Pixel size text box
            psize=tool.lesion.psize;
            psize_pre = tool.lesion.psize_pre;
            n_up = tool.lesion.n_up;
            str = {['Voxel size = (' num2str(psize(1)) ',' num2str(psize(2)) ',' num2str(psize(3)) ') mm'], ...
                ['Pre-upsampled voxel size = (' num2str(psize_pre(1)) ',' num2str(psize_pre(2)) ',' num2str(psize_pre(3)) ') mm' ],...
                ['Upsampled # of voxels = (' num2str(n_up(1)) ',' num2str(n_up(2)) ',' num2str(n_up(3)) ')'] };
            tool.handles.psizeText = uicontrol(tool.handles.VoxelizeTab,'Style','text','String',str,'Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %--------------------------------------------------------------
            
            
            %Make components of the Random generation tab------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'Random');
            set(tool.handles.RandomTab,'ResizeFcn',fun)
            %Lesion type pulldown menu
            names = {'Custom','Liver Lesion','Lung Nodule','Renal Stone'};
            tool.handles.Pulldown_randomType  =   uicontrol(tool.handles.RandomTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Random generation template');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_randomType');
            set(tool.handles.Pulldown_randomType,'Callback',fun)
            %Random generation table
            rowNames = {'Mean Radius','Radius COV','Peak Contrast','n','Edge Blur'};
            columnNames = {'Mean','Min','Max','STD'};
            data = [5 3 7 1;10 0 20 5;100 50 150 25;1 0 2 .5;1 0 2 .5];
            tool.handles.RandomTable = uitable(tool.handles.RandomTab,'Data',data,'Units','Pixels','RowName',rowNames,'ColumnName',columnNames,'ColumnEditable',true);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'RandomTableEdit');
            set(tool.handles.RandomTable,'CellEditCallback',fun)
            %Random generate button
            tool.handles.randomButton = uicontrol(tool.handles.RandomTab,'Style','pushbutton','String','Generate Random Lesion','Units','Pixels','TooltipString','Generate Random Lesion','HorizontalAlignment','left','Enable','On');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'randomButton');
            set(tool.handles.randomButton,'Callback',fun)
            %--------------------------------------------------------------
            
            
            %Make components of the texture tab----------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'Texture');
            set(tool.handles.TextureTab,'ResizeFcn',fun)
            %Texture type pulldown menu
            names = {'None','Clustered Lumpy Background'};
            tool.handles.Pulldown_textureType  =   uicontrol(tool.handles.TextureTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Texture type');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_textureType');
            set(tool.handles.Pulldown_textureType,'Callback',fun)
            %Make panel for CLB parameters
            tool.handles.ClusteredLumpyPanel = uipanel(tool.handles.TextureTab,'Units','Pixels','Title','Clustered Lumpy Background Settings','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14,'Visible','off');
            %K edit box
            tool.handles.KTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(.1),'Units','Pixels','Position',[buff/4 buff/4 buff buff],'TooltipString','Number of clusters per mm^3','HorizontalAlignment','left','UserData',.1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit');
            set(tool.handles.KTextEdit,'Callback',fun)
            lp = get(tool.handles.KTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.KText = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','text','String','K','Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k','Visible','on');
            lp = get(tool.handles.KText,'Position'); lp=lp(1)+lp(3);
            %N edit box
            tool.handles.NTTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(1),'Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'TooltipString','Number of blobs per cluster','HorizontalAlignment','left','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit_N');
            set(tool.handles.NTTextEdit,'Callback',fun)
            lp = get(tool.handles.NTTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.NTText = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','text','String','N','Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            lp = get(tool.handles.NTText,'Position'); lp=lp(1)+lp(3);
            %Sigma edit box
            tool.handles.SigmaTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(1),'Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'TooltipString','STD of blob position distribution about clusters','HorizontalAlignment','left','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit');
            set(tool.handles.SigmaTextEdit,'Callback',fun)
            lp = get(tool.handles.SigmaTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.SigmaText = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','text','String','Sigma','Units','Pixels','Position',[lp+buff/4 buff/4 1.5*buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            lp = get(tool.handles.SigmaText,'Position'); lp=lp(1)+lp(3);
            %Alpha edit box
            tool.handles.AlphaTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(1),'Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'TooltipString','Alpha value of exponential','HorizontalAlignment','left','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit');
            set(tool.handles.AlphaTextEdit,'Callback',fun)
            lp = get(tool.handles.AlphaTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.AlphaText = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','text','String','Alpha','Units','Pixels','Position',[lp+buff/4 buff/4 1.5*buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            lp = get(tool.handles.AlphaText,'Position'); lp=lp(1)+lp(3);
            %Beta edit box
            tool.handles.BetaTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(1),'Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'TooltipString','Beta value of exponential','HorizontalAlignment','left','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit');
            set(tool.handles.BetaTextEdit,'Callback',fun)
            lp = get(tool.handles.BetaTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.BetaText = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','text','String','Beta','Units','Pixels','Position',[lp+buff/4 buff/4 1.5*buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            lp = get(tool.handles.BetaText,'Position'); lp=lp(1)+lp(3);
            %L edit boxes
            tool.handles.LxTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(1),'Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'TooltipString','Lx','HorizontalAlignment','left','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit');
            set(tool.handles.LxTextEdit,'Callback',fun)
            lp = get(tool.handles.LxTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.LyTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(1),'Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'TooltipString','Ly','HorizontalAlignment','left','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit');
            set(tool.handles.LyTextEdit,'Callback',fun)
            lp = get(tool.handles.LyTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.LzTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(1),'Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'TooltipString','Lz','HorizontalAlignment','left','UserData',1);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit');
            set(tool.handles.LzTextEdit,'Callback',fun)
            lp = get(tool.handles.LzTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.LText = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','text','String','L','Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            lp = get(tool.handles.LText,'Position'); lp=lp(1)+lp(3);
            %Contrast edit box
            tool.handles.TextureContrastTextEdit = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','edit','String',num2str(.5),'Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'TooltipString','Magnitude of texture fluctuations (relative to total lesion contrast)','HorizontalAlignment','left','UserData',.5);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'CLBTextEdit_C');
            set(tool.handles.TextureContrastTextEdit,'Callback',fun)
            lp = get(tool.handles.TextureContrastTextEdit,'Position'); lp=lp(1)+lp(3);
            tool.handles.TextureContrastText = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','text','String','C','Units','Pixels','Position',[lp+buff/4 buff/4 buff buff],'HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            lp = get(tool.handles.BetaText,'Position'); lp=lp(1)+lp(3);
            %Refresh texture button
            tool.handles.RefreshTextureButton = uicontrol(tool.handles.ClusteredLumpyPanel,'Style','pushbutton','String','Refresh Texture','Units','Pixels','Position',[buff/4 2*buff 5*buff buff],'TooltipString','Regenerate lesion texture','HorizontalAlignment','left','Enable','On');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'RefreshTextureButton');
            set(tool.handles.RefreshTextureButton,'Callback',fun)
            %--------------------------------------------------------------
            
            
            %Make components of the library tab----------------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'Library');
            set(tool.handles.LibraryTab,'ResizeFcn',fun)
            %list boxes
            tool.handles.type_List = uicontrol(tool.handles.LibraryTab,'Style','listbox','String','','Units','Pixels','TooltipString','Filter by lesion type','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            tool.handles.type_List_text = uicontrol(tool.handles.LibraryTab,'Style','text','String','Type','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'LibraryList');
            set(tool.handles.type_List,'Callback',fun)
            tool.handles.tag_List = uicontrol(tool.handles.LibraryTab,'Style','listbox','String','','Units','Pixels','TooltipString','Filter by tag','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            tool.handles.tag_List_text = uicontrol(tool.handles.LibraryTab,'Style','text','String','Tag','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'LibraryList');
            set(tool.handles.tag_List,'Callback',fun)
            tool.handles.name_List = uicontrol(tool.handles.LibraryTab,'Style','listbox','String','','Units','Pixels','TooltipString','Select a lesion','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            tool.handles.name_List_text = uicontrol(tool.handles.LibraryTab,'Style','text','String','Name','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'LibraryList');
            set(tool.handles.name_List,'Callback',fun)
            %Load button
            tool.handles.LoadLesionButton = uicontrol(tool.handles.LibraryTab,'Style','pushbutton','String','Load','Units','Pixels','TooltipString','Load selected lesion from library','HorizontalAlignment','center','Enable','On');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'LoadLesionButton');
            set(tool.handles.LoadLesionButton,'Callback',fun)
            %Save to library button
            tool.handles.SaveLesionButton = uicontrol(tool.handles.LibraryTab,'Style','pushbutton','String','Save to library','Units','Pixels','TooltipString','Save current lesion to library','HorizontalAlignment','center','Enable','On');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'SaveLesionButton');
            set(tool.handles.SaveLesionButton,'Callback',fun)
            %Type text edit
            tool.handles.LesionTypeEdit = uicontrol(tool.handles.LibraryTab,'Style','edit','String','type','Units','Pixels','TooltipString','Lesion type','HorizontalAlignment','left','Enable','On');
            %Tag text edit
            tool.handles.LesionTagEdit = uicontrol(tool.handles.LibraryTab,'Style','edit','String','tag','Units','Pixels','TooltipString','Lesion tag','HorizontalAlignment','left','Enable','On');
            %Name text edit
            tool.handles.LesionNameEdit = uicontrol(tool.handles.LibraryTab,'Style','edit','String','name','Units','Pixels','TooltipString','Lesion name','HorizontalAlignment','left','Enable','On');
            %Lesion info text box
            tool.handles.LesionInfo_text = uicontrol(tool.handles.LibraryTab,'Style','text','String','Lesion info','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %--------------------------------------------------------------
            
            
            %Make components of the CT tab---------------------------------
            %imtool3D object
            tool.handles.imtoolCT_Panel = uipanel(tool.handles.CTTab,'Position',[.5 0 .5 1],'Title','CT viewer','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            tool.handles.imtool_CT = imtool3D(zeros(25),[0 0 1 1],tool.handles.imtoolCT_Panel,[],tool.handles.imtool_Lesion);
            h=getHandles(tool.handles.imtool_CT);
            set(h.Panels.Large,'Visible','off')
            set(h.Tools.Crop,'Visible','off')
            set(h.Tools.Help,'Visible','off')
            addlistener(tool.handles.imtool_CT,'newMousePos',@tool.handlePropEvents);
            tool.handles.CTTabGroup = uitabgroup(tool.handles.CTTab,'Units','Normalized','Position',[0 .5 .5 .5],'TabLocation','top');
            tool.handles.CTInfoTab = uitab(tool.handles.CTTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','CT Image Info');
            tool.handles.LesionInsertionTab = uitab(tool.handles.CTTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Lesion Insertion');
            tool.handles.LesionFittingTab = uitab(tool.handles.CTTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Lesion Fitting');
            %Make panel for the MIP viewer
            tool.handles.scoutImageViewer_Panel = uipanel(tool.handles.CTTab,'Position',[0 0 .5 .5],'Title','MIP viewer','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            tool.handles.MIPviewer = scoutImageViewer(tool.handles.scoutImageViewer_Panel,[0 0 1 1],tool.handles.imtool_CT,tool);
            %make panel for physics tab
            tool.handles.PhysicsTab = uitab(tool.handles.CTTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Imaging Physics');
            %make panel for the noise addition tab
            tool.handles.NoiseAdditionTab = uitab(tool.handles.CTTabGroup,'BackgroundColor','k','ForegroundColor','k','Title','Noise Addition');
            %--------------------------------------------------------------
            
            
            %Make components of the CT image info tab----------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'CTImageInfo');
            set(tool.handles.CTInfoTab,'ResizeFcn',fun)
            %Image type pulldown menu
            names = {'DICOMs','.ipv'};
            tool.handles.Pulldown_imageType  =   uicontrol(tool.handles.CTInfoTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Load image type');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_imageType');
            set(tool.handles.Pulldown_imageType,'Callback',fun)
            %Load button
            tool.handles.LoadCTDataButton = uicontrol(tool.handles.CTInfoTab,'Style','pushbutton','String','Load Images','Units','Pixel','TooltipString','Load CT data','HorizontalAlignment','center','Enable','On');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'LoadCTDataButton');
            set(tool.handles.LoadCTDataButton,'Callback',fun)
            %Export options pulldown
            names = {'W/ CT Data','W/O CT Data','Alpha Maps'};
            tool.handles.Pulldown_exportType  =   uicontrol(tool.handles.CTInfoTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Export options','Visible','off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_exportType');
            set(tool.handles.Pulldown_exportType,'Callback',fun)
            %Export button
            tool.handles.ExportCTDataButton = uicontrol(tool.handles.CTInfoTab,'Style','pushbutton','String','Export','Units','Pixel','TooltipString','Export CT data','HorizontalAlignment','center','Visible','off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'ExportCTDataButton');
            set(tool.handles.ExportCTDataButton,'Callback',fun)
            %Make the image info text box
            tool.handles.ImageInfo_text = uicontrol(tool.handles.CTInfoTab,'Style','text','String',{'Image Info:'},'Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Image header display button
            tool.handles.ImageInfoButton = uicontrol(tool.handles.CTInfoTab,'Style','pushbutton','String','Show Header','Units','Pixel','TooltipString','Show the header info of the current image','HorizontalAlignment','center','Visible','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'ImageInfoButton');
            set(tool.handles.ImageInfoButton,'Callback',fun)
            %Add listeners
            addlistener(tool,'CTimageChanged',@tool.handlePropEvents);
            addlistener(tool,'readingCTData',@tool.handlePropEvents);
            addlistener(tool,'readCTData',@tool.handlePropEvents);
            addlistener(tool,'exportingImages',@tool.handlePropEvents);
            addlistener(tool,'exportedImages',@tool.handlePropEvents);
            %Set the CTInfo property
            CTInfo.format = 'none';
            CTInfo.fname = '';
            CTInfo.S = [];
            CTInfo.psize = [];
            CTInfo.pspacing = [];
            CTInfo.FOV = [];
            CTInfo.x=[];
            CTInfo.y=[];
            CTInfo.z=[];
            tool.CTInfo = CTInfo;
            %--------------------------------------------------------------
            
            
            %Make components of the lesion insertion tab-------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'LesionInsertion');
            set(tool.handles.LesionInsertionTab,'ResizeFcn',fun)
            %make the lesion insertion-cross hairs
            tool.handles.InsertionPoint = insertionPointObject(tool);
            %make the noise ROI cross hairs
            tool.handles.NoiseInsertionPoint = insertionPointObject(tool);
            tool.handles.NoiseInsertionPoint.color='g';
            tool.handles.NoiseInsertionPoint.visible='off';
            %Insertion method pulldown
            names = {'Superposition','Alpha Blending'};
            tool.handles.Pulldown_insertionType  =   uicontrol(tool.handles.LesionInsertionTab,'Style','popupmenu','String',names,'Units','Pixels','TooltipString','Lesion insertion method');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Pulldown_insertionType');
            set(tool.handles.Pulldown_insertionType,'Callback',fun)
            %Check box for partial voluming
            tool.handles.PartialCheckBox = uicontrol(tool.handles.LesionInsertionTab,'Style','Checkbox','String','Partial Volume?','Units','Pixels','TooltipString','Allow for partial voluming when inserting?','HorizontalAlignment','left','Value',false,'BackgroundColor','k','ForegroundColor','w');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Check_PartialVolume');
            set(tool.handles.PartialCheckBox,'Callback',fun)
            %Text edit for parital volume factor
            tool.handles.ParitalTextEdit = uicontrol(tool.handles.LesionInsertionTab,'Style','edit','String',num2str(4),'Units','Pixels','TooltipString','Upsample factor for partial voluming','HorizontalAlignment','left','UserData',4,'Visible','off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'PartialTextEdit');
            set(tool.handles.ParitalTextEdit,'Callback',fun)
            %Check box for MTF blurring
            tool.handles.MTFCheckBox = uicontrol(tool.handles.LesionInsertionTab,'Style','Checkbox','String','MTF Blur?','Units','Pixels','TooltipString','Blur the lesion with MTF before inserting?','HorizontalAlignment','left','Enable','On','BackgroundColor','k','ForegroundColor','w');
            %Text showing insertion location
            tool.handles.InsertionLocation_text = uicontrol(tool.handles.LesionInsertionTab,'Style','text','String','','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %add a listener for the change in insertion location
            addlistener(tool.handles.InsertionPoint,'newInsertionPosition',@tool.handlePropEvents);
            addlistener(tool.handles.NoiseInsertionPoint,'newInsertionPosition',@tool.handlePropEvents);
            %This updates the string showin the insertion location
            tool.handles.InsertionPoint.position=tool.handles.InsertionPoint.position;
            %Insertion point visible check box
            tool.handles.InsertionPointVisibleCheckBox = uicontrol(tool.handles.LesionInsertionTab,'Style','Checkbox','String','Display Insertion Point?','Units','Pixels','TooltipString','Show the insertion location point on the image?','HorizontalAlignment','left','Value',true,'BackgroundColor','k','ForegroundColor','w');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'Check_InsertionVisible');
            set(tool.handles.InsertionPointVisibleCheckBox,'Callback',fun)
            %Inserted lesion list box
            tool.handles.inserted_List = uicontrol(tool.handles.LesionInsertionTab,'Style','listbox','String','','Units','Pixels','TooltipString','List of inserted lesions','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k','Value',0);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'InsertedList');
            set(tool.handles.inserted_List,'Callback',fun)
            %Remove lesion button
            tool.handles.RemoveInsertedLesionButton = uicontrol(tool.handles.LesionInsertionTab,'Style','pushbutton','String','-','Units','Pixels','TooltipString','Remove selected lesion','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'RemoveInsertedLesionButton');
            set(tool.handles.RemoveInsertedLesionButton,'Callback',fun)
            %Export inserted lesions button
            tool.handles.ExportInsertedLesionsButton = uicontrol(tool.handles.LesionInsertionTab,'Style','pushbutton','String','->','Units','Pixels','TooltipString','Export list of inserted lesions','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'ExportInsertedLesionsButton');
            set(tool.handles.ExportInsertedLesionsButton,'Callback',fun)
            %Inserted lesion text box
            tool.handles.InsertedLesion_text = uicontrol(tool.handles.LesionInsertionTab,'Style','text','String','Inserted lesion info:','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Insert lesion button
            tool.handles.InsertLesionButton = uicontrol(tool.handles.LesionInsertionTab,'Style','pushbutton','String','Insert Lesion','Units','Pixels','TooltipString','Insert lesion into CT data','HorizontalAlignment','left','Enable','off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'InsertLesionButton');
            set(tool.handles.InsertLesionButton,'Callback',fun)
            %Initialize the list of inserted lesions
            tool.insertedLesion=struct('lesion',{},'I',{},'Position',{},'Position_mm',{},'Lims',{},'Method',{},'MTFblur',{},'Partial',{},'Alpha',{});
            %Add the listener for the list
            addlistener(tool.handles.inserted_List,'Value','PostSet',@tool.handlePropEvents);
            addlistener(tool,'insertingLesion',@tool.handlePropEvents);
            addlistener(tool,'doneInsertingLesion',@tool.handlePropEvents);
            %--------------------------------------------------------------
            
            
            %Make components of the lesion fitting tab---------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'LesionFit');
            set(tool.handles.LesionFittingTab,'ResizeFcn',fun)
            %Fit button
            tool.handles.FitLesionButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Fit','Units','Pixels','TooltipString','Fit model to segmented lesion','HorizontalAlignment','left','Enable','off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'FitLesionButton');
            set(tool.handles.FitLesionButton,'Callback',fun)
            %Buffer Edit
            tool.handles.FitBufferEdit = uicontrol(tool.handles.LesionFittingTab,'Style','edit','String',num2str(7),'Units','Pixels','TooltipString','Number of voxels to buffer the ROI when fitting','HorizontalAlignment','left','UserData',7);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'FitBufferEdit');
            set(tool.handles.FitBufferEdit,'Callback',fun)
            tool.handles.FitBufferText = uicontrol(tool.handles.LesionFittingTab,'Style','text','String','Buffer','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %MTF and NPS check boxes
            tool.handles.FitMTFCheck = uicontrol(tool.handles.LesionFittingTab,'Style','Checkbox','String','MTF?','Units','Pixels','TooltipString','Account for system MTF during fit?','HorizontalAlignment','left','Value',false,'BackgroundColor','k','ForegroundColor','w');
            tool.handles.FitNPSCheck = uicontrol(tool.handles.LesionFittingTab,'Style','Checkbox','String','NPS?','Units','Pixels','TooltipString','Account for system NPS during fit?','HorizontalAlignment','left','Value',false,'BackgroundColor','k','ForegroundColor','w');
            %Load Mask Button
            tool.handles.LoadMaskButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Load Mask (.mat)','Units','Pixels','TooltipString','Load segmentation mask (.mat)','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'LoadMaskButton');
            set(tool.handles.LoadMaskButton,'Callback',fun)
            %Load Mask Button
            tool.handles.LoadDICOMMaskButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Load Mask (DICOM)','Units','Pixels','TooltipString','Load segmentation mask (from DICOM series)','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'LoadDICOMMaskButton');
            set(tool.handles.LoadDICOMMaskButton,'Callback',fun)
            %Export Mask Button
            tool.handles.ExportMaskButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Export Mask','Units','Pixels','TooltipString','Export segmentation mask','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'ExportMaskButton');
            set(tool.handles.ExportMaskButton,'Callback',fun)
            %Clear Mask Button
            tool.handles.ClearMaskButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Clear Mask','Units','Pixels','TooltipString','Clear segmentation mask','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'ClearMaskButton');
            set(tool.handles.ClearMaskButton,'Callback',fun)
            %Auto Segment Button
            tool.handles.AutoSegmentButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Auto-Segment','Units','Pixels','TooltipString','Perform Automatic Segmentation','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'AutoSegmentButton');
            set(tool.handles.AutoSegmentButton ,'Callback',fun)
            %Binary Dilate Button
            tool.handles.BinaryDilateButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Binary Dilate','Units','Pixels','TooltipString','Binary Dilate Segmentation','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'BinaryDilateButton');
            set(tool.handles.BinaryDilateButton ,'Callback',fun)
            %Binary Erode Button
            tool.handles.BinaryErodeButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Binary Erode','Units','Pixels','TooltipString','Binary Erode Segmentation','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'BinaryErodeButton');
            set(tool.handles.BinaryErodeButton ,'Callback',fun)
            %Fill Holes Button
            tool.handles.FillHolesButton = uicontrol(tool.handles.LesionFittingTab,'Style','pushbutton','String','Fill Holes','Units','Pixels','TooltipString','Fill Holes in Segmentation','HorizontalAlignment','left');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'FillHolesButton');
            set(tool.handles.FillHolesButton ,'Callback',fun)
            %Mask info text
            tool.handles.MaskInfo_text = uicontrol(tool.handles.LesionFittingTab,'Style','text','String','Mask info:','Units','Pixels','HorizontalAlignment','left','ForegroundColor','w','BackgroundColor','k');
            %Lesion Measure panel
            tool.handles.LesionMeasurePanel = lesionTool_quantitativeFeatures_controller(tool.handles.LesionFittingTab,[.5 .5 .5 .5],tool);
            %Make the panel invisible if needed
            if ~isOptionalRepoAvailable(tool,'QuantitativeTools')
                tool.handles.LesionMeasurePanel.Visible=false;
            end
            
            addlistener(tool.handles.imtool_CT,'maskChanged',@tool.handlePropEvents);
            addlistener(tool,'fittingLesion',@tool.handlePropEvents);
            addlistener(tool,'doneFittingLesion',@tool.handlePropEvents);
            %--------------------------------------------------------------
            
            %Make components of the imaging physics tab--------------------
            tool.NPS = NPSobject;
            tool.MTF_xy= MTFobject;
            tool.MTF_z = MTFobject; tool.MTF_z.m=.4;
            setNoise(tool.NPS,15);
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'Physics');
            set(tool.handles.PhysicsTab,'ResizeFcn',fun)
            %Make NPS panel
            tool.handles.NPSPanel = uipanel(tool.handles.PhysicsTab,'Units','Pixels','Title','NPS Settings','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            %Noise edit box
            tool.handles.NPS_noise_Edit = uicontrol(tool.handles.NPSPanel,'Style','edit','String',num2str(tool.NPS.noise),'Units','Pixels','Position',[0 buff/2 buff buff],'TooltipString','Noise STD (HU)','HorizontalAlignment','left','UserData',tool.NPS.noise);
            tool.handles.NPS_noise_Text = uicontrol(tool.handles.NPSPanel,'Style','text','String','N','Units','Pixels','HorizontalAlignment','left','Position',[0 buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'NPS_noise_Edit');
            set(tool.handles.NPS_noise_Edit,'Callback',fun)
            %b edit box
            tool.handles.NPS_b_Edit = uicontrol(tool.handles.NPSPanel,'Style','edit','String',num2str(tool.NPS.b),'Units','Pixels','TooltipString','b','Position',[buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.NPS.b);
            tool.handles.NPS_b_Text = uicontrol(tool.handles.NPSPanel,'Style','text','String','b','Units','Pixels','HorizontalAlignment','left','Position',[buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'NPS_b_Edit');
            set(tool.handles.NPS_b_Edit,'Callback',fun)
            %c edit box
            tool.handles.NPS_c_Edit = uicontrol(tool.handles.NPSPanel,'Style','edit','String',num2str(tool.NPS.c),'Units','Pixels','TooltipString','c','Position',[2*buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.NPS.c);
            tool.handles.NPS_c_Text = uicontrol(tool.handles.NPSPanel,'Style','text','String','c','Units','Pixels','HorizontalAlignment','left','Position',[2*buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'NPS_c_Edit');
            set(tool.handles.NPS_c_Edit,'Callback',fun)
            addlistener(tool.NPS,'NPSPropertyChanged',@tool.handlePropEvents);
            %d edit box
            tool.handles.NPS_d_Edit = uicontrol(tool.handles.NPSPanel,'Style','edit','String',num2str(tool.NPS.d),'Units','Pixels','TooltipString','d','Position',[3*buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.NPS.d);
            tool.handles.NPS_d_Text = uicontrol(tool.handles.NPSPanel,'Style','text','String','d','Units','Pixels','HorizontalAlignment','left','Position',[3*buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'NPS_d_Edit');
            set(tool.handles.NPS_d_Edit,'Callback',fun)
            %make the axis for the in-plane NPS plot
            tool.handles.NPS_axis = axes('Parent',tool.handles.PhysicsTab,'Units','Pixels');
            [Fr, npsxy] = getNPSprofile(tool.NPS);
            tool.handles.NPS_fxy=plot(Fr,npsxy,'-w','Parent',tool.handles.NPS_axis);
            set(tool.handles.NPS_axis,'Color','k','XColor','w','YColor','w','YTick',[])
            title(tool.handles.NPS_axis,'In-Plane NPS','Color','w')
            xlabel('f_r','Color','w')
            %make the axis for the z-direction NPS image
            tool.handles.NPS_z_axis = axes('Parent',tool.handles.PhysicsTab,'Units','Pixels');
            [fx,fz,npsz]=getZnps(tool.NPS);
            tool.handles.NPS_z_image=imshow(mat2gray(npsz),'Parent',tool.handles.NPS_z_axis,'XData',fx,'YData',fz);
            set(tool.handles.NPS_z_axis,'Color','k','XColor','w','YColor','w')
            axis on
            title(tool.handles.NPS_z_axis,'Z-direction NPS','Color','w');
            xlabel('f_r','Color','w'); ylabel('f_z','Color','w')
            %In-plane MTF panel
            tool.handles.MTFxyPanel = uipanel(tool.handles.PhysicsTab,'Units','Pixels','Title','In-Plane MTF Settings','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            %m edit box (for in-plane MTF)
            tool.handles.MTFxy_m_Edit = uicontrol(tool.handles.MTFxyPanel,'Style','edit','String',num2str(tool.MTF_xy.m),'Units','Pixels','TooltipString','m','Position',[0 buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.MTF_xy.m);
            tool.handles.MTFxy_m_Text = uicontrol(tool.handles.MTFxyPanel,'Style','text','String','m','Units','Pixels','HorizontalAlignment','left','Position',[0 buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFxy_m_Edit');
            set(tool.handles.MTFxy_m_Edit,'Callback',fun)
            %c edit box (for in-plane MTF)
            tool.handles.MTFxy_c_Edit = uicontrol(tool.handles.MTFxyPanel,'Style','edit','String',num2str(tool.MTF_xy.c),'Units','Pixels','TooltipString','c','Position',[buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.MTF_xy.c);
            tool.handles.MTFxy_c_Text = uicontrol(tool.handles.MTFxyPanel,'Style','text','String','c','Units','Pixels','HorizontalAlignment','left','Position',[buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFxy_c_Edit');
            set(tool.handles.MTFxy_c_Edit,'Callback',fun)
            %h edit box (for in-plane MTF)
            tool.handles.MTFxy_h_Edit = uicontrol(tool.handles.MTFxyPanel,'Style','edit','String',num2str(tool.MTF_xy.h),'Units','Pixels','TooltipString','h','Position',[2*buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.MTF_xy.h);
            tool.handles.MTFxy_h_Text = uicontrol(tool.handles.MTFxyPanel,'Style','text','String','h','Units','Pixels','HorizontalAlignment','left','Position',[2*buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFxy_h_Edit');
            set(tool.handles.MTFxy_h_Edit,'Callback',fun)
            %s edit box (for in-plane MTF)
            tool.handles.MTFxy_s_Edit = uicontrol(tool.handles.MTFxyPanel,'Style','edit','String',num2str(tool.MTF_xy.s),'Units','Pixels','TooltipString','s','Position',[3*buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.MTF_xy.s);
            tool.handles.MTFxy_s_Text = uicontrol(tool.handles.MTFxyPanel,'Style','text','String','s','Units','Pixels','HorizontalAlignment','left','Position',[3*buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFxy_s_Edit');
            set(tool.handles.MTFxy_s_Edit,'Callback',fun)
            %EFF checkbox (for in-plane MTF)
            tool.handles.MTFxy_EFF_Check = uicontrol(tool.handles.MTFxyPanel,'Style','checkbox','String','EFF?','Units','Pixels','TooltipString','Use edge-enhancing MTF?','Position',[4*buff buff/2 2*buff buff],'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFxy_EFF_Check');
            set(tool.handles.MTFxy_EFF_Check,'Callback',fun)
            %z MTF panel
            tool.handles.MTFzPanel = uipanel(tool.handles.PhysicsTab,'Units','Pixels','Title','Z MTF Settings','BackgroundColor','k','ForegroundColor','w','HighlightColor','k','TitlePosition','lefttop','FontSize',14);
            %m edit box (for z MTF)
            tool.handles.MTFz_m_Edit = uicontrol(tool.handles.MTFzPanel,'Style','edit','String',num2str(tool.MTF_z.m),'Units','Pixels','TooltipString','m','Position',[0 buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.MTF_z.m);
            tool.handles.MTFz_m_Text = uicontrol(tool.handles.MTFzPanel,'Style','text','String','m','Units','Pixels','HorizontalAlignment','left','Position',[0 buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFz_m_Edit');
            set(tool.handles.MTFz_m_Edit,'Callback',fun)
            %c edit box (for z MTF)
            tool.handles.MTFz_c_Edit = uicontrol(tool.handles.MTFzPanel,'Style','edit','String',num2str(tool.MTF_z.c),'Units','Pixels','TooltipString','c','Position',[buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.MTF_z.c);
            tool.handles.MTFz_c_Text = uicontrol(tool.handles.MTFzPanel,'Style','text','String','c','Units','Pixels','HorizontalAlignment','left','Position',[buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFz_c_Edit');
            set(tool.handles.MTFz_c_Edit,'Callback',fun)
            %h edit box (for z MTF)
            tool.handles.MTFz_h_Edit = uicontrol(tool.handles.MTFzPanel,'Style','edit','String',num2str(tool.MTF_z.h),'Units','Pixels','TooltipString','h','Position',[2*buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.MTF_z.h);
            tool.handles.MTFz_h_Text = uicontrol(tool.handles.MTFzPanel,'Style','text','String','h','Units','Pixels','HorizontalAlignment','left','Position',[2*buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFz_h_Edit');
            set(tool.handles.MTFz_h_Edit,'Callback',fun)
            %s edit box (for z MTF)
            tool.handles.MTFz_s_Edit = uicontrol(tool.handles.MTFzPanel,'Style','edit','String',num2str(tool.MTF_z.s),'Units','Pixels','TooltipString','s','Position',[3*buff buff/2 buff buff],'HorizontalAlignment','left','UserData',tool.MTF_z.s);
            tool.handles.MTFz_s_Text = uicontrol(tool.handles.MTFzPanel,'Style','text','String','s','Units','Pixels','HorizontalAlignment','left','Position',[3*buff buff+buff/2 buff buff/2],'ForegroundColor','w','BackgroundColor','k');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFz_s_Edit');
            set(tool.handles.MTFz_s_Edit,'Callback',fun)
            %EFF checkbox (for z MTF)
            tool.handles.MTFz_EFF_Check = uicontrol(tool.handles.MTFzPanel,'Style','checkbox','String','EFF?','Units','Pixels','TooltipString','Use edge-enhancing MTF?','Position',[4*buff buff/2 2*buff buff],'HorizontalAlignment','left','BackgroundColor','k','ForegroundColor','w');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MTFz_EFF_Check');
            set(tool.handles.MTFz_EFF_Check,'Callback',fun)
            %make the axis for the MTF plots
            tool.handles.MTF_axis = axes('Parent',tool.handles.PhysicsTab,'Units','Pixels');
            tool.handles.MTF_xy=plot(tool.MTF_xy.f,tool.MTF_xy.mtf,'-w','Parent',tool.handles.MTF_axis); hold on;
            tool.handles.MTF_z=plot(tool.MTF_z.f,tool.MTF_z.mtf,'--w','Parent',tool.handles.MTF_axis);
            set(tool.handles.MTF_axis,'Color','k','XColor','w','YColor','w','YTick',0:.1:1)
            title(tool.handles.MTF_axis,'MTF','Color','w')
            xlabel('f_r','Color','w')
            leg=legend({'In-Plane','Z'},'Location','NorthEast');
            legend(tool.handles.MTF_axis,'boxoff')
            set(leg,'TextColor','w')
            
            addlistener(tool.NPS,'NPSPropertyChanged',@tool.handlePropEvents);
            addlistener(tool.MTF_xy,'MTFPropertyChanged',@tool.handlePropEvents);
            addlistener(tool.MTF_z,'MTFPropertyChanged',@tool.handlePropEvents);
            %--------------------------------------------------------------
            
            %Make components of the noise addition tab---------------------
            fun = @(hObject,evnt) panelResizeFunction(hObject,evnt,tool,buff,'Noise');
            set(tool.handles.NoiseAdditionTab,'ResizeFcn',fun)
            %Measure Noise button
            tool.handles.MeasureCTNoiseButton = uicontrol(tool.handles.NoiseAdditionTab,'Style','pushbutton','String','Measure Noise Map','Units','Pixel','TooltipString','Create Noise Map From CT Data','HorizontalAlignment','center','Enable','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'MeasureCTNoiseButton');
            set(tool.handles.MeasureCTNoiseButton,'Callback',fun)
            %Generate Noise Button
            tool.handles.GenerateCTNoiseButton = uicontrol(tool.handles.NoiseAdditionTab,'Style','pushbutton','String','Generate Noise','Units','Pixel','TooltipString','Generate Noise','HorizontalAlignment','center','Enable','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'GenerateCTNoiseButton');
            set(tool.handles.GenerateCTNoiseButton,'Callback',fun)
            %Dose reduction slider
            tool.handles.DoseReductionSlider = uicontrol(tool.handles.NoiseAdditionTab,'Style','Slider','Units','Pixels','TooltipString','Simulated dose reduction level','Enable','off');
            set(tool.handles.DoseReductionSlider,'min',0,'max',.95,'value',0,'SliderStep',[.01/.95 .01/.95])
            tool.handles.DoseReductionSliderLabel = uicontrol(tool.handles.NoiseAdditionTab,'Style','Text','Units','Pixels','String','Select the desired dose reduction','BackgroundColor','k','ForegroundColor','w');
            tool.handles.DoseReductionText = uicontrol(tool.handles.NoiseAdditionTab,'Style','Text','Units','Pixels','String','0%','BackgroundColor','k','ForegroundColor','w');
            %Noise beta edit box
            tool.handles.NoiseAdditionBeta = uicontrol(tool.handles.NoiseAdditionTab,'Style','edit','String','0.5','Units','Pixels','TooltipString','Beta defines how noise changes with dose (0.5 -> sqrt relationship)','HorizontalAlignment','left','UserData',0.5);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'GreaterThanZeroTextEdit');
            set(tool.handles.NoiseAdditionBeta,'Callback',fun)
            tool.handles.NoiseAdditionBetaLabel = uicontrol(tool.handles.NoiseAdditionTab,'Style','Text','Units','Pixels','String',' Beta','BackgroundColor','k','ForegroundColor','w','HorizontalAlignment','left');
            %Create noise vs dose reduction plot
            Pn = @(x) 100*((1./(1-x)).^.5-1);
            x = linspace(0,.95); y = Pn(x);
            tool.handles.noiseBetaAxis = axes('Parent',tool.handles.NoiseAdditionTab,'Units','Pixels','XColor','w','YColor','w','Color','k','XTick',[0 .95],'XTickLabel',{'0%','95%'},'YTick',[Pn(0) round(Pn(.95))],'YTickLabel',{'0%',[sprintf('%d',round(Pn(.95))) '%']});
            xlabel('Dose Reduction','Color','w'); ylabel('Noise Increase'); hold on;
            tool.handles.noiseBetaFixedLine = plot(x,y,'--w','Parent',tool.handles.noiseBetaAxis);
            tool.handles.noiseBetaLine = plot(x,y,'-w','Parent',tool.handles.noiseBetaAxis);
            tool.handles.noiseBetaDot = plot(0,0,'.r','Parent',tool.handles.noiseBetaAxis,'MarkerSize',14);
            legend(tool.handles.noiseBetaAxis,{'Beta = 0.5','Beta = 0.5'},'TextColor','w','Box','off','Location','NorthWest')
            grid on;
            addlistener(tool.handles.NoiseAdditionBeta,'UserData','PostSet',@tool.handlePropEvents);
            
            %Create noise map panel
            fun=get(tool.handles.fig,'WindowScrollWheelFcn');
            fun2=get(tool.handles.fig,'WindowButtonMotionFcn');
            tool.handles.imtool_Noise = imtool3D(zeros(25),[.5 0 .5 1],tool.handles.NoiseAdditionTab,[],[tool.handles.imtool_Lesion tool.handles.imtool_CT]);
            set(tool.handles.fig,'WindowScrollWheelFcn',fun);
            set(tool.handles.fig,'WindowButtonMotionFcn',fun2);
            h=getHandles(tool.handles.imtool_Noise);
            set(h.Panels.Tools,'Visible','Off')
            set(h.Panels.ROItools,'Visible','Off')
            set(h.Panels.Info,'Visible','Off')
            set(h.Panels.Slider,'Visible','Off')
            set(h.Panels.Large,'Title','Noise Map')
            colormap(h.Axes,'hot')
            set(h.I,'ButtonDownFcn',[]);
            %Sync the noise panel with the CT image panel
            addlistener(tool.handles.imtool_CT,'newSlice',@tool.handlePropEvents);
            addlistener(tool.handles.DoseReductionSlider,'Value','PostSet',@tool.handlePropEvents);
            %Make NPS fit button
            tool.handles.FitNPSButton = uicontrol(tool.handles.NoiseAdditionTab,'Style','pushbutton','String','Fit NPS','Units','Pixel','TooltipString','Fit shape of NPS to patient data','HorizontalAlignment','center','Enable','Off');
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'FitNPSButton');
            set(tool.handles.FitNPSButton,'Callback',fun)
            %Make NPS fit # of slices text box
            tool.handles.FitNPSNSlices = uicontrol(tool.handles.NoiseAdditionTab,'Style','edit','String','4','Units','Pixels','TooltipString','# of slices to use in NPS fitting','HorizontalAlignment','left','UserData',4);
            fun=@(hObject,evnt) callbacks(hObject,evnt,tool,'IntegerGreaterThanZeroTextEdit');
            set(tool.handles.FitNPSNSlices,'Callback',fun)
            tool.handles.FitNPSNSlicesLabel = uicontrol(tool.handles.NoiseAdditionTab,'Style','Text','Units','Pixels','String','# of Slices','BackgroundColor','k','ForegroundColor','w','HorizontalAlignment','left');
            
            %--------------------------------------------------------------
            
            %Make sure new plots are not created on this figure
            addlistener(tool.handles.fig,'NextPlot','PostSet',@tool.handlePropEvents);
            addlistener(tool.handles.meshViewer,'meshUpdated',@tool.handlePropEvents);
            set(tool.handles.fig,'NextPlot','new')
            %Add listener for warning messages
            addlistener(tool,'warningMessage',@tool.handlePropEvents);
            %update the lesion
            updateLesion(tool)
            updateLibraryLists(tool)
            
        end
        
        function handlePropEvents(tool,src,evnt)
            
            switch evnt.EventName
                
                case 'newSlice'
                    if ~isempty(tool.CTInfo.S) && tool.noiseMeasured
                        slice = getCurrentSlice(tool.handles.imtool_CT);
                        setCurrentSlice(tool.handles.imtool_Noise,slice);
                        showSyntheticNoiseinCurrentSlice(tool);
                        
                    end
                
                case 'newMousePos'
                    [xi,yi,zi]=getCurrentMouseLocation(tool.handles.imtool_CT);
                    if ~isempty(tool.CTInfo.x)
                        
                        x=tool.CTInfo.x(xi); y=tool.CTInfo.y(yi); z=tool.CTInfo.z(zi);
                        h=getHandles(tool.handles.imtool_CT);
                        oldStr=get(h.Info,'String');
                    
                        newStr=[oldStr sprintf(' \t\t\tx= %3.1f',x) ', y=' sprintf('%3.1f',y) ', z=' sprintf('%3.1f',z)  ' mm'];
                    
                        set(h.Info,'String',newStr)
                    end
                
                case 'MTFPropertyChanged'
                    try
                        set(tool.handles.MTF_xy,'XData',tool.MTF_xy.f,'YData',tool.MTF_xy.mtf);
                        set(tool.handles.MTF_z,'XData',tool.MTF_z.f,'YData',tool.MTF_z.mtf);
                        
                        %set all the text boxes so they're in sync with the
                        %underlying MTF models
                        set(tool.handles.MTFxy_m_Edit,'String',num2str(tool.MTF_xy.m),'UserData',tool.MTF_xy.m);
                        set(tool.handles.MTFxy_c_Edit,'String',num2str(tool.MTF_xy.c),'UserData',tool.MTF_xy.c);
                        set(tool.handles.MTFxy_h_Edit,'String',num2str(tool.MTF_xy.h),'UserData',tool.MTF_xy.h);
                        set(tool.handles.MTFxy_s_Edit,'String',num2str(tool.MTF_xy.s),'UserData',tool.MTF_xy.s);
                        
                        set(tool.handles.MTFz_m_Edit,'String',num2str(tool.MTF_z.m),'UserData',tool.MTF_z.m);
                        set(tool.handles.MTFz_c_Edit,'String',num2str(tool.MTF_z.c),'UserData',tool.MTF_z.c);
                        set(tool.handles.MTFz_h_Edit,'String',num2str(tool.MTF_z.h),'UserData',tool.MTF_z.h);
                        set(tool.handles.MTFz_s_Edit,'String',num2str(tool.MTF_z.s),'UserData',tool.MTF_z.s);
                    end
                case 'NPSPropertyChanged'
                    try
                        [Fr, npsxy] = getNPSprofile(tool.NPS);
                        set(tool.handles.NPS_fxy,'XData',Fr,'YData',npsxy)
                        [fx,fz,npsz]=getZnps(tool.NPS);
                        set(tool.handles.NPS_z_image,'CData',mat2gray(npsz),'XData',fx,'YData',fz);
                        
                        %Set all the text so they're in sync with the
                        %underlying NPS model
                        set(tool.handles.NPS_noise_Edit,'String',num2str(tool.NPS.noise),'UserData',tool.NPS.noise);
                        set(tool.handles.NPS_b_Edit,'String',num2str(tool.NPS.b),'UserData',tool.NPS.b);
                        set(tool.handles.NPS_c_Edit,'String',num2str(tool.NPS.c),'UserData',tool.NPS.c);
                        set(tool.handles.NPS_d_Edit,'String',num2str(tool.NPS.d),'UserData',tool.NPS.d);
                        
                    end
                case 'meshUpdated'
                    notify(tool,'LesionPropertyChanged')
                    
                case 'fittingLesion'
                    set(tool.handles.Status,'String','Fitting Lesion to CT Data')
                    set(tool.handles.FitLesionButton,'Enable','off')
                    drawnow
                    
                case 'doneFittingLesion'
                    set(tool.handles.Status,'String','Done')
                    set(tool.handles.FitLesionButton,'Enable','on')
                    drawnow
                    pause(1)
                    set(tool.handles.Status,'String','Ready')
                    
                case 'insertingLesion'
                    set(tool.handles.Status,'String','Inserting Lesion')
                    set(tool.handles.InsertLesionButton,'Enable','off')
                    drawnow
                    
                case 'doneInsertingLesion'
                    set(tool.handles.Status,'String','Done')
                    set(tool.handles.InsertLesionButton,'Enable','on')
                    drawnow
                    pause(1)
                    set(tool.handles.Status,'String','Ready')
                    
                case 'maskChanged'
                    
                    %get the mask
                    mask = getMask(tool.handles.imtool_CT);
                    
                    %Get the seperate components
                    CC = bwconncomp(mask);
                    T{1} = 'Mask info:';
                    v = prod(tool.CTInfo.pspacing); %voxel volume
                    for i=1:CC.NumObjects
                        num = numel(CC.PixelIdxList{i});
                        vol = v*num;
                        T{i+1} = ['Region ' num2str(i) ', Volume = ' num2str(round(vol)) ' mm^3, ' '# of Voxels = ' num2str(num)];
                    end
                    set(tool.handles.MaskInfo_text,'String',T);
                    
                    if CC.NumObjects>0
                        set(tool.handles.FitLesionButton,'Enable','on')
                    else
                        set(tool.handles.FitLesionButton,'Enable','off')
                    end
                    
                case 'exportingImages'
                    set(tool.handles.Status,'String','Exporting CT Data')
                    set(tool.handles.ExportCTDataButton,'Enable','off')
                    drawnow
                    
                case 'exportedImages'
                    set(tool.handles.Status,'String','Done')
                    set(tool.handles.ExportCTDataButton,'Enable','on')
                    drawnow
                    pause(1)
                    set(tool.handles.Status,'String','Ready')
                    
                case 'warningMessage'
                    set(tool.handles.Status,'String',evnt.Message)
                    drawnow
                    pause(1)
                    set(tool.handles.Status,'String','Ready')
                    
                case 'newInsertionPosition'
                    if isempty(tool.handles.InsertionPoint)
                        T = getLocationText(evnt.Source);
                    else
                        T = getLocationText(tool.handles.InsertionPoint);
                    end
                    if isempty(tool.handles.NoiseInsertionPoint)
                        T2 = getLocationText(evnt.Source);
                    else
                        T2 = getLocationText(tool.handles.NoiseInsertionPoint);
                    end
                    method = get(tool.handles.Pulldown_insertionType,'String');
                    n = get(tool.handles.Pulldown_insertionType,'Value');
                    method = method{n};
                    switch method
                        case 'Superposition'
                            t = ['Insertion point = ' T];
                        case 'Alpha Blending'
                            t = {['Insertion point = ' T],['Noise point = ' T2]};
                            
                    end
                    set(tool.handles.InsertionLocation_text,'String',t);
                    
                case 'readingCTData'
                    set(tool.handles.Status,'String','Reading CT Data')
                    set(tool.handles.LoadCTDataButton,'Enable','off')
                    drawnow
                    
                case 'readCTData'
                    set(tool.handles.Status,'String','Done')
                    set(tool.handles.LoadCTDataButton,'Enable','on')
                    drawnow
                    pause(1)
                    set(tool.handles.Status,'String','Ready')
                    
                case 'updatingLesion'
                    set(tool.handles.Status,'String','Voxelizing the lesion model')
                    set(tool.handles.ButtonLesionRefresh,'Enable','off','ForegroundColor','k')
                    drawnow
                    
                case 'updatedLesion'
                    set(tool.handles.Status,'String','Done')
                    drawnow
                    pause(1)
                    set(tool.handles.Status,'String','Ready')
                    
                case 'LesionPropertyChanged'
                    set(tool.handles.ButtonLesionRefresh,'Enable','on','ForegroundColor','r')
                    lesion = getUpdatedLesion(tool);
                    FOV = lesion.FOV;
                    try
                        FOV = [tool.handles.meshViewer.center-FOV./2; tool.handles.meshViewer.center+FOV./2];
                        tool.handles.meshViewer.FOV=FOV;
                    catch
                        FOV = [tool.meshViewer.center-FOV./2; tool.meshViewer.center+FOV./2];
                        tool.meshViewer.FOV=FOV;
                    end
                    [x,y] = getAverageProfile(lesion);
                    set(tool.handles.AverageProfile,'XData',x,'YData',y)
                    C = lesion.contrast;
                    if C < 0
                        lims = [C 0];
                    else
                        lims = [0 C];
                    end
                    set(tool.handles.ProfileAxes,'Ylim',lims);
                    psize=lesion.psize;
                    psize_pre = lesion.psize_pre;
                    n_up = lesion.n_up;
                    str = {['Voxel size = (' num2str(psize(1)) ',' num2str(psize(2)) ',' num2str(psize(3)) ') mm'], ...
                        ['Pre-upsampled voxel size = (' num2str(psize_pre(1)) ',' num2str(psize_pre(2)) ',' num2str(psize_pre(3)) ') mm' ],...
                        ['Upsampled # of voxels = (' num2str(n_up(1)) ',' num2str(n_up(2)) ',' num2str(n_up(3)) ')'] };
                    set(tool.handles.psizeText,'String',str)
                    
                case 'TexturePropertyChanged'
                    
                case 'PostSet'
                    switch evnt.AffectedObject
                        case tool.handles.fig
                            set(evnt.AffectedObject,'NextPlot','new')
                        case tool.handles.inserted_List
                            showInsertedLesionText(tool)
                        case  tool.handles.DoseReductionSlider
                            set(tool.handles.DoseReductionText,'String',sprintf('%d%%',round(100*get(tool.handles.DoseReductionSlider,'Value'))))
                            showSyntheticNoiseinCurrentSlice(tool);
                        case tool.handles.NoiseAdditionBeta
                            %update the noise reduction plot
                            beta = get(tool.handles.NoiseAdditionBeta,'UserData');
                            Pn = @(x) 100*((1./(1-x)).^beta-1);
                            x = get(tool.handles.noiseBetaLine,'XData');
                            set(tool.handles.noiseBetaLine,'YData',Pn(x));
                            legend(tool.handles.noiseBetaAxis,{'Beta = 0.5',['Beta = ' num2str(beta)]},'TextColor','w','Box','off','Location','NorthWest')
                            set(tool.handles.noiseBetaAxis,'YTick',[Pn(0) round(Pn(.95))],'YTickLabel',{'0%',[sprintf('%d',round(Pn(.95))) '%']});
                            %Update CT image
                            showSyntheticNoiseinCurrentSlice(tool);
                            
                        
                            
                    end
                    
                case 'CTimageChanged'
                    T{1} = 'Image Info';
                    T{2} = ['Format: ' tool.CTInfo.format];
                    T{3} = ['File Name: ' tool.CTInfo.fname];
                    T{4} = ['# of Pixels: ' num2str(tool.CTInfo.S)];
                    T{5} = ['Pixel Size (mm): ' num2str(tool.CTInfo.psize)];
                    T{6} = ['FOV (mm): ' num2str(tool.CTInfo.FOV)];
                    set(tool.handles.ImageInfo_text,'String',T);
                    
                    %Need to set the output options according to the type
                    %of the input
                    try
                        switch tool.CTInfo.format
                            case 'none'
                                set(tool.handles.ImageInfoButton,'Visible','off')
                                set(tool.handles.Pulldown_exportType,'Visible','off')
                                set(tool.handles.ExportCTDataButton,'Visible','off')
                                set(tool.handles.InsertLesionButton,'Enable','off')
                                h=getHandles(tool.handles.imtool_CT);
                                set(h.Panels.Large,'Visible','off')
                                
                            case 'DICOMs'
                                set(tool.handles.ImageInfoButton,'Visible','on')
                                set(tool.handles.Pulldown_exportType,'Visible','on')
                                set(tool.handles.ExportCTDataButton,'Visible','on')
                                set(tool.handles.InsertLesionButton,'Enable','on')
                                h=getHandles(tool.handles.imtool_CT);
                                set(h.Panels.Large,'Visible','on')
                            case '.ipv'
                                set(tool.handles.ImageInfoButton,'Visible','on')
                                set(tool.handles.Pulldown_exportType,'Visible','on')
                                set(tool.handles.ExportCTDataButton,'Visible','on')
                                set(tool.handles.InsertLesionButton,'Enable','on')
                                h=getHandles(tool.handles.imtool_CT);
                                set(h.Panels.Large,'Visible','on')
                            case '.raw'
                                set(tool.handles.ImageInfoButton,'Visible','on')
                                set(tool.handles.Pulldown_exportType,'Visible','on')
                                set(tool.handles.ExportCTDataButton,'Visible','on')
                                set(tool.handles.InsertLesionButton,'Enable','on')
                                h=getHandles(tool.handles.imtool_CT);
                                set(h.Panels.Large,'Visible','on')
                            otherwise
                                set(tool.handles.ImageInfoButton,'Visible','off')
                                set(tool.handles.Pulldown_exportType,'Visible','off')
                                set(tool.handles.ExportCTDataButton,'Visible','off')
                                set(tool.handles.InsertLesionButton,'Enable','off')
                                h=getHandles(tool.handles.imtool_CT);
                                set(h.Panels.Large,'Visible','off')
                        end
                        
                        
                        %Need to reset the inserted lesions
                        tool.insertedLesion=struct('lesion',{},'I',{},'Position',{},'Position_mm',{},'Lims',{},'Method',{},'MTFblur',{},'Partial',{},'Alpha',{});
                        set(tool.handles.inserted_List,'String','','Value',0);
                        
                        %Need to reset the noise map
                        tool.noiseMeasured=false;
                        setImage(tool.handles.imtool_Noise,zeros(25));
                        
                        %Enable the measure noise button
                        set(tool.handles.MeasureCTNoiseButton,'Enable','on')
                        
                        %Enable the fit NPS button
                        set(tool.handles.FitNPSButton,'Enable','on')
                        
                        %Need to reset the generated noise;
                        tool.noiseGenerated = false;
                        tool.generatedNoise = [];
                        set(tool.handles.GenerateCTNoiseButton,'Enable','off')
                        set(tool.handles.DoseReductionSlider,'Enable','off')
                    end
                    
            end
        end
        
        function fitNPStoCTData(tool,ROI,slice,nSlices)
            %Get the ROI position
            position = getPosition(ROI);
            pos = round([position(1)-position(3)/2 position(2)-position(3)/2 position(3) position(4)]);
            %%%%%%Need to add some error checking to see if the box is within
            %%%%%%bounds
            
            %get CT data
            lims = [pos(2) pos(2)+pos(4)-1; pos(1) pos(1)+pos(3)-1; slice slice+nSlices-1];
            im = getImageValues(tool.handles.imtool_CT,lims);
            %Fit the NPS model to the data
            fitNPS2ROI(tool.NPS,im,tool.CTInfo.psize,'2D');
            
            
        end
        
        function generateCTNoise(tool)
            if tool.noiseMeasured
                %Set forward projection settings
                nDetectors = 736;       %These are based on a Siemens scanner. May want to generalize but seem to be OK for now
                nAngles = 360;
                FanCoverage = 'cycle';
                FanRotationIncrement = 1;
                FanSensorGeometry = 'arc';
                FanDetectorSpan = 75; %degrees
                FanSensorSpacing = FanDetectorSpan/nDetectors;
                ParallelCoverage = FanCoverage;
                mu_water = 1.837E-02; %assumed linear attenuation of water
                SFOV = 500; %Scan field of view
                
                %get dicom header info (need source to detector distance)
                info = tool.CTInfo.Headers(1);
                psize = tool.CTInfo.pspacing(1);
                D = (tool.CTInfo.FOV(1)/SFOV)*tool.projectionDistanceScaling*(info.DistanceSourceToDetector/2)/psize; %Distance from source to center of image in pixels
                dTh = 360/nAngles;
                
                
                
                for i=1:tool.CTInfo.S(3)
                    set(tool.handles.Status,'String',['Generating projection noise for slice ' num2str(i) '/' num2str(tool.CTInfo.S(3))]); drawnow
                    
                    %Forward project through slice
                    im = getImageSlices(tool.handles.imtool_CT,i,i); %Get the slice
                    im = mu_water + mu_water*im/1000; im(im<0)=0; %Convert to attenuation
                    im = fanbeam(im,D,'FanRotationIncrement',dTh,...
                        'FanSensorGeometry',FanSensorGeometry,'FanSensorSpacing',FanSensorSpacing);
                    %Generate random projections
                    F = random('norm',0,im);
                    %Interpolate to parrallel geometry
                    [P, xp, theta] = fan2para(F,D,'FanCoverage',FanCoverage,'FanRotationIncrement',FanRotationIncrement,...
                        'FanSensorGeometry',FanSensorGeometry,'FanSensorSpacing',FanSensorSpacing,'ParallelCoverage',ParallelCoverage);
                    %Filter with nps
                    xp=diff(xp); xp=xp(1);
                    %P=filterProjectionsWithNPS(tool.NPS,P,xp);
                    
                    %Reconstruct
                    im = iradon(P,theta,'Linear','Ram-Lak',1,tool.CTInfo.S(1));
                    %im = iradon(P,theta,'Linear','none',1,tool.CTInfo.S(1));
                    noise(:,:,i) = im;
                      
                end
                %Filter with NPS
                set(tool.handles.Status,'String','Filtering noise with NPS'); drawnow;
                noise = filterImagewithNPS(tool.NPS,noise,tool.CTInfo.psize);
                
                %Normalize the local variance
                set(tool.handles.Status,'String','Normalizing local variance'); drawnow;
                %nhood = strel('sphere',8);
                nhood = strel('disk',8,0);
                nhood = nhood.Neighborhood;
                noise = noise./stdfilt(noise,nhood);
                
                tool.generatedNoise=noise;
                set(tool.handles.Status,'String','Ready')
                tool.noiseGenerated=true;
                set(tool.handles.DoseReductionSlider,'Enable','on')
                showSyntheticNoiseinCurrentSlice(tool)
                
                
            end
            
            
        end
        
        function measureCTNoise(tool)
            
            if ~isempty(tool.CTInfo.S)
                noise = zeros(tool.CTInfo.S);
                for i=1:size(noise,3)
                    set(tool.handles.Status,'String',['Measuring noise in slice ' num2str(i) '/' num2str(size(noise,3))])
                    drawnow;
                    lims = [1 tool.CTInfo.S(1); 1 tool.CTInfo.S(2); i i];
                    im = getImageValues(tool.handles.imtool_CT,lims);
                    
                    %new projection-based method
                    SFOV = 500; %Scan field of view
                    info = tool.CTInfo.Headers(i);
                    psize = tool.CTInfo.pspacing(1);
                    D = (tool.CTInfo.FOV(1)/SFOV)*tool.projectionDistanceScaling*(info.DistanceSourceToDetector/2)/psize;
                    noise(:,:,i)=getCTNoiseMapProjectionMethod(im,D);
                    
                    %Old image-based method
                    %noise(:,:,i)=getCTNoiseMap(getImageValues(tool.handles.imtool_CT,lims));
                end
                setImage(tool.handles.imtool_Noise,noise);
                tool.noiseMeasured=true;
                set(tool.handles.Status,'String','Ready')
                set(tool.handles.MeasureCTNoiseButton,'Enable','off')
                set(tool.handles.GenerateCTNoiseButton,'Enable','on')
            end
            
            
        end
        
        function noise = getNoiseImage(tool)
            %Get the Percent reduced dose
             P = get(tool.handles.DoseReductionSlider,'Value');
             if P>0 && tool.noiseGenerated
                 beta = get(tool.handles.NoiseAdditionBeta,'UserData');
                 noise = tool.generatedNoise.*getImage(tool.handles.imtool_Noise)*sqrt(((1/(1-P).^(2*beta))-1));
             else
                 noise=zeros(tool.CTInfo.S);
             end
        end
        
        function showSyntheticNoiseinCurrentSlice(tool)
            
            if tool.noiseMeasured && tool.noiseGenerated
                
                %Get the Percent reduced dose
                P = get(tool.handles.DoseReductionSlider,'Value');
                %Get the slice # of interest
                slice = getCurrentSlice(tool.handles.imtool_CT);
                %Get the CT slice
                im = getCurrentImageSlice(tool.handles.imtool_CT);
                h = getHandles(tool.handles.imtool_CT);
                %add the noise to the current image
                if P > 0
                    %Get the noise map slice
                    STDmap = getCurrentImageSlice(tool.handles.imtool_Noise);
                    %Get the generated noise slice
                    noise = tool.generatedNoise(:,:,slice);
                    %Get the scale of the noise (according to P and STDmap)
                    %scale=STDmap*sqrt(((1/(1-P))-1));
                    beta = get(tool.handles.NoiseAdditionBeta,'UserData');
                    scale=STDmap*sqrt(((1/(1-P)).^(2*beta)-1)); %This is
                    %the generalized version that doesn't assume a sqrt
                    %relationship between dose and noise.
                    
                    %scale the noise to have the targeted STD
                    noise = noise.*scale;
                    %set the displayed image to include the added noise
                    set(h.I,'CData',noise + im);
                    
                    Pn = @(x) 100*((1./(1-x)).^beta-1);
                    set(tool.handles.noiseBetaDot,'Xdata',P,'YData',Pn(P));
                else
                    set(h.I,'CData',im);
                    set(tool.handles.noiseBetaDot,'Xdata',0,'YData',0);
                end
            end
            
        end
        
        function getCTPhysicalCoordinate(tool,xi,yi,zi)
            
        end
        
        function showErrorMessage(tool,message)
            notify(tool,'warningMessage',warningEventData(message));
        end
        
        function updateLesion(tool)
            %This function updates the lesion properties
            %Broadcast that the lesion is being updated
            notify(tool,'updatingLesion')
            
            %update the lesion properties according to the user defined
            %settings
            getUpdatedLesion(tool);
            
            %Voxelize the lesion
            I = voxelizeLesion(tool.lesion);
            
            %Update the imtool3D viewer (this should also update the 3D
            %viewer)
            slice = round(size(I,3)/2);
            setImage(tool.handles.imtool_Lesion,I)
            setCurrentSlice(tool.handles.imtool_Lesion,slice)
            % set the aspect ratio of the
            setAspectRatio(tool.handles.imtool_Lesion,tool.lesion.psize)
            %h=getHandles(tool.handles.imtool_Lesion);
            %set(h.Axes,'DataAspectRatio',1./tool.lesion.psize)
            
            
            %broadcast that the lesion is done being updated
            notify(tool,'updatedLesion')
        end
        
        function [varargout] = getUpdatedLesion(tool)
            
            switch nargout
                case 0
                    lesion = tool.lesion;
                case 1  %use this when you want to make a copy of the lesion instead of actually altering the lesion
                    lesion = copy(tool.lesion);
                    varargout{1} = lesion;
            end
            
            %Get the shape
            n = get(tool.handles.Pulldown_shapeType,'Value');
            shapeType = get(tool.handles.Pulldown_shapeType,'String');
            shapeType=shapeType{n};
            switch shapeType
                case 'Spoke'
                    shape.type = shapeType;
                    shape.Rb = get(tool.handles.SpokeShapeTable,'Data');
                    data = get(tool.handles.SpokeShapeTable,'UserData');
                    shape.theta_b = data.theta_b;
                    shape.phi_b = data.phi_b;
                    shape.f = str2num(get(tool.handles.ShapeFilterTextEdit,'String'));
                    lesion.shape = shape;
                case {'Mesh', 'Simple Mesh'}
                    
                    %set the shape settings of the lesion
                    shape.type=shapeType;
                    try
                        shape.faces=tool.handles.meshViewer.fv.faces;
                        shape.vertices=tool.handles.meshViewer.fv.vertices;
                        shape.center=tool.handles.meshViewer.center;
                    catch
                        shape.faces=tool.meshViewer.fv.faces;
                        shape.vertices=tool.meshViewer.fv.vertices;
                        shape.center=tool.meshViewer.center;
                    end
                    lesion.shape=shape;
                    
                    
                    
            end
            
            %get the profile
            n = get(tool.handles.Pulldown_profileType,'Value');
            profileType = get(tool.handles.Pulldown_profileType,'String');
            profileType=profileType{n};
            switch profileType
                case 'Designer'
                    profile.type = profileType;
                    profile.n = str2num(get(tool.handles.nTextEdit,'String'));
                    profile.C = str2num(get(tool.handles.PeakContrastTextEdit,'String'));
                    lesion.profile=profile;
                case 'Flat'
                    profile.type=profileType;
                    profile.C = str2num(get(tool.handles.PeakContrastTextEdit,'String'));
                    lesion.profile=profile;
            end
            
            %get the blurring
            n = get(tool.handles.Pulldown_blurType,'Value');
            blurType = get(tool.handles.Pulldown_blurType,'String');
            blurType=blurType{n};
            switch blurType
                case 'Gaussian'
                    blur.type=blurType;
                    blur.PSF = [str2num(get(tool.handles.BxEdit,'String')) str2num(get(tool.handles.ByEdit,'String')) str2num(get(tool.handles.BzEdit,'String'))];
                    lesion.blur=blur;
                case 'None'
                    blur.type = blurType;
                    lesion.blur=blur;
            end
            
            %get the texture type
            n = get(tool.handles.Pulldown_textureType,'Value');
            textureType = get(tool.handles.Pulldown_textureType,'String');
            textureType=textureType{n};
            switch textureType
                case 'None'
                    texture.type='none';
                    lesion.texture=texture;
                case 'Clustered Lumpy Background'
                    texture.type = 'clusturedLumpyBackground';
                    texture.K = str2num(get(tool.handles.KTextEdit,'String')); %Number of clusters per mm^3
                    texture.N = str2num(get(tool.handles.NTTextEdit,'String'));  %Number of blobs (per cluster point) total number of blobs = round(K*vol*N)
                    texture.sigma = str2num(get(tool.handles.SigmaTextEdit,'String')); %Defines how far away subclusters are from their parent
                    texture.alpha = str2num(get(tool.handles.AlphaTextEdit,'String')); %Alpha and beta are parameters of the exponential function
                    texture.beta = str2num(get(tool.handles.BetaTextEdit,'String'));
                    texture.L = [str2num(get(tool.handles.LxTextEdit,'String')) str2num(get(tool.handles.LyTextEdit,'String')) str2num(get(tool.handles.LzTextEdit,'String'))]; %vector that defines the size of the function in each direction
                    switch tool.lesion.textureType %Set the new positions to empty if the old lesion didn't have texture
                        case 'none'
                            texture.positions = [];
                        case 'clusturedLumpyBackground'
                            texture.positions = tool.lesion.texture.positions;      %may need to add a refresh button to update the texture positions when the size of the lesion changes
                        otherwise
                            texture.positions = [];
                    end
                    texture.contrast = str2num(get(tool.handles.TextureContrastTextEdit,'String')); %This defines the magnitude of texture fluctuations as a fraction of the lesion contrast
                    lesion.texture=texture;
            end
            
            %get the voxelization settings
            lesion.FOV = [str2num(get(tool.handles.FOVxEdit,'String')) str2num(get(tool.handles.FOVyEdit,'String')) str2num(get(tool.handles.FOVzEdit,'String'))];
            lesion.n = [str2num(get(tool.handles.NxEdit,'String')) str2num(get(tool.handles.NyEdit,'String')) str2num(get(tool.handles.NzEdit,'String'))];
            lesion.scale = round(str2num(get(tool.handles.ScaleEdit,'String')));
            n = get(tool.handles.Pulldown_interpType,'Value');
            interpType = get(tool.handles.Pulldown_interpType,'String');
            interpType=interpType{n};
            lesion.interpType = interpType;
            
        end
        
        function setUpdatedLesion(tool,lesion)
            %This function takes a new lesion model and imports it into the
            %GUI
            %tool.lesion=lesion;
            
            %Set the shape settings
            shapeTypes = get(tool.handles.Pulldown_shapeType,'String');
            ind = find(strcmp(shapeTypes,lesion.shapeType));
            set(tool.handles.Pulldown_shapeType,'Value',ind);
            switch lesion.shapeType
                case 'Spoke'
                    [rowNames,columnNames,columnWidths] = getShapeTableNames(lesion);
                    data.theta_b = lesion.shape.theta_b; data.phi_b = lesion.shape.phi_b;
                    set(tool.handles.SpokeShapeTable,'Data',lesion.shape.Rb,'RowName',rowNames,'ColumnName',columnNames,'ColumnWidth',columnWidths,'UserData',data);
                    set(tool.handles.ShapeFilterTextEdit,'String',num2str(lesion.shape.f))
                case {'Mesh','Simple Mesh'}
                    fv.faces = lesion.shape.faces;
                    fv.vertices = lesion.shape.vertices;
                    tool.handles.meshViewer.fv=fv;
            end
            
            
            %Set the profile settings
            profileTypes = get(tool.handles.Pulldown_profileType,'String');
            ind = find(strcmp(profileTypes,lesion.profileType));
            set(tool.handles.Pulldown_profileType,'Value',ind);
            switch lesion.profileType
                case 'Designer'
                    set(tool.handles.PeakContrastTextEdit,'String',num2str(lesion.profile.C))
                    set(tool.handles.nTextEdit,'String',num2str(lesion.profile.n))
                case 'Flat'
                    set(tool.handles.PeakContrastTextEdit,'String',num2str(lesion.profile.C))
            end
            
            %Set the blur settings
            blurTypes = get(tool.handles.Pulldown_blurType,'String');
            ind = find(strcmp(blurTypes,lesion.blurType));
            set(tool.handles.Pulldown_blurType,'Value',ind);
            switch lesion.blurType
                case 'Gaussian'
                    set(tool.handles.BxEdit,'String',num2str(lesion.blur.PSF(1)))
                    set(tool.handles.ByEdit,'String',num2str(lesion.blur.PSF(2)))
                    set(tool.handles.BzEdit,'String',num2str(lesion.blur.PSF(3)))
                case 'None'
            end
            
            %Set the texture settings
            textureTypes = get(tool.handles.Pulldown_textureType,'String');
            switch lesion.textureType
                case 'none'
                    textureType='None';
                case 'clusturedLumpyBackground'
                    textureType = 'Clustered Lumpy Background';
            end
            ind = find(strcmp(textureTypes,textureType));
            set(tool.handles.Pulldown_textureType,'Value',ind);
            switch lesion.textureType
                case 'none'
                case 'clusturedLumpyBackground'
                    set(tool.handles.KTextEdit,'String',num2str(lesion.texture.K))
                    set(tool.handles.NTTextEdit,'String',num2str(lesion.texture.N))
                    set(tool.handles.SigmaTextEdit,'String',num2str(lesion.texture.sigma))
                    set(tool.handles.AlphaTextEdit,'String',num2str(lesion.texture.alpha))
                    set(tool.handles.BetaTextEdit,'String',num2str(lesion.texture.beta))
                    set(tool.handles.LxTextEdit,'String',num2str(lesion.texture.L(1)))
                    set(tool.handles.LyTextEdit,'String',num2str(lesion.texture.L(2)))
                    set(tool.handles.LzTextEdit,'String',num2str(lesion.texture.L(3)))
                    set(tool.handles.TextureContrastTextEdit,'String',num2str(lesion.texture.contrast))
            end
            
            %Set the Voxelization settings
            interpTypes = get(tool.handles.Pulldown_interpType,'String');
            ind = find(strcmp(interpTypes,lesion.interpType));
            set(tool.handles.Pulldown_interpType,'Value',ind);
            set(tool.handles.FOVxEdit,'String',num2str(lesion.FOV(1)))
            set(tool.handles.FOVyEdit,'String',num2str(lesion.FOV(2)))
            set(tool.handles.FOVzEdit,'String',num2str(lesion.FOV(3)))
            set(tool.handles.NxEdit,'String',num2str(lesion.n(1)))
            set(tool.handles.NyEdit,'String',num2str(lesion.n(2)))
            set(tool.handles.NzEdit,'String',num2str(lesion.n(3)))
            set(tool.handles.ScaleEdit,'String',num2str(lesion.scale))
            
            %Set the type, name, and tag settings
            set(tool.handles.LesionTypeEdit,'String',lesion.type)
            set(tool.handles.LesionTagEdit,'String',lesion.tag)
            set(tool.handles.LesionNameEdit,'String',lesion.name)
            
            %set the iso settings
            tool.lesion.iso=lesion.iso;
            
            updateViewSettings(tool)
            notify(tool,'LesionPropertyChanged')
            updateLesion(tool)
            
        end
        
        function updateViewSettings(tool)
            %this function runs a series of callbacks to make sure the view
            %is correct when a new leasion is loaded
            
            %update the size/shape view
            callbacks(tool.handles.Pulldown_shapeType,[],tool,'Pulldown_shapeType')
            
            %update the profile view
            callbacks(tool.handles.Pulldown_profileType,[],tool,'Pulldown_profileType')
            callbacks(tool.handles.Pulldown_blurType,[],tool,'Pulldown_blurType')
            
            %update the texture view
            callbacks(tool.handles.Pulldown_textureType,[],tool,'Pulldown_textureType')
            
        end
        
        function updateLibraryLists(tool)
            types = unique(tool.lesionTable.type);
            types = ['All' ;types];
            tags = unique(tool.lesionTable.tag);
            tags = ['All' ;tags];
            
            nTypes = get(tool.handles.type_List,'Value');
            if nTypes > length(types)
                nTypes=length(types);
            end
            
            nTags = get(tool.handles.tag_List,'Value');
            if nTags > length(tags)
                nTags=length(tags);
            end
            
            set(tool.handles.type_List,'String',types,'Value',nTypes);
            set(tool.handles.tag_List,'String',tags,'Value',nTags);
            
            callbacks([],[],tool,'LibraryList')
            
        end
        
        function lesionTable = createLesionTable(tool)
            
            %Initialize the table
            lesionTable = table({},{},{},{},[],{},{},{},[],'VariableNames',{'FileName','type','tag','name','contrast','profileType','blurType','textureType','row'});
            
            %Get a list of all the lesion files
            list = dir([tool.paths.SavedLesions '*.mat']);
            
            n=1;
            for i=1:length(list)
                clear lesion
                load([tool.paths.SavedLesions list(i).name])
                try
                    if exist('lesion','var')
                        %initialize the temporary table
                        temp = table({list(i).name},{lesion.type},{lesion.tag},{lesion.name},[lesion.contrast],{lesion.profileType},{lesion.blurType},{lesion.textureType},[n],'VariableNames',{'FileName','type','tag','name','contrast','profileType','blurType','textureType','row'});
                        lesionTable = [lesionTable ;temp];
                        n=n+1;
                    end
                catch ME
                    ME
                    
                    
                end
                
            end
            
        end
        
        function filteredLesionTable = get.filteredLesionTable(tool)
            %gets a lesionTable filtered by the selected type and tag
            
            %Filter by the type
            nTypes = get(tool.handles.type_List,'Value');
            if nTypes ==1
                indType = true(size(tool.lesionTable,1),1);
            else
                type =  get(tool.handles.type_List,'String');
                type = type{nTypes};
                indType = strcmp(type,tool.lesionTable.type);
            end
            
            
            %Filter by the tag
            nTags = get(tool.handles.tag_List,'Value');
            if nTags ==1
                indTag = true(size(tool.lesionTable,1),1);
            else
                tag =  get(tool.handles.tag_List,'String');
                tag = tag{nTags};
                indTag = strcmp(tag,tool.lesionTable.tag);
            end
            
            %get the list of names
            ind = indType & indTag;
            filteredLesionTable = tool.lesionTable(ind,:);
        end
        
        function ind = get.lesionTableIndex(tool)
            %gets the row index of the currently selected lesion in the library
            filteredLesionTable = tool.filteredLesionTable;
            n = get(tool.handles.name_List,'Value');
            if n>0
                ind = filteredLesionTable.row(n);
            else
                ind = 0;
            end
            
        end
        
        function T = getTextFromLesionTable(tool,ind)
            
            if ind>0
                
                T{1} = ['File Name: ' tool.lesionTable.FileName{ind}];
                T{2} = ['Type: ' tool.lesionTable.type{ind}];
                T{3} = ['Tag: ' tool.lesionTable.tag{ind}];
                T{4} = ['Name: ' tool.lesionTable.name{ind}];
                T{5} = ['Contrast: ' num2str(tool.lesionTable.contrast(ind))];
                T{6} = ['Profile Type: ' tool.lesionTable.profileType{ind}];
                T{7} = ['Blur Type: ' tool.lesionTable.blurType{ind}];
                T{8} = ['Texture Type: ' tool.lesionTable.textureType{ind}];
                
            else
                T = 'No lesion selected';
            end
            
            
        end
        
        function set.CTInfo(tool,CTInfo)
            tool.CTInfo = CTInfo;
            notify(tool,'CTimageChanged')
        end
        
        function insertLesion(tool)
            
            notify(tool,'insertingLesion')
            
            %voxelize the lesion at the resolution of the CT data
            lesion = copy(tool.lesion); lesion.origin=[0 0 0];
            n = ceil(lesion.FOV./tool.CTInfo.pspacing);
            lesion.FOV=n.*tool.CTInfo.pspacing;
            lesion.n=n;
            
            % Account for case of thick slices with small increments
            if tool.CTInfo.psize(3)>tool.CTInfo.pspacing(3)
                sliceOverlap = true;
                
                %Determine extra space on end slices that needs to be added
                nBuff = ceil((tool.CTInfo.psize(3)-tool.CTInfo.pspacing(3))/(2*tool.CTInfo.pspacing(3)));
                buff = 2*nBuff * tool.CTInfo.pspacing(3);
                
                %Extend the FOV
                lesion.FOV(3)=lesion.FOV(3)+buff;
                lesion.n(3)=lesion.n(3)+2*nBuff;
                
                %Create the averaging kernel
                nFlat = floor(tool.CTInfo.psize(3)/tool.CTInfo.pspacing(3));
                res = (tool.CTInfo.psize(3) - (nFlat*tool.CTInfo.pspacing(3)))/tool.CTInfo.pspacing(3);
                sliceKernel = ones(nFlat,1);
                sliceKernel = [res; sliceKernel; res];
                sliceKernel = sliceKernel/sum(sliceKernel);
                sliceKernel = reshape(sliceKernel,[1 1 numel(sliceKernel)]);
                
            else
                sliceOverlap=false;
            end
            
            %Voxelize the lesion (account for parital voluming if needed)
            if get(tool.handles.PartialCheckBox,'Value')
                partial='Yes';
                lesion.scale=get(tool.handles.ParitalTextEdit,'UserData');
                I = voxelizeLesion(lesion); %upsampled voxelization
                h = ones([lesion.scale lesion.scale lesion.scale]); %create the averaging filter
                h= h./sum(h(:));
                I = imfilter(I,h);      %filter to account for partial voluming
                
                %downsample
                [Xq,Yq,Zq] = getCartesianCoordinates(lesion,'Pre');
                [X,Y,Z] = getCartesianCoordinates(lesion,'Post');
                I = interp3(X,Y,Z,I,Xq,Yq,Zq,'linear',0);
                lesion.scale=1;
            else
                partial='No';
                lesion.scale=1;
                I = voxelizeLesion(lesion);
            end
            
            if sliceOverlap
                %run the slice averaging kernel
                I = imfilter(I,sliceKernel);
                
                %Cut off the extra slices
                I = I(:,:,nBuff+1:size(I,3)-nBuff);
                lesion.n(3)=lesion.n(3)-2*nBuff;
                lesion.FOV(3)=lesion.FOV(3)-buff;
                
                
                
            end
            
            %Further blur with MTF if needed
            if get(tool.handles.MTFCheckBox,'Value')
                MTFblur='Yes';
                try
                    I = blurWithMTF(I,tool,lesion);
                catch
                    MTFblur='Error';
                    showErrorMessage(tool,'Error with MTF blurring')
                end
            else
                MTFblur='No';
            end
            
            %get the insertion method
            n = get(tool.handles.Pulldown_insertionType,'Value');
            method = get(tool.handles.Pulldown_insertionType,'String');
            method = method{n};
            
            switch method
                case 'Superposition'
                    %Crop the lesion image
                    [lims,I,position]=cropLesionROI(tool,I,tool.handles.InsertionPoint,lesion);
                    alpha = zeros(size(I));
                case 'Alpha Blending'
                    %Get the noise image
                    [nLims,~,~]=cropLesionROI(tool,I,tool.handles.NoiseInsertionPoint,lesion);
                    %Need to add code to make sure the noise image is the
                    %same size as the insertion image!!!!!!!!!!!!!!!!!!!!!!
                    noise = getImageValues(tool.handles.imtool_CT,nLims);
                    noise = noise-mean(noise(:));
                    
                    %get the alpha map
                    nLesion=copy(lesion);
                    profile.type='Flat'; profile.C=1;
                    nLesion.profile=profile;
                    blur.type='Gaussian';
                    blur.PSF=[1 1 1];       %This is hardcoded! May want to change in the future!
                    nLesion.blur=blur;
                    alpha = voxelizeLesion(nLesion);
                    
                    %Crop the lesion image and the alpha image
                    [lims,I,position]=cropLesionROI(tool,I,tool.handles.InsertionPoint,lesion);
                    [~,alpha,~]=cropLesionROI(tool,alpha,tool.handles.InsertionPoint,lesion);
                    
                    %Add noise to the lesion image
                    I = I+noise;
                    
                    %get the pixels within the limits from the image
                    bkg = getImageValues(tool.handles.imtool_CT,lims);
                    
                    %Estimate the background HU value
                    bmask=alpha<=.1; B = mean(bkg(bmask));
                    
                    %alpha blend the lesion image with the background
                    I = (I+B).*alpha + (1-alpha).*bkg;
                    
                    %Get the difference between the blended image and the
                    %original background (this is what's actually added
                    %onto the image)
                    I = I-bkg;
                    
            end
            
            %Add the lesion
            addImageValues(tool.handles.imtool_CT,I,lims);
            
            %Add lesion to the inserted list
            addInsertedLesion(tool,lesion,position,lims,I,method,MTFblur,partial,alpha);
            
            notify(tool,'doneInsertingLesion')
            
        end
        
        function [lims,I,position]=cropLesionROI(tool,I,point,lesion)
            n=lesion.n; n=[n(2) n(1) n(3)];
            S = getImageSize(tool.handles.imtool_CT);
            
            %Get the bounding box where the lesion is to be inserted
            position = round(point.position);
            position(3) = getCurrentSlice(tool.handles.imtool_CT);
            position=[position(2) position(1) position(3)];
            lims=zeros(3,2);
            for i=1:length(n)
                if mod(n(i),2)
                    %odd
                    lims(i,:) = [position(i)-(n(i)-1)/2 position(i)+(n(i)-1)/2];
                else
                    %even
                    lims(i,:) = [position(i)-n(i)/2 position(i)-1+n(i)/2];
                end
                
            end
            
            %Crop the lesion image %%%%Need to fix this!!!!!!!!!!!!!!!!!!!!
            for i=1:length(n)
                if lims(i,1)<1
                    l(i)=abs(lims(i,1))+2;
                    lims(i,1) = 1;
                else
                    l(i)=1;
                end
                if lims(i,2)>S(i)
                    m(i) = size(I,i)-(lims(i,2)-S(i));
                    lims(i,2)=S(i);
                else
                    m(i)=size(I,i);
                end
            end
            I=I(l(1):m(1),l(2):m(2),l(3):m(3));
            position=[position(2) position(1) position(3)];
            
            
            
        end
        
        function addInsertedLesion(tool,lesion,position,lims,I,method,MTFblur,partial,alpha)
            
            %make the structured variable
            insertedLesion.lesion=lesion;
            insertedLesion.I=I;
            insertedLesion.Position=position;
            insertedLesion.Position_mm=(position-1).*tool.CTInfo.pspacing+tool.CTInfo.Origin;
            insertedLesion.Lims=lims;
            insertedLesion.Method=method;
            insertedLesion.MTFblur=MTFblur;
            insertedLesion.Partial=partial;
            insertedLesion.Alpha = alpha;
            
            %add it to the list
            tool.insertedLesion(end+1)=insertedLesion;
            T = get(tool.handles.inserted_List,'String');
            T{end+1}=['Lesion ' num2str(length(tool.insertedLesion))];
            set(tool.handles.inserted_List,'String',T);
            set(tool.handles.inserted_List,'Value',length(tool.insertedLesion))
        end
        
        function showInsertedLesionText(tool)
            switch get(tool.handles.inserted_List,'Value')
                case 0
                    set(tool.handles.InsertedLesion_text,'String','Inserted lesion info:');
                otherwise
                    n=get(tool.handles.inserted_List,'Value');
                    insertedLesion=tool.insertedLesion(n);
                    T{1} = 'Inserted lesion info:';
                    T{2} = ['Insertion method: ' insertedLesion.Method];
                    T{3} = ['Partial volumed?: ' insertedLesion.Partial];
                    T{4} = ['MTF blur?: ' insertedLesion.MTFblur];
                    T{5} = ['Pos: (' num2str(insertedLesion.Position(1)) ',' num2str(insertedLesion.Position(2)) ',' num2str(insertedLesion.Position(3)) ')'];
                    T{6} = ['Min: (' num2str(insertedLesion.Lims(2,1)) ',' num2str(insertedLesion.Lims(1,1)) ',' num2str(insertedLesion.Lims(3,1)) ')'];
                    T{7} = ['Max: (' num2str(insertedLesion.Lims(2,2)) ',' num2str(insertedLesion.Lims(1,2)) ',' num2str(insertedLesion.Lims(3,2)) ')'];
                    T{8} = ['Profile type: ' insertedLesion.lesion.profileType];
                    T{9} = ['Blur type: ' insertedLesion.lesion.blurType];
                    T{10} = ['Texture type: ' insertedLesion.lesion.textureType];
                    T{11} = ['Contrast: ' num2str(insertedLesion.lesion.contrast)];
                    set(tool.handles.InsertedLesion_text,'String',T);
                    
            end
        end
        
        function deleteInsertedLesion(tool)
            n=get(tool.handles.inserted_List,'Value');
            names = get(tool.handles.inserted_List,'String');
            if n>0
                %get the pixels that were added
                I=-tool.insertedLesion(n).I;
                lims=tool.insertedLesion(n).Lims;
                %Update the image
                addImageValues(tool.handles.imtool_CT,I,lims);
                %remove it from the list
                if n==1 && length(tool.insertedLesion)>1
                    tool.insertedLesion=tool.insertedLesion(2:end);
                    nNew = 1;
                    names=names(2:end);
                elseif n==1 && length(tool.insertedLesion)==1
                    tool.insertedLesion=struct('lesion',{},'I',{},'Position',{},'Position_mm',{},'Lims',{},'Method',{},'MTFblur',{},'Partial',{},'Alpha',{});
                    nNew=0;
                    names={};
                elseif n>1 && n<length(tool.insertedLesion)
                    tool.insertedLesion=[tool.insertedLesion(1:n-1) tool.insertedLesion(n+1:end)];
                    names=[names(1:n-1) ;names(n+1:end)];
                    nNew=n;
                elseif n>1 && n==length(tool.insertedLesion)
                    tool.insertedLesion=tool.insertedLesion(1:end-1);
                    names=names(1:n-1);
                    nNew=n-1;
                end
                
                set(tool.handles.inserted_List,'Value',nNew,'String',names)
                
                
                
            end
            
        end
        
        function I = blurWithMTF(I,tool,lesion)
            
            I = BlurWithMTFs3D(I,lesion.psize,tool.MTF_xy,tool.MTF_xy,tool.MTF_z);
            
        end
        
        function varargout = exportImage(tool,output)
            notify(tool,'exportingImages')
            %write out files according to the data type of the input
            if output
                %Get the image that will be exported
                I = getImagetoExport(tool);
                switch tool.CTInfo.format
                    case 'DICOMs'
                        
                        uid = writeDicoms(output,I,tool.CTInfo.Headers);
                        if nargout ==1
                            varargout{1}=uid;
                        end
                        
                    case '.ipv'
                        
                        exportIPIPEVolume(output,I,tool.CTInfo);
                        
                    otherwise
                        showErrorMessage(tool,'Error exporting CT data, data type not supported')
                end
                
                notify(tool,'exportedImages')
                
            end
        end
        
        function I = getImagetoExport(tool)
            n=get(tool.handles.Pulldown_exportType,'Value');
            option = get(tool.handles.Pulldown_exportType,'String');
            option = option{n};
            switch option
                case 'W/ CT Data'
                    I = getImage(tool.handles.imtool_CT)+getNoiseImage(tool);
                case 'W/O CT Data'
                    S = getImageSize(tool.handles.imtool_CT);
                    I = zeros(S);
                    for i=1:length(tool.insertedLesion)
                        im = tool.insertedLesion(i).I;
                        lims = tool.insertedLesion(i).Lims;
                        I(lims(1,1):lims(1,2),lims(2,1):lims(2,2),lims(3,1):lims(3,2))=...
                            I(lims(1,1):lims(1,2),lims(2,1):lims(2,2),lims(3,1):lims(3,2))+im;
                    end
                    
                    switch tool.CTInfo.format
                        case '.ipv'
                            I=I+tool.CTInfo.rescale;
                    end
                    
                case 'Alpha Maps'
                    S = getImageSize(tool.handles.imtool_CT);
                    I = zeros(S);
                    for i=1:length(tool.insertedLesion)
                        im = tool.insertedLesion(i).Alpha;
                        lims = tool.insertedLesion(i).Lims;
                        I(lims(1,1):lims(1,2),lims(2,1):lims(2,2),lims(3,1):lims(3,2))=...
                            I(lims(1,1):lims(1,2),lims(2,1):lims(2,2),lims(3,1):lims(3,2))+im;
                    end
                    
            end
            
            
        end
        
        function I = getCTImageSlices(tool,zmin,zmax)
            
            I = getImageSlices(tool.handles.imtool_CT,zmin,zmax);
            
        end
        
        function lims = getUserDefinedBox(tool)
            lims = zeros(3,2);
            h=getHandles(tool.handles.imtool_CT);
            str_old=get(tool.handles.Status,'String');
            axes(h.Axes);
            
            
            %get the left edge
            set(tool.handles.Status,'String','Click on LEFT-most edge of the lesion')
            k = waitforbuttonpress;
            bp=round(get(h.Axes,'CurrentPoint'));
            lims(1,1)=bp(1,1);
            h_temp(1)=scatter(bp(1,1),bp(1,2),'or','filled','Parent',h.Axes);
            
            %get the right edge
            set(tool.handles.Status,'String','Click on Right-most edge of the lesion')
            k = waitforbuttonpress;
            bp=round(get(h.Axes,'CurrentPoint'));
            lims(1,2)=bp(1,1);
            h_temp(2)=scatter(bp(1,1),bp(1,2),'or','filled','Parent',h.Axes);
            
            %get the top edge
            set(tool.handles.Status,'String','Click on Top-most edge of the lesion')
            k = waitforbuttonpress;
            bp=round(get(h.Axes,'CurrentPoint'));
            lims(2,1)=bp(1,2);
            h_temp(3)=scatter(bp(1,1),bp(1,2),'or','filled','Parent',h.Axes);
            
            %get the bottom edge
            set(tool.handles.Status,'String','Click on Bottom-most edge of the lesion')
            k = waitforbuttonpress;
            bp=round(get(h.Axes,'CurrentPoint'));
            lims(2,2)=bp(1,2);
            h_temp(4)=scatter(bp(1,1),bp(1,2),'or','filled','Parent',h.Axes);
            
            %Get the lowest slice
            set(tool.handles.Status,'String','Scroll to LOWEST slice and click');
            k = waitforbuttonpress;
            bp=round(get(h.Axes,'CurrentPoint'));
            lims(3,1)=getCurrentSlice(tool.handles.imtool_CT);
            h_temp(5)=scatter(bp(1,1),bp(1,2),'or','filled','Parent',h.Axes);
            
            %Get the highest slice
            set(tool.handles.Status,'String','Scroll to HIGHEST slice and click');
            k = waitforbuttonpress;
            bp=round(get(h.Axes,'CurrentPoint'));
            lims(3,2)=getCurrentSlice(tool.handles.imtool_CT);
            h_temp(6)=scatter(bp(1,1),bp(1,2),'or','filled','Parent',h.Axes);
            
            delete(h_temp);
            
            %Rearange lims
            lims = [lims(2,:); lims(1,:); lims(3,:)];
            
            %Set the status string
            set(tool.handles.Status,'String',str_old);
            drawnow
        end
        
        function fitLesionToCTData(tool)
            notify(tool,'fittingLesion')
            
            %Get the mask
            mask = getMask(tool.handles.imtool_CT);
            CC = bwconncomp(mask);
            if CC.NumObjects>1
                showErrorMessage(tool,'Warning, multiple regions found in mask, using the largest region for fit')
                for i=1:CC.NumObjects
                    num(i) = numel(CC.PixelIdxList{i});
                end
                [~,ind]=max(num);
            else
                ind=1;
            end
            mask = false(size(mask)); mask(CC.PixelIdxList{ind})=true;
            
            %get the image values
            buff = get(tool.handles.FitBufferEdit,'UserData');
            lims = getMaskBoundingBox(mask,buff);
            mask = mask(lims(1,1):lims(1,2),lims(2,1):lims(2,2),lims(3,1):lims(3,2));
            im = getImageValues(tool.handles.imtool_CT,lims);
            
            %Get the MTF and NPS objects if needed
            if get(tool.handles.FitMTFCheck,'Value')
                MTF.x=tool.MTF_xy;
                MTF.y=tool.MTF_xy;
                MTF.z=tool.MTF_z;
            else
                MTF=[];
            end
            if get(tool.handles.FitNPSCheck,'Value')
                NPS=tool.NPS;
            else
                NPS=[];
            end
            
            %perform the fit
            lesion = fitModelToCTData(tool.lesion,im,mask,tool.CTInfo.psize,'MTF',MTF,'NPS',NPS);
            lesion.profile.C=round(lesion.profile.C);
            
            %update the lesion in the gui
            setUpdatedLesion(tool,lesion)
            
            notify(tool,'doneFittingLesion')
        end
        
        function loadNewMesh(tool,fv)
            tool.handles.meshViewer.fv=fv;
            
        end
        
        function loadCTdata(tool,input,format,varargin)
            if input
                switch format
                    case 'DICOMs'
                        CTInfo.format = 'DICOMs';
                        notify(tool,'readingCTData')
                        try
                            [I, info]=readCTSeries(input);
                            CTInfo.S = size(I);
                            [x,y,z]=getCTcoordinates(info,format);
                            sliceInt = abs(diff(z)); sliceInt=max(sliceInt);
                            CTInfo.psize =[info(1).PixelSpacing' info(1).SliceThickness];
                            CTInfo.pspacing= [info(1).PixelSpacing' sliceInt];
                            CTInfo.FOV=CTInfo.S.*CTInfo.pspacing;
                            CTInfo.Origin = info(1).ImagePositionPatient';
                            CTInfo.fname = input;
                            CTInfo.Headers = info;
                            
                            CTInfo.x=x; CTInfo.y=y; CTInfo.z=z;
                            notify(tool,'readCTData')
                            try
                                W = CTInfo.Headers(1).WindowWidth(1);
                                L = CTInfo.Headers(1).WindowCenter(1);
                            catch
                                W = max(I(:)) - min(I(:));
                                L = 0;
                            end
                        catch ME
                            notify(tool,'readCTData')
                            return
                        end
                        
                    case '.ipv'
                        
                        notify(tool,'readingCTData')
                        try
                            [I,CTInfo] = importIPIPEVolume(input);
                            CTInfo.format = '.ipv';
                            CTInfo.S=size(I);
                            [x,y,z]=getCTcoordinates(CTInfo,format);
                            CTInfo.x=x; CTInfo.y=y; CTInfo.z=z;
                            range = [min(I(:)) max(I(:))];
                            W = diff(range); L = mean(range);
                            
                            
                            
                            notify(tool,'readCTData')
                        catch
                            notify(tool,'readCTData')
                            return
                        end
                    case '.raw'
                        notify(tool,'readingCTData')
                        %Getting meta data
                        S=varargin{1}; %array dimensions
                        d=varargin{2}; %voxel spacing
                        precision=varargin{3}; %data type
                        skip=varargin{4}; %Number of bytes to skip;
                        %Reading file
                        id = fopen(input,'r');
                        fseek(id,skip,'bof');
                        I = reshape(fread(id,prod(S),precision),S);
                        fclose(id);
                        x=(0:S(1)-1)*d(1);
                        y=(0:S(2)-1)*d(2);
                        z=(0:S(3)-1)*d(3);
                        
                        I = permute(I,[2 1 3]);
                        S=[S(2) S(1) S(3)];
                        d=[d(2) d(1) d(3)];
                        
                        CTInfo.format='.raw';
                        CTInfo.x=x;
                        CTInfo.y=y;
                        CTInfo.z=z;
                        CTInfo.S=S;
                        CTInfo.type=precision;
                        CTInfo.FOV=S.*d;
                        CTInfo.psize=d;
                        CTInfo.pspacing=d;
                        CTInfo.fname=input;
                        CTInfo.Origin=[0,0,0];
                        
                        range = [min(I(:)) max(I(:))];
                        W = diff(range); L = mean(range);
                        notify(tool,'readCTData')
                        
                        
                        
                        
                        
                        
                end
                
                %Set the CT image
                setImage(tool.handles.imtool_CT, I)
                
                %Set the window and level
                setWindowLevel(tool.handles.imtool_CT,W,L)
                
                %Set the CT image info
                tool.CTInfo = CTInfo;
            else
                notify(tool,'readCTData')
            end
        end
        
        function insertLesionAtLocation(tool,location,locationFlag)
            switch locationFlag
                case 'Physical'
                    location = findClosestVoxelIndex(tool,location);
                case 'Pixel'
            end
            
            %Move image to target slice
            setCurrentSlice(tool.handles.imtool_CT,location(3))
            
            %Move insertion location to target position
            tool.handles.InsertionPoint.position=location(1:2);
            
            %perform insertion
            insertLesion(tool)
            
            
        end
        
        function pixelLocation = findClosestVoxelIndex(tool,physicalLocation)
            %This finds the closes voxel relative to a given voxel location
            x = physicalLocation(1); y = physicalLocation(2); z = physicalLocation(3);
            
            [~,indx] = min(abs(x-tool.CTInfo.x));
            [~,indy] = min(abs(y-tool.CTInfo.y));
            [~,indz] = min(abs(z-tool.CTInfo.z));
            
            pixelLocation = [indx indy,indz];
        end
        
        function loaded = get.CTDataLoaded(tool)
            loaded = ~isempty(tool.CTInfo.S);  
        end
        
        function runCallback(tool,hObject,label)
            callbacks(hObject,[],tool,label)
        end
        
        function set.Visible(tool,Visible)
            tool.Visible=Visible;
            if Visible
                set(tool.handles.fig,'Visible','on');
            else
                set(tool.handles.fig,'Visible','off');
            end
        end
        
        function available = isOptionalRepoAvailable(tool,repoName)
	   % Commented out 11/22/2024 -- error checking method no longer works
            % Replaced with search path check for the correct folder, but not directly connected to git lab repo
            %This function checks if an optional repository is available
            %ind = find(strcmp(tool.optionalRepositories,repoName));
            %if ind
            %    if ~isempty(tool.gitSHA1CheckSums_optional{ind})
            %        available = true;
            %   else
            %        available = false;
            %    end
            %else
            %    available = false;
            %end
            
	   %This function checks if an optional repository is available
            % Get all paths in the MATLAB search path
            check_search_path = strsplit(path, pathsep);

            % Find matches
            match = any(contains(check_search_path, repoName));

            if ~match
                available = false;
            else
                available = true;
            end

        end
        
        
    end
    
end

function new = checkEditBoxValue(hObject,checkType)
%Get the old value
old = get(hObject,'UserData');
%get the new value
new = str2num(get(hObject,'String'));
if isempty(new)
    new = old;
end

switch checkType
    case 'GreaterThanZero'
        if new<=0 || ~isreal(new)
            new = old;
        end
    case 'GreaterThanZeroInt'
        new=round(new);
        if new<0 || ~isreal(new)
            new = old;
        end
    case 'AnyRealValue'
        if ~isreal(new)
            new = old;
        end
end
set(hObject,'String',num2str(new));
set(hObject,'UserData',new);
end

function callbacks(hObject,evnt,tool,label)
%This function handles all the GUI callbacks
switch label
    
    case 'FitNPSButton'
        %Have the user draw an ROI
        h = getHandles(tool.handles.imtool_CT);
        ROI = imtool3DROI_rect(h.I);
        %Get the current slice
        slice = getCurrentSlice(tool.handles.imtool_CT);
        %Get the number of slices
        nSlices = get(tool.handles.FitNPSNSlices,'UserData');
        %Fit the NPS
        fitNPStoCTData(tool,ROI,slice,nSlices);
        %delete the ROI
        delete(ROI);
    
    case 'GenerateCTNoiseButton'
        
        generateCTNoise(tool);
    
    case 'MeasureCTNoiseButton'
        
        measureCTNoise(tool);
    
    case 'MTFz_EFF_Check'
        val=logical(get(hObject,'Value'));
        tool.MTF_z.withEFF=val;
    case 'MTFz_s_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.MTF_z.s=new;
        
    case 'MTFz_h_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.MTF_z.h=new;
        
    case 'MTFz_c_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.MTF_z.c=new;
        
    case 'MTFz_m_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.MTF_z.m=new;
    
    case 'MTFxy_EFF_Check'
        val=logical(get(hObject,'Value'));
        tool.MTF_xy.withEFF=val;
    case 'MTFxy_s_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.MTF_xy.s=new;
        
    case 'MTFxy_h_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.MTF_xy.h=new;
        
    case 'MTFxy_c_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.MTF_xy.c=new;
        
    case 'MTFxy_m_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.MTF_xy.m=new;
        
    case 'NPS_noise_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        setNoise(tool.NPS,new);
        
    case 'NPS_b_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.NPS.b=new;
        
    case 'NPS_c_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.NPS.c=new;
        
    case 'NPS_d_Edit'
        new = checkEditBoxValue(hObject,'GreaterThanZero');
        tool.NPS.d=new;
        
    case 'FillHolesButton'
        mask = getMask(tool.handles.imtool_CT);
        mask = imfill(mask,'holes');
        setMask(tool.handles.imtool_CT,mask)
        
    case 'BinaryErodeButton'
        mask = getMask(tool.handles.imtool_CT);
        SE = strel(ones(3,3,3));
        mask = imerode(mask,SE);
        setMask(tool.handles.imtool_CT,mask)
        
    case 'BinaryDilateButton'
        mask = getMask(tool.handles.imtool_CT);
        SE = strel(ones(3,3,3));
        mask = imdilate(mask,SE);
        setMask(tool.handles.imtool_CT,mask)
        
    case 'AutoSegmentButton'
        %Get the ROI
        lims = getUserDefinedBox(tool);
        im = getImageValues(tool.handles.imtool_CT,lims);
        
        %show the central slice
        slice = lims(3,1)+ceil(diff(lims(3,:)/2));
        setCurrentSlice(tool.handles.imtool_CT,slice)
        
        %perform k-means segmentation
        [W,L] = getWindowLevel(tool.handles.imtool_CT);
        lmask = segmentLesionKmeans(im,tool.CTInfo.psize,W,L);
        
        %make a full-sized empty mask
        mask=false(getImageSize(tool.handles.imtool_CT));
        
        %add lmask to full mask
        mask(lims(1,1):lims(1,2),lims(2,1):lims(2,2),lims(3,1):lims(3,2))=lmask;
        
        %Set the mask
        setMask(tool.handles.imtool_CT,mask)
        
        
    case 'ClearMaskButton'
        
        if ~isempty(tool.CTInfo.S)
            mask = false(tool.CTInfo.S);
            setMask(tool.handles.imtool_CT,mask);
        end
        
    case 'ExportMaskButton'
        
        mask = getMask(tool.handles.imtool_CT);
        [FileName,PathName] = uiputfile('*.mat');
        save([PathName FileName], 'mask')
        
    case 'LoadMaskButton'
        
        [FILENAME, PATHNAME, FILTERINDEX] = uigetfile('*.mat', 'Select a mask file');
        load([PATHNAME FILENAME]);
        
        if exist('mask') && ~isempty(tool.CTInfo.S) && sum(size(mask) == tool.CTInfo.S)==3
            if islogical(mask)
                setMask(tool.handles.imtool_CT,mask);
            end
        end
    case 'LoadDICOMMaskButton'
        input=uigetdir(pwd,'Select the folder containing the DICOMs');
        [mask, info]=readCTSeries(input);
        mask = logical(mask);
        setMask(tool.handles.imtool_CT,mask);
        
    case 'FitBufferEdit'
        new = checkEditBoxValue(hObject,'GreaterThanZeroInt');
        
    case 'FitLesionButton'
        fitLesionToCTData(tool);
        
    case 'PartialTextEdit'
        new = checkEditBoxValue(hObject,'GreaterThanZeroInt');
        
    case 'Check_PartialVolume'
        
        if get(hObject,'Value')
            set(tool.handles.ParitalTextEdit,'Visible','on')
        else
            set(tool.handles.ParitalTextEdit,'Visible','off')
        end
        
    case 'InsertLesionButton'
        insertLesion(tool)
        
    case 'RemoveInsertedLesionButton'
        deleteInsertedLesion(tool)
        
    case 'ExportInsertedLesionsButton'
        lesions = tool.insertedLesion;
        [PATHSTR,NAME,EXT] = fileparts(tool.CTInfo.fname);
        NAME = [NAME EXT];
        try
            lesionTable = evalin('base', 'lesionTable');
        catch
            lesionTable=[];
        end
        for i=1:length(lesions)
            ind = length(lesionTable)+1;
            lesionTable(ind).fname = NAME;
            lesionTable(ind).Position=lesions(i).Position;
            lesionTable(ind).Position_mm=lesions(i).Position_mm;
        end
        assignin('base', 'lesionTable', lesionTable)
        insertionTable=struct2table(lesionTable);
        save('~/Desktop/InsertionPositions.mat','insertionTable');
        
    case 'InsertedList'
        
    case 'Check_InsertionVisible'
        
        if get(hObject,'Value')
            tool.handles.InsertionPoint.visible='on';
            n = get(tool.handles.Pulldown_insertionType,'Value');
            method = get(tool.handles.Pulldown_insertionType,'String');
            method = method{n};
            switch method
                case 'Superposition'
                    tool.handles.NoiseInsertionPoint.visible='off';
                    
                case 'Alpha Blending'
                    tool.handles.NoiseInsertionPoint.visible='on';
            end
        else
            tool.handles.InsertionPoint.visible='off';
            tool.handles.NoiseInsertionPoint.visible='off';
        end
        
        
        
    case 'Pulldown_insertionType'
        n = get(hObject,'Value');
        method = get(hObject,'String');
        method = method{n};
        switch method
            case 'Superposition'
                tool.handles.NoiseInsertionPoint.visible='off';
                
            case 'Alpha Blending'
                tool.handles.NoiseInsertionPoint.visible='on';
        end
        %This updates the string showin the insertion location
        tool.handles.InsertionPoint.position=tool.handles.InsertionPoint.position;
        
    case 'Pulldown_imageType'
        
    case 'ImageInfoButton'
        switch tool.CTInfo.format
            
            case 'DICOMs'
                %Get the dicom header for the current slice
                slice = getCurrentSlice(tool.handles.imtool_CT);
                info = tool.CTInfo.Headers(slice);
                imageinfo(info);
            case '.ipv'
                %Create a new figure
                f = figure('menu','none','toolbar','none','name','XML file');
                T = xmlwrite(tool.CTInfo.xml);
                uicontrol(f,'style','listbox','Units','normalized','position',[0 0 1 1],'String',T);
        end
        
    case 'ExportCTDataButton'
        
        switch tool.CTInfo.format
            case 'DICOMs'
                oldDir = pwd;
                cd(tool.CTInfo.fname)
                cd ..
                PATHSTR = pwd;
                cd(oldDir);
                output = uigetdir(PATHSTR,'Select a folder to export DICOMS');
                
                
            case '.ipv'
                [PATHSTR,NAME,EXT] = fileparts(tool.CTInfo.fname);
                [FileName,PathName] = uiputfile({[PATHSTR filesep '*.ipv']},'Select a file name');
                if FileName
                    output = [PathName FileName];
                else
                    output=false;
                end
            otherwise
                showErrorMessage(tool,'Error exporting CT data, data type not supported')
                output=false;
        end
        exportImage(tool,output);
        
    case 'Pulldown_exportType'
        
    case 'LoadCTDataButton'
        
        %Get the format of the data to be loaded
        n = get(tool.handles.Pulldown_imageType,'Value');
        format = get(tool.handles.Pulldown_imageType,'String');
        format=format{n};
        switch format
            %get the folder or filename (depends on input)
            case 'DICOMs'
                input=uigetdir(pwd,'Select the folder containing the DICOMs');
            case '.ipv';
                
                [filename, pathname] = uigetfile('*.ipv', 'Select a .ipv file');
                if filename
                    input = [pathname filename];
                else
                    input = false;
                end
        end
        %load the CT data
        loadCTdata(tool,input,format)
        
    case 'LibraryList'
        filteredLesionTable = tool.filteredLesionTable;
        if size(filteredLesionTable,1)==0
            n=0;
        else
            n = get(tool.handles.name_List,'Value');
            if n>size(filteredLesionTable,1)
                n = size(filteredLesionTable,1);
            elseif n==0
                n=1;
            end
        end
        names = filteredLesionTable.name;
        set(tool.handles.name_List,'String',names,'Value',n)
        ind = tool.lesionTableIndex;
        T = getTextFromLesionTable(tool,ind);
        %set(tool.handles.LesionInfo_text,'String',T)
        set(tool.handles.LesionInfo_text,'String',tool.paths.SavedLesions)
        
    case 'LoadLesionButton'
        
        ind = tool.lesionTableIndex;
        if ind >0
            fname = [tool.paths.SavedLesions tool.lesionTable.FileName{ind}];
            load(fname)
            setUpdatedLesion(tool,lesion)
        end
        
    case 'SaveLesionButton'
        
        %contruct the file name
        type = get(tool.handles.LesionTypeEdit,'String');
        tag = get(tool.handles.LesionTagEdit,'String');
        name = get(tool.handles.LesionNameEdit,'String');
        fname = [type '_' tag '_' name '.mat'];
        
        %get the lesion object
        lesion = tool.lesion;
        
        %change the type tag and name of the lesion
        lesion.type = type;
        lesion.tag = tag;
        lesion.name = name;
        
        %save the object to the saved lesions folder
        save([tool.paths.SavedLesions fname],'lesion');
        
        %update the lesionTable
        tool.lesionTable = createLesionTable(tool);
        
        %update the lists according to the newly save lesion
        updateLibraryLists(tool)
        
    case 'RefreshTextureButton'
        tool.lesion.texture.positions=[];
        updateLesion(tool)
        
    case 'CLBTextEdit'  %Use this for K and Sigma
        checkEditBoxValue(hObject,'GreaterThanZero');
        notify(tool,'TexturePropertyChanged')
        notify(tool,'LesionPropertyChanged')
        
        
    case 'CLBTextEdit_C'  %Use this for the texture contrast
        checkEditBoxValue(hObject,'AnyRealValue');
        notify(tool,'TexturePropertyChanged')
        notify(tool,'LesionPropertyChanged')
        
    case 'CLBTextEdit_N'  %Use this for N
        checkEditBoxValue(hObject,'GreaterThanZeroInt');
        notify(tool,'TexturePropertyChanged')
        notify(tool,'LesionPropertyChanged')
        
    case 'Pulldown_textureType'
        str = get(hObject,'String'); n = get(hObject,'Value'); str = str{n};
        switch str
            case 'None'
                set(tool.handles.ClusteredLumpyPanel,'Visible','off')
            case 'Clustered Lumpy Background'
                set(tool.handles.ClusteredLumpyPanel,'Visible','on')
        end
        notify(tool,'TexturePropertyChanged')
        notify(tool,'LesionPropertyChanged')
        
    case 'randomButton'
        %get the values in the random table
        data = get(tool.handles.RandomTable,'Data');
        
        %Generate the lesion
        lesion = generateRandomLesionModel(data);
        
        %Push the new lesion to the tool
        setUpdatedLesion(tool,lesion)
        
    case 'Pulldown_randomType'
        str = get(hObject,'String'); n = get(hObject,'Value'); str = str{n};
        switch str
            case 'Custom'
                data = [5 3 7 1;10 0 20 5;100 50 150 25;1 0 2 .5;1 0 2 .5];
                set(tool.handles.RandomTable,'Data',data)
            case 'Liver Lesion'
                data = [8 0 20 2.25; 20 0 100 5; -80 -200 -10 25; .1 0 .2 0; .85 0 2 .2];
                set(tool.handles.RandomTable,'Data',data)
            case 'Lung Nodule'
                data = [4 0 20 1.75; 10 0 100 4.5; 1000 10 2000 300; .75 0 2 .5; .35 0 2 0];
                set(tool.handles.RandomTable,'Data',data)
            case 'Renal Stone'
                data = [2 0 4 .75; 30 0 100 7.5; 1000 10 2000 500; .5 0 3 .3; .1 0 2 0];
                set(tool.handles.RandomTable,'Data',data)
        end
        
    case 'LoadMeshShapeButton'
        %read in the mesh file
        try
            file = get(tool.handles.MeshShapeTextEdit,'String');
            [~, name, ext]=fileparts(file);
            [F,V]=stlread(file);
            fv.faces=F; fv.vertices=V;
            loadNewMesh(tool,fv)
            
        catch
            showErrorMessage(tool,['Error reading' name ext]);
        end
        
        
    case 'Pulldown_shapeType'
        str = get(hObject,'String'); n = get(hObject,'Value'); str = str{n};
        switch str
            case 'Spoke'
                set(tool.handles.SpokeShapeTable,'Visible','On')
                set(tool.handles.PlusShapeButton,'Visible','On')
                set(tool.handles.MultiplyShapeButton,'Visible','On')
                set(tool.handles.MultiplyTextEdit,'Visible','On')
                set(tool.handles.ShapeFilterTextEdit,'Visible','On')
                set(tool.handles.ShapeFilterText,'Visible','On')
                set(tool.handles.MeshShapeTextEdit,'Visible','Off')
                set(tool.handles.MeshShapeButton,'Visible','Off')
                set(tool.handles.LoadMeshShapeButton,'Visible','Off')
                tool.handles.meshViewer.visible='Off';
                
                notify(tool,'LesionPropertyChanged')
            case 'Simple Mesh'
                set(tool.handles.SpokeShapeTable,'Visible','Off')
                set(tool.handles.PlusShapeButton,'Visible','Off')
                set(tool.handles.MultiplyShapeButton,'Visible','Off')
                set(tool.handles.MultiplyTextEdit,'Visible','Off')
                set(tool.handles.ShapeFilterTextEdit,'Visible','Off')
                set(tool.handles.ShapeFilterText,'Visible','Off')
                set(tool.handles.MeshShapeTextEdit,'Visible','On')
                set(tool.handles.MeshShapeButton,'Visible','On')
                set(tool.handles.LoadMeshShapeButton,'Visible','On')
                tool.handles.meshViewer.visible='On';
                
                %Set the profile to flat
                str = get(tool.handles.Pulldown_profileType,'String');
                val = get(tool.handles.Pulldown_profileType,'Value');
                ind = find(strcmp('Flat',str));
                if ~(ind==val)
                    showErrorMessage(tool,'Warning, switching the profile type to ''Flat'' ')
                    set(tool.handles.Pulldown_profileType,'Value',ind);
                    callbacks(tool.handles.Pulldown_profileType,[],tool,'Pulldown_profileType')
                else
                    notify(tool,'LesionPropertyChanged')
                end
            case 'Mesh'
                set(tool.handles.SpokeShapeTable,'Visible','Off')
                set(tool.handles.PlusShapeButton,'Visible','Off')
                set(tool.handles.MultiplyShapeButton,'Visible','Off')
                set(tool.handles.MultiplyTextEdit,'Visible','Off')
                set(tool.handles.ShapeFilterTextEdit,'Visible','Off')
                set(tool.handles.ShapeFilterText,'Visible','Off')
                set(tool.handles.MeshShapeTextEdit,'Visible','On')
                set(tool.handles.MeshShapeButton,'Visible','On')
                set(tool.handles.LoadMeshShapeButton,'Visible','On')
                tool.handles.meshViewer.visible='On';
                notify(tool,'LesionPropertyChanged')
        end
        
        
    case 'Button_meshPath'
        p = get(tool.handles.MeshShapeTextEdit,'String');
        [root, ~, ~] = fileparts(p);
        if exist(root)
            searchName = fullfile(root,'*.stl');
        else
            searchName = fullfile(tool.root,'*.stl');
        end
        [FileName,PathName,FilterIndex] = uigetfile(searchName);
        if FileName
            editString = fullfile(PathName,FileName);
            set(tool.handles.MeshShapeTextEdit,'String',editString,'UserData',editString);
        end
        
    case 'Edit_meshPath'
        %check if the new file exists
        oldFile = get(hObject,'UserData');
        file = get(hObject,'String');
        if exist(file)
            set(hObject,'String',file,'UserData',file);
        else
            set(hObject,'String',oldFile,'UserData',oldFile);
        end
        
        
    case 'Button_LesionRefresh'
        updateLesion(tool)
        
    case 'PlusShapeButton'
        val = str2num(get(tool.handles.MultiplyTextEdit,'String'));
        data = get(tool.handles.SpokeShapeTable,'Data');
        data_new = data+val;
        ind = data_new <=0;
        data_new(ind) = data(ind);
        set(tool.handles.SpokeShapeTable,'Data',data_new);
        notify(tool,'LesionPropertyChanged')
        
    case 'MultiplyShapeButton'
        val = str2num(get(tool.handles.MultiplyTextEdit,'String'));
        data = get(tool.handles.SpokeShapeTable,'Data');
        data_new = data*val;
        ind = data_new <=0;
        data_new(ind) = data(ind);
        set(tool.handles.SpokeShapeTable,'Data',data_new);
        notify(tool,'LesionPropertyChanged')
        
    case 'MultiplyTextEdit'
        checkEditBoxValue(hObject,'AnyRealValue');
        
    case 'RandomTableEdit'
        rowNames = get(hObject,'RowName');
        i = evnt.Indices(1); j = evnt.Indices(2);
        data = get(hObject,'Data');
        new = evnt.NewData;
        old = evnt.PreviousData;
        if isnan(new) || ~isreal(new)
            new = old;
        end
        switch rowNames{i}
            case 'Mean Radius'
                if new <=0 && j~=4
                    new = old;
                end
            case 'Radius COV'
                if new<0
                    new = old;
                end
            case 'Peak Contrast'
            case 'n'
                if new <0
                    new = old;
                end
            case 'Edge Blur'
                if new <0
                    new = old;
                end
        end
        
        switch j %columns
            case 1 %Mean
                if new < data(i,2) || new > data(i,3)   %make sure mean is between the min and max
                    new = old;
                end
            case 2 %Min
                if new > data(i,1) || new > data(i,3)   %make sure min is less than mean and max
                    new = old;
                end
            case 3 %Max
                if new < data(i,1) || new < data(i,2)   %make sure max is greater than mean and min
                    new = old;
                end
            case 4 %STD
                if new <0
                    new = old;
                end
        end
        
        data(i,j)=new;
        set(hObject,'Data',data)
        
    case 'ShapeTableEdit'
        i = evnt.Indices(1); j = evnt.Indices(2);
        data = get(hObject,'Data');
        if isnan(evnt.NewData) || ~isreal(evnt.NewData)
            data(i,j) = evnt.PreviousData;
        end
        if i==1 || i==size(data,1)
            data(i,:) = data(i,j);
        end
        set(hObject,'Data',data)
        notify(tool,'LesionPropertyChanged')
        
    case 'ShapeFilterTextEdit'
        checkEditBoxValue(hObject,'GreaterThanZero');
        notify(tool,'LesionPropertyChanged')
        
    case 'Pulldown_profileType'
        str = get(hObject,'String'); n = get(hObject,'Value'); str = str{n};
        switch str
            case 'Designer'
                set(tool.handles.DesignerNodulePanel,'Visible','On')
                set(tool.handles.ProfileText,'String','$$C = C_p \left( 1- \left( \frac{r}{R_{\theta , \phi }} \right)^2 \right)^n$$')
            case 'Flat'
                set(tool.handles.DesignerNodulePanel,'Visible','Off')
                set(tool.handles.ProfileText,'String','$$C = C_p \left( H(r-R_{\theta, \phi})  \right)$$')
                
        end
        notify(tool,'LesionPropertyChanged')
        
    case 'PeakContrastTextEdit'
        checkEditBoxValue(hObject,'AnyRealValue');
        notify(tool,'LesionPropertyChanged')
        
    case 'IntegerGreaterThanZeroTextEdit'
        checkEditBoxValue(hObject,'GreaterThanZeroInt');
    case 'GreaterThanZeroTextEdit'
        checkEditBoxValue(hObject,'GreaterThanZero');
    case 'nTextEdit'
        checkEditBoxValue(hObject,'GreaterThanZero');
        notify(tool,'LesionPropertyChanged')
        
    case 'Pulldown_blurType'
        str = get(hObject,'String'); n = get(hObject,'Value'); str = str{n};
        switch str
            case 'Gaussian';
                set(tool.handles.GaussianBlurPanel,'Visible','On')
            case 'None'
                set(tool.handles.GaussianBlurPanel,'Visible','Off')
                
        end
        notify(tool,'LesionPropertyChanged')
        
    case 'BlurTextEdit'
        checkEditBoxValue(hObject,'GreaterThanZero');
        notify(tool,'LesionPropertyChanged')
        
    case 'FOVTextEdit'
        checkEditBoxValue(hObject,'GreaterThanZero');
        notify(tool,'LesionPropertyChanged')
        
    case 'NTextEdit'
        checkEditBoxValue(hObject,'GreaterThanZeroInt');
        notify(tool,'LesionPropertyChanged')
        
    case 'ScaleEdit'
        checkEditBoxValue(hObject,'GreaterThanZeroInt');
        notify(tool,'LesionPropertyChanged')
        
    case 'Pulldown_interpType'
        notify(tool,'LesionPropertyChanged')
end
end

function panelResizeFunction(hObject,events,tool,buff,label)

switch label
    
    case 'Noise'
        pos = getPixelPosition(tool.handles.NoiseAdditionTab);
        set(tool.handles.MeasureCTNoiseButton,'Position',[buff pos(4)-2*buff 5*buff buff])
        set(tool.handles.GenerateCTNoiseButton,'Position',[buff pos(4)-4*buff 5*buff buff])
        set(tool.handles.FitNPSButton,'Position',[buff pos(4)-3*buff 5*buff buff])
        set(tool.handles.FitNPSNSlices,'Position',[6*buff pos(4)-3*buff buff buff])
        set(tool.handles.FitNPSNSlicesLabel,'Position',[7.5*buff pos(4)-3*buff 3*buff buff])
        set( tool.handles.DoseReductionSliderLabel,'Position',[buff pos(4)-5.5*buff (pos(3)/2)-4*buff buff])
        set(tool.handles.DoseReductionSlider,'Position',[buff pos(4)-6.5*buff (pos(3)/2)-4*buff buff])
        set(tool.handles.DoseReductionText,'Position',[(pos(3)/2)-4*buff pos(4)-5.5*buff 2*buff buff])
        set(tool.handles.NoiseAdditionBeta,'Position',[buff pos(4)-7.5*buff buff buff]);
        set(tool.handles.NoiseAdditionBetaLabel,'Position',[2*buff pos(4)-7.5*buff buff buff]);
        set(tool.handles.noiseBetaAxis,'Position',[2*buff 1.5*buff pos(3)/2-3*buff pos(4)-9.5*buff]);
        
        
    
    case 'Physics'
        pos = getPixelPosition(tool.handles.PhysicsTab);
        set(tool.handles.NPSPanel,'Position',[buff pos(4)-4*buff 4*buff 3*buff])
        set(tool.handles.MTFxyPanel,'Position',[5*buff pos(4)-4*buff 6*buff 3*buff])
        set(tool.handles.MTFzPanel,'Position',[11*buff pos(4)-4*buff 6*buff 3*buff])
        set(tool.handles.NPS_axis,'Position',[buff buff (pos(3)-2*buff)/3 pos(4)-6*buff])
        set(tool.handles.NPS_z_axis,'Position',[buff+(pos(3)-2*buff)/3 buff (pos(3)-2*buff)/3 pos(4)-6*buff])
        set(tool.handles.MTF_axis,'Position',[buff+2*(pos(3)-2*buff)/3 buff (pos(3)-2*buff)/3 pos(4)-6*buff])
        
    case 'LesionFit'
        pos = getPixelPosition(tool.handles.LesionFittingTab);
        set(tool.handles.FitLesionButton,'Position',[buff pos(4)-2*buff 5*buff buff])
        set(tool.handles.FitBufferEdit,'Position',[buff pos(4)-3*buff buff buff])
        set(tool.handles.FitBufferText,'Position',[2*buff pos(4)-3*buff 4*buff buff])
        
        set(tool.handles.FitMTFCheck,'Position',[buff pos(4)-4*buff 2*buff buff])
        set(tool.handles.FitNPSCheck,'Position',[3*buff pos(4)-4*buff 4*buff buff])
        
        set(tool.handles.LoadMaskButton,'Position',[buff pos(4)-5*buff 5*buff buff])
        set(tool.handles.LoadDICOMMaskButton,'Position',[buff pos(4)-6*buff 5*buff buff]);
        set(tool.handles.ExportMaskButton,'Position',[buff pos(4)-7*buff 5*buff buff])
        set(tool.handles.ClearMaskButton,'Position',[buff pos(4)-8*buff 5*buff buff])
        set(tool.handles.AutoSegmentButton,'Position',[buff pos(4)-9*buff 5*buff buff])
        set(tool.handles.BinaryDilateButton,'Position',[buff pos(4)-10*buff 5*buff buff])
        set(tool.handles.BinaryErodeButton,'Position',[buff pos(4)-11*buff 5*buff buff])
        set(tool.handles.FillHolesButton,'Position',[buff pos(4)-12*buff 5*buff buff])
        set(tool.handles.MaskInfo_text,'Position',[7*buff buff pos(3)-8*buff buff])
        
        tool.handles.LesionMeasurePanel.position = [7*buff/pos(3) (pos(4)-10*buff)/pos(4) 1-8*buff/pos(3) 9*buff/pos(4)];
        
    case 'LesionInsertion'
        pos = getPixelPosition(tool.handles.LesionInsertionTab);
        set(tool.handles.Pulldown_insertionType,'Position',[buff pos(4)-2*buff 5*buff buff])
        set(tool.handles.PartialCheckBox,'Position',[7*buff pos(4)-2*buff 4*buff buff])
        set(tool.handles.ParitalTextEdit,'Position',[11*buff pos(4)-2*buff buff buff])
        set(tool.handles.MTFCheckBox,'Position',[7*buff pos(4)-3*buff 3*buff buff])
        set(tool.handles.inserted_List,'Position',[buff 2*buff 5*buff pos(4)-6*buff])
        set(tool.handles.RemoveInsertedLesionButton,'Position',[buff buff buff buff])
        set(tool.handles.ExportInsertedLesionsButton,'Position',[2*buff buff buff buff])
        set(tool.handles.InsertedLesion_text,'Position',[7*buff 2*buff pos(3)-13*buff pos(4)-6*buff],'Visible','on')
        set(tool.handles.InsertLesionButton,'Position',[pos(3)-6*buff 3*buff 5*buff buff])
        set(tool.handles.InsertionPointVisibleCheckBox,'Position',[pos(3)-6*buff 2*buff 5*buff buff])
        set(tool.handles.InsertionLocation_text,'Position',[pos(3)-6*buff buff 5*buff buff])
        
        
    case 'CTImageInfo'
        pos = getPixelPosition(tool.handles.CTInfoTab);
        set(tool.handles.Pulldown_imageType,'Position',[buff pos(4)-2*buff 5*buff buff])
        set(tool.handles.LoadCTDataButton,'Position',[buff pos(4)-3*buff 5*buff buff])
        set(tool.handles.ImageInfo_text,'Position',[7*buff pos(4)/2 pos(3)-13*buff  pos(4)/2-buff])
        set(tool.handles.ImageInfoButton,'Position',[pos(3)-6*buff pos(4)-2*buff 5*buff  buff])
        set(tool.handles.Pulldown_exportType,'Position',[buff pos(4)-5*buff 5*buff buff])
        set(tool.handles.ExportCTDataButton,'Position',[buff pos(4)-6*buff 5*buff buff])
        
        
    case 'Library'
        pos = getPixelPosition(tool.handles.LibraryTab);
        w = (pos(3)-4*buff)/3;
        set(tool.handles.type_List,'Position',[buff buff w pos(4)/2])
        set(tool.handles.type_List_text,'Position',[buff pos(4)/2+buff w buff])
        set(tool.handles.tag_List,'Position',[2*buff+w buff w pos(4)/2])
        set(tool.handles.tag_List_text,'Position',[2*buff+w pos(4)/2+buff w buff])
        set(tool.handles.name_List,'Position',[3*buff+2*w 2*buff w pos(4)/2-buff])
        set(tool.handles.name_List_text,'Position',[3*buff+2*w pos(4)/2+buff w buff])
        set(tool.handles.LoadLesionButton,'Position',[3*buff+2*w buff w buff])
        set(tool.handles.SaveLesionButton,'Position',[buff pos(4)-2*buff w buff])
        set(tool.handles.LesionTypeEdit,'Position',[buff pos(4)-3*buff w buff])
        set(tool.handles.LesionTagEdit,'Position',[buff pos(4)-4*buff w buff])
        set(tool.handles.LesionNameEdit,'Position',[buff pos(4)-5*buff w buff])
        set(tool.handles.LesionInfo_text,'Position',[3*buff+2*w pos(4)-5*buff w 4*buff])
        
        
    case 'Texture'
        pos = getPixelPosition(tool.handles.TextureTab);
        set(tool.handles.Pulldown_textureType,'Position',[buff pos(4)-2*buff 5*buff buff])
        set(tool.handles.ClusteredLumpyPanel,'Position',[buff pos(4)-6*buff pos(3)-2*buff 4*buff])
        %set(tool.handles.RefreshTextureButton,'Position',[buff pos(4)-6*buff 5*buff buff])
        
    case 'Random'
        pos = getPixelPosition(tool.handles.RandomTab);
        set(tool.handles.Pulldown_randomType,'Position',[buff pos(4)-2*buff 5*buff buff])
        set(tool.handles.RandomTable,'Position',[buff pos(4)-2*buff-4*buff pos(3)-2*buff  4*buff])
        set(tool.handles.randomButton,'Position',[buff pos(4)-8*buff 5*buff buff])
        
    case 'SizeShape'
        pos = getPixelPosition(tool.handles.ShapeTab);
        set(tool.handles.Pulldown_shapeType,'Position',[buff pos(4)-2*buff 5*buff buff])
        set(tool.handles.PlusShapeButton, 'Position',[pos(3)-4*buff pos(4)-2*buff buff buff])
        set(tool.handles.MultiplyShapeButton, 'Position',[pos(3)-3*buff pos(4)-2*buff buff buff])
        set(tool.handles.MultiplyTextEdit, 'Position',[pos(3)-2*buff pos(4)-2*buff buff buff])
        set(tool.handles.SpokeShapeTable,'Position',[buff pos(4)-2*buff-7*buff pos(3)-2*buff  7*buff])
        set(tool.handles.ShapeFilterTextEdit,'Position',[buff pos(4)-2*buff-9*buff buff buff])
        set(tool.handles.ShapeFilterText,'Position',[2*buff pos(4)-2*buff-9*buff 5*buff buff])
        set(tool.handles.LoadMeshShapeButton,'Position',[buff pos(4)-4*buff 5*buff buff])
        set(tool.handles.MeshShapeTextEdit,'Position',[6*buff pos(4)-4*buff pos(3)-8*buff buff])
        set(tool.handles.MeshShapeButton,'Position',[pos(3)-2*buff pos(4)-4*buff buff buff])
        tool.handles.meshViewer.position=[buff buff pos(3)-2*buff pos(4)-6*buff];
        
    case 'Profile'
        pos = getPixelPosition(tool.handles.ProfileTab);
        set(tool.handles.Pulldown_profileType,'Position',[buff pos(4)-2*buff 5*buff buff])
        set(tool.handles.ProfileAxes,'Position',[1.5*buff/pos(3) 1.5*buff/pos(4) 1-2*(1.5*buff/pos(3)) .5])
        set(tool.handles.ProfileBackgroundAxes,'Xlim',[0 pos(3)],'Ylim',[0 pos(4)])
        set(tool.handles.ProfileText,'Position',[7*buff pos(4)-2*buff])
        set(tool.handles.DesignerNodulePanel,'Position',[buff pos(4)-4*buff pos(3)-2*buff 2*buff])
        set(tool.handles.PeakContrastTextEdit,'Position',[buff pos(4)-6*buff buff buff])
        set(tool.handles.PeakContrastText,'Position',[2*buff pos(4)-6*buff 5*buff buff])
        set(tool.handles.Pulldown_blurType,'Position',[buff pos(4)-8*buff 5*buff buff])
        set(tool.handles.GaussianBlurPanel,'Position',[buff pos(4)-10*buff pos(3)-2*buff 2*buff])
        
    case 'Voxelize'
        pos = getPixelPosition(tool.handles.VoxelizeTab);
        %Upscale interp type
        set(tool.handles.Pulldown_interpType,'Position',[buff pos(4)-2*buff 5*buff buff])
        %Upscale text box
        set(tool.handles.ScaleEdit,'Position',[buff pos(4)-3*buff buff buff])
        set(tool.handles.ScaleText,'Position',[2.5*buff pos(4)-3*buff 4*buff buff])
        %xyz text boxes
        set(tool.handles.xText,'Position',[buff pos(4)-5*buff buff buff])
        set(tool.handles.yText,'Position',[2*buff pos(4)-5*buff buff buff])
        set(tool.handles.zText,'Position',[3*buff pos(4)-5*buff buff buff])
        %FOV text boxes
        set(tool.handles.FOVxEdit,'Position',[buff pos(4)-6*buff buff buff])
        set(tool.handles.FOVyEdit,'Position',[2*buff pos(4)-6*buff buff buff])
        set(tool.handles.FOVzEdit,'Position',[3*buff pos(4)-6*buff buff buff])
        set(tool.handles.FOVText,'Position',[4.5*buff pos(4)-6*buff 5*buff buff])
        %N text boxes
        set(tool.handles.NxEdit,'Position',[buff pos(4)-7*buff buff buff])
        set(tool.handles.NyEdit,'Position',[2*buff pos(4)-7*buff buff buff])
        set(tool.handles.NzEdit,'Position',[3*buff pos(4)-7*buff buff buff])
        set(tool.handles.NText,'Position',[4.5*buff pos(4)-7*buff 5*buff buff])
        %Pixel size text
        set(tool.handles.psizeText,'Position',[buff pos(4)-10*buff pos(3)-2*buff 2*buff])
        
end
end

function pos = getPixelPosition(h)
oldUnits = get(h,'Units');
set(h,'Units','Pixels');
pos = get(h,'Position');
set(h,'Units',oldUnits);
end

function varargout = writeDicoms(output,I,info)

uid = dicomuid;

%Rescale and shift image data
slope=info(1).RescaleSlope;
intercept=info(1).RescaleIntercept;
I=int16(I./slope-intercept);

for i=1:size(I,3)
    
    %get the source dicom filename
    source=info(i).Filename;
    [pathstr, name, ext] = fileparts(source);
    
    info(i).SeriesInstanceUID=uid;
    
    %write the dicom file
    dicomwrite(I(:,:,i),[output '/' name ext],info(i))
    
end

if nargout ==1
    varargout{1} = uid;
end
end

function varargout = stlread(file)
% STLREAD imports geometry from an STL file into MATLAB.
%    FV = STLREAD(FILENAME) imports triangular faces from the ASCII or binary
%    STL file idicated by FILENAME, and returns the patch struct FV, with fields
%    'faces' and 'vertices'.
%
%    [F,V] = STLREAD(FILENAME) returns the faces F and vertices V separately.
%
%    [F,V,N] = STLREAD(FILENAME) also returns the face normal vectors.
%
%    The faces and vertices are arranged in the format used by the PATCH plot
%    object.

% Copyright 2011 The MathWorks, Inc.

if ~exist(file,'file')
    error(['File ''%s'' not found. If the file is not on MATLAB''s path' ...
        ', be sure to specify the full path to the file.'], file);
end

fid = fopen(file,'r');
if ~isempty(ferror(fid))
    error(lasterror); %#ok
end

M = fread(fid,inf,'uint8=>uint8');
fclose(fid);

[f,v,n] = stlbinary(M);
%if( isbinary(M) ) % This may not be a reliable test
%    [f,v,n] = stlbinary(M);
%else
%    [f,v,n] = stlascii(M);
%end

varargout = cell(1,nargout);
switch nargout
    case 2
        varargout{1} = f;
        varargout{2} = v;
    case 3
        varargout{1} = f;
        varargout{2} = v;
        varargout{3} = n;
    otherwise
        varargout{1} = struct('faces',f,'vertices',v);
end

end

function [F,V,N] = stlbinary(M)

F = [];
V = [];
N = [];

if length(M) < 84
    error('MATLAB:stlread:incorrectFormat', ...
        'Incomplete header information in binary STL file.');
end

% Bytes 81-84 are an unsigned 32-bit integer specifying the number of faces
% that follow.
numFaces = typecast(M(81:84),'uint32');
%numFaces = double(numFaces);
if numFaces == 0
    warning('MATLAB:stlread:nodata','No data in STL file.');
    return
end

T = M(85:end);
F = NaN(numFaces,3);
V = NaN(3*numFaces,3);
N = NaN(numFaces,3);

numRead = 0;
while numRead < numFaces
    % Each facet is 50 bytes
    %  - Three single precision values specifying the face normal vector
    %  - Three single precision values specifying the first vertex (XYZ)
    %  - Three single precision values specifying the second vertex (XYZ)
    %  - Three single precision values specifying the third vertex (XYZ)
    %  - Two unused bytes
    i1    = 50 * numRead + 1;
    i2    = i1 + 50 - 1;
    facet = T(i1:i2)';
    
    n  = typecast(facet(1:12),'single');
    v1 = typecast(facet(13:24),'single');
    v2 = typecast(facet(25:36),'single');
    v3 = typecast(facet(37:48),'single');
    
    n = double(n);
    v = double([v1; v2; v3]);
    
    % Figure out where to fit these new vertices, and the face, in the
    % larger F and V collections.
    fInd  = numRead + 1;
    vInd1 = 3 * (fInd - 1) + 1;
    vInd2 = vInd1 + 3 - 1;
    
    V(vInd1:vInd2,:) = v;
    F(fInd,:)        = vInd1:vInd2;
    N(fInd,:)        = n;
    
    numRead = numRead + 1;
end

end

function [F,V,N] = stlascii(M)
warning('MATLAB:stlread:ascii','ASCII STL files currently not supported.');
F = [];
V = [];
N = [];
end

function tf = isbinary(A)
% ISBINARY uses the first line of an STL file to identify its format.
if isempty(A) || length(A) < 5
    error('MATLAB:stlread:incorrectFormat', ...
        'File does not appear to be an ASCII or binary STL file.');
end
if strcmpi('solid',char(A(1:5)'))
    tf = false; % ASCII
else
    tf = true;  % Binary
end
end

function [F,V] = getDefaultSphereMesh

lesion = lesionModel;
I = voxelizeLesion(lesion);
[X,Y,Z] = getCartesianCoordinates(lesion,'Post');
C = lesion.contrast;
shape = isosurface(X,Y,Z,I,.1*C);
F = shape.faces; V = shape.vertices;
end

function mask = segmentLesionKmeans(I,psize,W,L)
%This function automatically segments a lesion using the k-means method

%Apply window and leveling to data
I=mat2gray(I,[L-W/2 L+W/2]);

%create a coordiate system
x=1:size(I,2); x=x-size(I,2)/2; x=x*psize(1);
y=1:size(I,1); y=y-size(I,1)/2; y=y*psize(2);
z=1:size(I,3); z=z-size(I,3)/2; z=z*psize(1);
[X,Y,Z]=meshgrid(x,y,z);
[~,~,R] = cart2sph(X,Y,Z);
R=mat2gray(R);

%create an initial Ostu segmentation (used as a feature over which k-mean
%clusters are found)
level = graythresh(I(:));
otsu = im2bw(I(:),level);

%determine the number of clusters according to hitogram of I
[N,edges] = histcounts(I(:));
N = smooth(N);
N=N./max(N);
pks = findpeaks(N);
K = length(pks)+1;
if K<2
    K=2;
end

%perform k-means clustering
%data = [I(:) R(:) otsu(:)];
data = [I(:) otsu(:)];
label = kmeans(data, K);
label=reshape(label,size(I));

%Figure out which label is of the lesion (will be one with lowest R value)
for i=1:K
    m = label==i;
    rmean(i) = mean(R(m));
end
[~,ind]=min(rmean);
mask = label==ind;

%dilate and erode the mask (helps smooth edges)
%SE = strel(ones(3,3,3));
%mask = imdilate(mask,SE);
%mask = imerode(mask,SE);

%fill holes
mask = imfill(mask,'holes');

%remove islands
CC = bwconncomp(mask);
for i=1:CC.NumObjects
    els(i) = numel(CC.PixelIdxList{i});
end
[~,ind]=max(els);
mask = false(size(I));
mask(CC.PixelIdxList{ind})=true;



end

function lims = getMaskBoundingBox(varargin)
switch nargin
    case 1
        mask = varargin{1};
        [Y, X, Z]=ind2sub(size(mask),find(mask));
        lims = [min(Y) max(Y); min(X) max(X); min(Z) max(Z)];
    case 2
        mask = varargin{1};
        buff = varargin{2};
        
        [Y, X, Z]=ind2sub(size(mask),find(mask));
        xmin=min(X)-buff; if xmin<1 xmin=1;end
        xmax=max(X)+buff; if xmax>size(mask,2) xmax=size(mask,2);end
        ymin=min(Y)-buff; if ymin<1 ymin=1;end
        ymax=max(Y)+buff; if ymax>size(mask,1) ymax=size(mask,1);end
        zmin=min(Z)-buff; if zmin<1 zmin=1;end
        zmax=max(Z)+buff; if zmax>size(mask,3) zmax=size(mask,3);end
        
        lims = [ymin ymax; xmin xmax; zmin zmax];
end
end

function volumeOfMesh(FV)
p=FV.vertices;
t=FV.faces;
normals=triangulation(t,p);
normals = normals.faceNormal;
% Compute the vectors d13 and d12
d13= [(p(1,t(2,:))-p(1,t(3,:))); (p(2,t(2,:))-p(2,t(3,:)));  (p(3,t(2,:))-p(3,t(3,:)))];
d12= [(p(1,t(1,:))-p(1,t(2,:))); (p(2,t(1,:))-p(2,t(2,:))); (p(3,t(1,:))-p(3,t(2,:)))];
cr = cross(d13,d12,1);%cross-product (vectorized)
area = 0.5*sqrt(cr(1,:).^2+cr(2,:).^2+cr(3,:).^2);% Area of each triangle
totalArea = sum(area);
% crNorm = sqrt(cr(1,:).^2+cr(2,:).^2+cr(3,:).^2);
zMean = (p(3,t(1,:))+p(3,t(2,:))+p(3,t(3,:)))/3;
% nz = -cr(3,:)./crNorm;% z component of normal for each triangle
nz2 = normals(:,3)';
% volume = area.*zMean.*nz; % contribution of each triangle
volume = area.*zMean.*nz2; % contribution of each triangle
totalVolume = abs(sum(volume));%divergence theorem

end

function [x,y,z]=getCTcoordinates(info,format)

switch format
    case 'DICOMs'
        psize=info(1).PixelSpacing;
        x=(0:double(info(1).Width)-1)'; x=x*psize(1); x=x+info(1).ImagePositionPatient(1);
        y=(0:double(info(1).Height)-1)'; y=y*psize(2); y=y+info(1).ImagePositionPatient(2);
        clear z;
        for j=1:length(info)
            z(j,1)=info(j).ImagePositionPatient(3);
        end
    case '.ipv'
        
        x=(0:info.S(2)-1).*info.pspacing(1)+info.Origin(1);
        y=(0:info.S(1)-1).*info.pspacing(2)+info.Origin(2);
        z=(0:info.S(3)-1).*info.pspacing(3)+info.Origin(3);
        
end

end

function [proceed,paths] = checkForDependentRespositories(fnames,verbose)

%Initialize output
proceed = true(size(fnames));
paths = {};

% Get all paths in the MATLAB search path
check_search_path = strsplit(path, pathsep);

for i=1:length(fnames)
    
    % Find matches
    matches(i) = any(contains(check_search_path, fnames));

    if ~matches(i)
        proceed(i) = false;
        paths{i,1}='';
        if verbose
            warning([fnames{i} ' respository not found and is required for lesionTool to run properly. ' ...
                'Get the repository and add to Matlab search path!']);
        end
    else
        p = what(fnames{i});
        p=p(1);
        paths{i,1}=[p.path filesep];
    end
end
end