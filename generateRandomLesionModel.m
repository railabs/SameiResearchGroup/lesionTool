function lesion = generateRandomLesionModel(varargin)
%This function randomly generates a lesion model based on either a
%pre-defined randomization profile or based on user defined randomization
%setttings
% lesion = generateRandomLesionModel() generates a lesion model based on
% default randomization settings.
%
% lesion = generateRandomLesionModel(type) generates a lesion model based
% on a pre-defined randomization profile specified by type. type is a
% string and can be either 'Liver', 'Lung', or 'Renal'.
%
% lesion = generateRandomLesionModel(randomTable) generates a lesion model
% based on the parameters in randomTable, a 5x4 numeric array in the
% following format:
%               Mean    Min     Max     STD
%Mean Radius   [x       x       x       x 
%Radius COV     x       x       x       x
%Peak Contrast  x       x       x       x
%n              x       x       x       x
%Edge Blur      x       x       x       x]
%
%Notes:
%This function will automatically adjust the FOV and number of voxels of
%the model in order to fit the lesion and to have a pixel size that is
%similar to the default lesion (.8 mm isotropic). The final FOV and pixel
%size however may not be isotropic depending on the shape of the lesion.

switch nargin
    case 0
        input = 'Generic';
    case 1
        input = varargin{1};
    otherwise
        error('Incorrect number of inputs');
end


%Determine if user input a lesion type string or a matrix of randomization
%parameters
if isnumeric(input)
    type = 'Custom';    
elseif ischar(input)
    type = input;
end

%Get the randomization parameters
switch type
    case 'Custom'
        data = input;
    case 'Liver'
        data = [8 0 20 2.25; 20 0 100 5; -80 -200 -10 25; .1 0 .2 0; .85 0 2 .2];
    case 'Lung'
        data = [4 0 20 1.75; 10 0 100 4.5; 1000 10 2000 300; .75 0 2 .5; .35 0 2 0];
    case 'Renal'
        data = [2 0 4 .75; 30 0 100 7.5; 1000 10 2000 500; .5 0 3 .3; .1 0 2 0];
    case 'Generic'
        data = [5 3 7 1;10 0 20 5;100 50 150 25;1 0 2 .5;1 0 2 .5];
    otherwise
        error('LesionType not found, possible types are ''Liver'', ''Lung'', or ''Renal''');
end

%Make a default lesion
lesion = lesionModel;

%Get the mean radius
i = 1; %First row of data
Rmean = random('norm',data(i,1),data(i,4),1);
if Rmean<data(i,2)
    Rmean = data(i,2);
elseif Rmean>data(i,3)
    Rmean=data(i,3);
end

%Get the radius COV
i = 2; %Second row of data
Rcov = random('norm',data(i,1),data(i,4),1);
if Rcov<data(i,2)
    Rcov = data(i,2);
elseif Rcov>data(i,3)
    Rcov=data(i,3);
end

%Get the radius values based on Rmean and Rcov
Rb = random('norm',Rmean,Rmean*Rcov/100,size(lesion.shape.Rb));
Rb(1,:) = Rb(1,1);
Rb(end,:) = Rb(end,1);
Rb(Rb<0) = Rmean;
lesion.shape.Rb = Rb;

%Get the peak contrast
i = 3;
Cp = round(random('norm',data(i,1),data(i,4),1));
if Cp < data(i,2)
    Cp = data(i,2);
end
if Cp > data(i,3)
    Cp = data(i,3);
end
lesion.profile.C=Cp;

%Get n
i = 4;
n = random('norm',data(i,1),data(i,4),1);
if n < data(i,2)
    n = data(i,2);
end
if n > data(i,3)
    n = data(i,3);
end
lesion.profile.n=n;

%Get edge blur;
i=5;
B = random('norm',data(i,1),data(i,4),1);
if B < data(i,2)
    B = data(i,2);
end
if B > data(i,3)
    B = data(i,3);
end
lesion.blur.PSF = [B B B];

%Get old psize
psize_old = lesion.psize;

%Crop the lesion
cropToBoundingBox(lesion)

%Get new psize
psize_new = lesion.psize;

%Adjust number of voxels to get approximately the same old pixel size
lesion.n = ceil(lesion.n.*(psize_new./psize_old));

end

